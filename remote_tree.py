#!/usr/bin/python3
## ファイルツリーを表示するクラス
# 参考：https://qiita.com/dede-20191130/items/327fbeb181f13ee4ba2b
# cc by-sa Yasushi Honda 2023 12.20
import cv2
import glob
import os,time
import subprocess
import tkinter as tk
import tkinter.ttk as ttk
import linecache
import threading
import timeout_decorator
from PIL import Image,ImageTk
from modules import md2tk
from modules import pycode

text_fg="#204030"
text_bg="#d0e0d0"
howto_bg="#fafaa0"

class Tree(): ## pythonファイルを別窓で開くクラス
    def __init__(self,root,myPath,location,local,robot_id,robot_addr,calc_id,calc_addr): ## コンストラクター
        self.path=myPath
        self.home=myPath
        self.location=location
        self.local=local
        self.robot_id=robot_id
        self.robot_addr=robot_addr
        self.calc_id=calc_id
        self.calc_addr=calc_addr
        self.user="" # open_list実行時にrobot_id/calc_idが選ばれる
        self.addr="" # open_list実行時にrobot_addr/calc_addrが選ばれる
        self.tab_select=False
        self.selected_file=""
        if 'Robot' in location:
            self.color="DarkOliveGreen3"
        elif 'Calc' in location:
            self.color="pink"

        tree_frame = tk.Frame(root, relief=tk.RAISED, bd=0,bg=text_bg) ##
        cmd_frame = tk.Frame(root, relief=tk.RAISED, bd=0,bg=self.color) ##
        btn_frame = tk.Frame(root, relief=tk.RAISED, bd=0,bg=self.color) ##

        self.style = ttk.Style()
        self.style.configure("Treeview", fieldbackground=self.color)
        '''
        self.tree = ttk.Treeview(tree_frame, height=27, selectmode="browse")
        '''
        self.tree = ttk.Treeview(tree_frame, selectmode="browse") ## Treeviewインスタンス
        self.tree.bind("<<TreeviewSelect>>",self.select_record) ## File Treeからマウスで選んだとき実行
        #self.tree.bind("<Double-Button-1>", self.double_click)  ## ダブルクリックしたとき実行
        #self.tree.bind("<Button-3>", self.click3)
        self.tree.pack(fill="both",expand=True,side="top") ## File Treeを配置

        cmd_frame.pack(side="top", fill="x")
        tree_frame.pack(side="bottom", expand=True, fill="both")
        btn_frame.pack(side="bottom", fill="x")

        execute_button = tk.Button(cmd_frame,text="ssh実行", command=self.execute, bg="white",fg="black")
        execute_button.pack(side='right')
        # 選ばれたエントリーを表示
        Label=tk.Label(cmd_frame,text=" Path:", bg=text_bg,fg=text_fg, height=1)
        Label.pack(side="left",padx=0,pady=0)
        self.select_entry=tk.Entry(cmd_frame, bd=0, bg="DodgerBlue4",fg="white")
        self.select_entry.pack(side='left',expand=True,fill='x')


        self.tree["columns"] = (1,2,3,4) #列インデックスの作成

        self.tree["show"] = ("tree","headings")
        
        self.tree.column(1,width=80)
        self.tree.column(2,width=80)
        self.tree.column(3,width=320)
        self.tree.column(4,width=50)
        self.tree.heading(1, text="Time Stamp") # 各列のヘッダー設定
        self.tree.heading(2, text="Permission")
        self.tree.heading(3, text="Path")
        self.tree.heading(4, text="Size")
       

        # tagの色指定
        self.tree.tag_configure("dir_row", background='#b0b4b0',foreground='#040804')
        self.tree.tag_configure("latex_row", background='darkkhaki',foreground='black')
        self.tree.tag_configure("python_row", background=self.color,foreground=text_fg)
        self.tree.tag_configure("csv_row", background="white",foreground='DodgerBlue')
        self.tree.tag_configure("md_row", background='white',foreground='black')
        self.tree.tag_configure("img_row", background='white',foreground='DodgerBlue')
        self.tree.tag_configure("pth_row", background='white',foreground='red')
        self.tree.tag_configure("etc_row", background='white',foreground='black')

        # ボタンの作成
        #exec_button = tk.Button(button_frame,text="実行", command=self.execute)
        parent_button = tk.Button(btn_frame,text="<<", command=self.parent_dir,bg="white",fg="black")
        reload_button = tk.Button(btn_frame,text="reload", command=self.reload_mydir,bg="white",fg="black")
        home_button = tk.Button(btn_frame,text="home", command=self.home_dir,bg="white",fg="black")
        child_button = tk.Button(btn_frame,text=">>", command=self.child_dir,bg="white",fg="black")
        # ボタンの配置
        #exec_button.grid(padx=5, pady=5, row=2,column=0)
        #select_button.grid(padx=2, pady=5, row=0,column=1)
        child_button.pack(side="right",expand=True,fill="x")
        reload_button.pack(side="right",expand=True,fill="x")
        home_button.pack(side="right",expand=True,fill="x")
        parent_button.pack(side="right",expand=True,fill="x")

        #print(glob.glob('/home/honda/GitLab/**'))
        #print(self.remote_ls(self.calc_id.get(), self.calc_addr.get(), "GitLab/"))

    def select_record(self,event):
        try:
            selectedItems = self.tree.selection()
            #print(self.tree.item(selectedItems[0])['values'])
            filePath = self.tree.item(selectedItems[0])['values'][2]
            self.select_entry.delete(0,tk.END)
            self.select_entry.insert(0,filePath)
        except:
            print("ファイルを選んでください")

    def send(self,user,addr): ## Execute/ に scpする
        try:
            selectedItems = self.tree.selection()
            path = self.tree.item(selectedItems[0])['values'][2]
            print(path)
            cmd="scp -r " + path + " "+user.get()+"@"+addr.get() + ":Execute/ "
            #cmd="rsync -av -h --progress " + path + " "+user.get()+"@"+addr.get() + ":Execute/ "
            #pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
            #print(proc.stderr)

            pro=subprocess.Popen(cmd.strip().split(' '),stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
            #line=pro.stdout.readline().decode('utf8').replace("\r","")
            #print(line)

        except:
            print(user+"@"+addr+" にファイル転送失敗")
    def check_execute(self,user,addr):
        # Execute/の存在確認
        path='Execute/'
        cmd='ssh '+user+'@'+addr+' test -d ' + path +'; echo $?'
        prc=subprocess.run(cmd,shell=True,capture_output=True,text=True)
        if int(prc.stdout.strip("\n"))==1: # Execute/が存在しない場合
            cmd='ssh '+user+'@'+addr+' mkdir Execute'
            prc=subprocess.run(cmd,shell=True,capture_output=True,text=True)
        else:
            print("%s は %s に存在します．" % (path,addr))

    @timeout_decorator.timeout(60) # ネットワークなしのときなど，時間切れを設定
    def reload_mydir(self):
        self.tree.delete(*self.tree.get_children())
        if 'Robot' in self.location:
            self.open_list(self.robot_id.get(),self.robot_addr.get(),"",self.path)
        elif 'Calc' in self.location:
            self.open_list(self.calc_id.get(),self.calc_addr.get(),"",self.path)

    @timeout_decorator.timeout(60) # ネットワークなしのときなど，時間切れを設定
    def reload_robot(self):
        print(self.path)
        self.check_execute(self.robot_id.get(),self.robot_addr.get())
        if self.tab_select==False:
            self.tree.delete(*self.tree.get_children())
            self.open_list(self.robot_id.get(),self.robot_addr.get(),"",self.path)
            self.tab_select=True

    @timeout_decorator.timeout(60) # ネットワークなしのときなど，時間切れを設定
    def reload_calc(self):
        print(self.path)
        self.check_execute(self.calc_id.get(),self.calc_addr.get())
        if self.tab_select==False:
            self.tree.delete(*self.tree.get_children())
            self.open_list(self.calc_id.get(),self.calc_addr.get(),"",self.path)
            self.tab_select=True

    def home_dir(self):
        filePath=self.home
        self.path=self.home
        self.select_entry.delete(0,tk.END)
        self.select_entry.insert(0,filePath)
        self.tree.delete(*self.tree.get_children())
        self.open_list(self.user,self.addr,"",filePath)
        self.output_work_dir(filePath)

    def key_check(self,user,addr): # 既存のssh公開鍵を確認
        #cmd="ssh-keygen -l -f ~/.ssh/"+addr+"_rsa.pub"
        key_path=os.path.expanduser("~")+"/.ssh/"+addr+"_rsa.pub"
        #print(key_path)
        if os.path.exists(key_path):
            return True
        else:
            print("公開鍵ファイルが存在しません．")
            return False

    def remote_ls(self, user, addr, path):
        if self.key_check(user,addr): # 公開鍵の存在確認
            #sshとls コマンドでリモートのファイルとディレクトリのリストを取得．ls に-p オプションをつけて
            #ディレクトリ名の末尾に/をつける．
            cmd="ssh " + user +"@"+ addr +" ls -plh "+ path +" &"
            #print(cmd)
            proc = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
            ls_list=proc.stdout.strip("\n").split("\n")
            file_list=[]
            for items in ls_list:
                file_list.append(items.split())
            return file_list[1:][:] # 0番目は合計なのでリターンしない．
        else:
            print("ssh lsに失敗しました.")
            return False

    def open_list(self,user,addr,parent,path): ## ディレクトリを再帰的に呼び出して実際にtreeviewの内容をつくる
        self.user=user
        self.addr=addr
        # 選択可能なpythonファイルを辞書の値として設定
        # 指定フォルダ直下のすべてのpythonファイル（再帰的に検索）
        file_Dic = {} # ファイルリスト用の辞書
        dir_Dic = {} # ディレクトリリスト用の辞書
        #print(glob.glob(path+'/**'))
        #for file in glob.glob(path+'/**'): ## pathの中のすべての名前(file)に対して
        if not self.remote_ls(user,addr,path):
            print("ネットワーク(Wi-Fi)接続を確認してください．\n または userとaddressを確認してください．")
            return

        for item in self.remote_ls(user, addr, path):
            # fileは絶対パス付きのファイル名 
            file=item[8]
            nameRoot, ext = os.path.splitext(os.path.basename(file))
            if os.path.isfile(file) and ext in ('.py', '.pyw'): ## fileがPythonの場合
                fp=open(file,"r",encoding="utf8",errors='ignore')
                fp.readline()
                comment=fp.readline() ## 2行目をコメントとして読み込む
                fp.close()
            else:
                comment="none"

            if file.find('/')>0: ## 名前がディレクトリの場合
                dir_Dic[file] = {'name':file, ## 要素をdir_Dicに格納(commend,extはなし)
                              'timestanp':item[7]+" / "+item[5]+item[6],
                              'permission':item[0],
                              'abspath':path+file,
                              'size':item[4]}
            else:
                file_Dic[file] = {'name':file, ## 要素をfile_Dicに格納（２次元辞書）
                              'timestanp':item[7]+" / "+item[5]+item[6],
                              'permission':item[0],
                              'abspath':path+file,
                              'size':item[4],
                              'ext':ext}
            # selectで絶対パス付きのファイル名を必要とするので，abspathも辞書に格納


        for k, v in sorted(dir_Dic.items(), key=lambda x: x[1]['name']): ## dirレコードの挿入
            if str(v['name'])=='Calc':
                child=self.tree.insert(parent, "end", text=str(v['name']), 
                    values=(str(v['timestanp']),str(v['permission']),str(v['abspath']),str(v['size'])),tag="dir_row" )
            elif str(v['name'])=='Robot':
                child=self.tree.insert(parent, "end", text=str(v['name']), 
                    values=(str(v['timestanp']),str(v['permission']),str(v['abspath']),str(v['size'])),tag="dir_row" )
            else:
                child=self.tree.insert(parent, "end", text=str(v['name']), 
                    values=(str(v['timestanp']),str(v['permission']),str(v['abspath']),str(v['size'])),tag="dir_row" )
            print(str(v['abspath']))
            self.open_list(user, addr, child, str(v['abspath']))  ## 再帰的にchildデイレクトリを開く

        # fileを拡張子で分類してレコードの挿入
        for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']): ## fileレコードの挿入
            if v['ext']=='.py' :
                tag_name='python_row'
            elif v['ext']=='.csv' :
                tag_name='csv_row'
            elif v['ext']=='.trc' :
                tag_name='csv_row'
            elif v['ext']=='.tex' :
                tag_name='latex_row'
            elif v['ext']=='.pth' :
                tag_name='pth_row'
            elif v['ext']=='.md' :
                tag_name='md_row'
            elif v['ext']=='.txt' :
                tag_name='etc_row'
            elif v['ext']=='.pdf' or v['ext']=='.PDF' :
                tag_name='etc_row'
            elif v['ext']=='.mp4' or v['ext']=='.MP4':
                tag_name='img_row'
            elif v['ext']=='.webm' or v['ext']=='.WEBM':
                tag_name='img_row'
            elif v['ext']=='.mp3' or v['ext']=='.MP3':
                tag_name='img_row'
            elif v['ext']=='.wav' or v['ext']=='.WAV':
                tag_name='img_row'
            elif v['ext']=='.jpg' or v['ext']=='.JPG' or v['ext']=='.jpeg' or v['ext']=='.JPEG':
                tag_name='img_row'
            elif v['ext']=='.png' or v['ext']=='.PNG':
                tag_name='img_row'
            else:
                tag_name='etc_row'

            self.tree.insert(parent, "end", text=str(v['name']), 
                values=(str(v['timestanp']),str(v['permission']),str(v['abspath']),str(v['size'])),tag=tag_name )


    def output_work_dir(self,path):
        exec_dir=os.path.dirname(__file__)
        fp=open(exec_dir+"/work_dir","w")
        fp.write(path)
        fp.close()

    def get_selected(self):
        return self.selected_file

    def execute(self):
        # 表から選んだファイルパスを取得
        selectedItems = self.tree.selection()
        filePath = self.tree.item(selectedItems[0])['values'][2]
        print(filePath)

        # pyファイル呼び出し実行
        #subprocess.check_call(['python3', filePath])
        program="ssh -X " + self.user+"@"+self.addr+ \
                " \'source ~/.profile; python3 -u ~/" + filePath + \
                " "+ self.local.get()+" "+self.robot_addr.get()+" "+self.calc_addr.get()+ \
                " ;bash \' "
                # python3 -u オプションでバッファリングしない．
                # bashを続けて実行することで，端末を開いたままにする．
        print(program)
        cmd="mate-terminal --title='"+filePath+"' --command \"" + program + "\" " + \
            " --geometry 84x18+200+600 "
        print(cmd)
        pro=subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT,text=True)

    def parent_dir(self):
        if self.path.count('/')>=2:
            self.path=self.path.strip('/')
            self.path=self.path[:self.path.rfind('/')]+'/'
            #print(self.path)
            self.select_entry.delete(0,tk.END)
            self.select_entry.insert(0,self.path)
            self.tree.delete(*self.tree.get_children())
            self.open_list(self.user,self.addr,"",self.path)

    def child_dir(self):
        try:
            selectedItems = self.tree.selection()
            self.path = self.tree.item(selectedItems[0])['values'][2]
        except:
            print("選択失敗")

        print(self.path)
        self.tree.delete(*self.tree.get_children())
        self.open_list(self.user,self.addr,"",self.path)

    def font_mod(self,size):
        font = ('Yu Gothic UI', size)
        self.style.configure("Treeview", font=font, rowheight=size*2)
        self.tree.update()

if __name__ == '__main__': ## 実行されたときのメインルーチン
    my_tree=Tree('/home/honda/GitLab/')  ##
