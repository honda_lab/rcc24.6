call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

Plug 'preservim/nerdtree' |
            \ Plug 'Xuyuanp/nerdtree-git-plugin'

Plug 'tyru/open-browser.vim'

Plug 'nelstrom/vim-visual-star-search'

Plug 'nathanaelkane/vim-indent-guides'

call plug#end()

let g:NERDTreeGitIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ "Unknown"   : "?"
    \ }

let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1

let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=indianred   ctermbg=240
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=dodgerblue ctermbg=240

" open-browser.vim
let g:netrw_nogx = 1 " disable netrw's gx mapping.
nmap gx <Plug>(openbrowser-smart-search)
vmap gx <Plug>(openbrowser-smart-search)

map <C-p> :PlugInstall<CR>
map <C-t> :NERDTreeToggle<CR>
map <C-i> :IndentGuidesEnable<CR>
map <C-d> :IndentGuidesDisable<CR>
map <C-e> :!python3 % <CR>
" autocmd vimenter * NERDTree

set nu
colorscheme desert
set ts=4 sw=4 noet
