#!/usr/bin/python3
# wavファイルをchunkごとにpsdを表示と再生
import re
import keyin
import sys
import pyaudio
import time as tm
import numpy as np
import wave as wv
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

matplotlib.use('TkAgg')
plt.rcParams['font.family'] = 'IPAPGothic'
jpfont={'family':'IPAexGothic'}

sns.set()
sns.set_style("whitegrid", {'grid.linestyle': '--'})
sns.set(font_scale=0.5)
#plt.rcParams["figure.figsize"] = [12, 12]
#plt.grid()
fig = plt.figure(figsize=(8, 8))
wav_ax = fig.add_subplot(2,1,1)
wav_ax.set_xlabel("time (sec)")
wav_ax.set_ylabel("Amp")

chunk_ax = fig.add_subplot(2,2,3)
chunk_ax.set_xlabel("time (sec)")
chunk_ax.set_ylabel("Amp in a chunk")
chunk_ax.set_title("Chunk波形拡大",font=jpfont)

fft_ax = fig.add_subplot(2,2,4)
fft_ax.set_xlabel("freq (Hz)")
fft_ax.set_ylabel("FFT Amp")
fft_ax.set_title("スペクトル(フーリエ係数)",font=jpfont)

plt.legend(loc='upper left')
plt.get_current_fig_manager().window.wm_geometry("+480+330")


if __name__ == '__main__':
    # Chunk サイズ 
    size=2**13 # 対象にする音声データの個数．sampling rate(sr)に等しくしたら１秒間のデータに対応する．

    freq_max=size

    filepath=sys.argv[1]
    wav_file=re.findall('[^/]+.wav',filepath, flags=re.IGNORECASE)[0]
    wav_ax.set_title(wav_file+"の全体波形概略",font=jpfont)

    wf=wv.open(filepath,'r')
    full=wf.readframes(-1)
    wave=np.frombuffer(full, dtype="int16") / float(2**15)

    sr=wf.getframerate()
    nframes=wf.getnframes()
    length=nframes/sr

    chunk_time = np.arange(0,nframes*2) / (sr*2)
    dt=1/sr
    print(nframes,sr,nframes/sr,len(full),len(wave))


    # waveファイルから間引きthinningをつくる
    #thinning_size=2**11
    #thinning=wave[::thinning_size]

    # Chunk内の絶対値最大を抜き出す
    thinning=[]
    p=0
    while p<nframes:
       py=wave[p*2:(p+size)*2]
       try:
          k=np.argmax(np.abs(py))
          thinning.append(py[k])
       except:
          pass
       
       p+=size 
    thinning_size=int(2*nframes/len(thinning))
    time = np.arange(0,len(thinning)) / (2*sr/thinning_size)
    print(len(thinning),length)

    pa = pyaudio.PyAudio()
    stream = pa.open(format=pa.get_format_from_width(wf.getsampwidth()),
                     channels=wf.getnchannels(),
                     rate=int(wf.getframerate()*1.0),
                     output=True,
                     frames_per_buffer = size)

    stream.start_stream()

    # lineの準備
    px=[0.0 for i in range(2*size)]
    py=[0.0 for i in range(2*size)]
    py1=[0.0 for i in range(freq_max)]

    px2=[0.0 for i in range(len(thinning))]
    py2=[0.0 for i in range(len(thinning))]
    
    #freq = np.fft.fftfreq(size, d=dt) # 周波数を割り当てる
    freq = np.fft.fftfreq(freq_max, d=dt) # 周波数を割り当てる
    fft_lin, =  fft_ax.plot(freq,py1,color='#0000ff',label='fft')
    fft_ax.set_ylim(0,0.15)
    fft_ax.set_xlim(0,1000)
    fft_lin.set_xdata(freq)

    wav_ax.set_ylim(-1.0,+1.0)
    #wav_ax.set_xlim(min(px),max(px))
    #wav_lin, =  wav_ax.plot(px,py,color='#0000ff',label='wav')

    wav_ax.set_xlim(min(time),max(time))
    wav_lin, =  wav_ax.plot(px2,py2,color='#bbbbbb',label='thinning wav')
    wav_lin.set_xdata(time)
    # wave in real time
    wav_lin.set_ydata(thinning)
    chunk_lin, =  wav_ax.plot(px,py,color='#ff0000',label='chunk wav')

    chunk_ax.set_ylim(-1.0,+1.0)
    chunkz_lin, =  chunk_ax.plot(px,py,color='#ff0000',label='chunk wav')

    key=keyin.Keyboard()
    ch='c' 
    state=ch
    while ch!='q': # mainループ
       start_t=0.0 # スタート時間(sec)
       period_t=length # 再生長さ(sec)
       start_p=int(sr*start_t)
       end_p=start_p+int(sr*period_t)
    
       now=tm.time()
       start=now
       p=start_p
       while p<end_p and ch!='q':
          print("\r %5d %5d %5.1f/%5.1f" % (p,size,chunk_time[2*p],length),end='')
          t=start_t+(now-start)
          chunk=full[p*4:(p+size)*4]
          stream.write(chunk)

          px=chunk_time[p*2:(p+size)*2]
          py=wave[p*2:(p+size)*2]
          chunk_lin.set_xdata(px) 
          chunk_lin.set_ydata(py) 
         
          chunk_ax.set_xlim(min(px),max(px))
          chunkz_lin.set_xdata(px) 
          chunkz_lin.set_ydata(py) 

          # fft with small time window
          y_fft = np.fft.fft(py,n=freq_max) # 離散フーリエ変換
          Amp = abs(y_fft/(size/2)) # 音の大きさ（振幅の大きさ）
          fft_lin.set_ydata(Amp)

          plt.pause(0.001)
          now=tm.time()

          if state=='c':
             p+=size

          # キーボード操作で制御
          ch=key.read()
          if ch=='s':
             state='s'
          if ch=='c':
             state='c'
          if ch=='k':
             p-=size
          if ch=='j':
             p+=size
          if ch=='K':
             p-=10*size
          if ch=='J':
             p+=10*size
          if ch=='x':
             size=int(size*2)
             px=[0.0 for i in range(2*size)]
             py=[0.0 for i in range(2*size)]
             py1=[0.0 for i in range(size)]
             freq = np.fft.fftfreq(size, d=dt) # 周波数を割り当てる
             fft_lin, =  fft_ax.plot(freq,py1,color='#ff0000',label='fft')
             fft_lin.set_ydata(py1)
             chunkz_lin, =  chunk_ax.plot(px,py,color='#0000ff',label='wav')
          if ch=='u':
             size=int(size/2)
             px=[0.0 for i in range(2*size)]
             py=[0.0 for i in range(2*size)]
             py1=[0.0 for i in range(size)]
             freq = np.fft.fftfreq(size, d=dt) # 周波数を割り当てる
             fft_lin, =  fft_ax.plot(freq,py1,color='#ff0000',label='fft')
             fft_lin.set_ydata(py1)
             chunkz_lin, =  chunk_ax.plot(px,py,color='#0000ff',label='wav')

          tm.sleep(0.02)

    
    stream.stop_stream()
    stream.close()
    pa.terminate()
