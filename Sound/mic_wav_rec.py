import keyin
import os,sys
import pyaudio
import wave
import time as tm
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
sns.set_style("whitegrid", {'grid.linestyle': '--'})
sns.set(font_scale=1.0)
#plt.rcParams["figure.figsize"] = [12, 12]
#plt.grid()
fig = plt.figure(figsize=(8, 4))
wav_ax = fig.add_subplot(1,2,1)
wav_ax.set_xlabel("time (sec)")
wav_ax.set_ylabel("Amp")

fft_ax = fig.add_subplot(1,2,2)
fft_ax.set_xlabel("freq (Hz)")
fft_ax.set_ylabel("FFT Amp")
plt.legend(loc='upper left')

class Mic(): 
    def __init__(self):
        self.pa = pyaudio.PyAudio()

    def open(self,sr,size):
        FORMAT = pyaudio.paInt16
        stream = self.pa.open(format=FORMAT,
                     channels=1,
                     rate=sr,
                     input=True,
                     frames_per_buffer = size)

        stream.start_stream()
        return stream

    def close(self,stream):
        stream.stop_stream()
        stream.close()
        self.pa.terminate()


if __name__ == '__main__':
    filepath=sys.argv[1]
    print(filepath)
    nameRoot,ext=os.path.splitext(os.path.basename(filepath))
    if ext!='.wav':
        print("wavファイルを選んでください")
        exit(0)

    wf=wave.open(filepath,'wb')
    wf.setnchannels(1)
    wf.setsampwidth(2)
    #wf.setsampwidth(pa.get_sample_size(FORMAT))

    sr=44100
    wf.setframerate(sr)
    dt=1/sr
    chunk_size=2**13 # 対象にする音声データのchunkサイズ．sampling rate(sr)に等しくしたら１秒間のデータに対応する．

    mic=Mic()
    stream=mic.open(sr,chunk_size)

    # lineの準備
    px=[0.0 for i in range(2*chunk_size)]
    py=[0.0 for i in range(2*chunk_size)]
    py1=[0.0 for i in range(chunk_size)]
    
    freq = np.fft.fftfreq(chunk_size, d=dt) # 周波数を割り当てる
    fft_lin, =  fft_ax.plot(freq,py1,color='#ff0000',label='fft')
    fft_ax.set_ylim(0,0.05)
    fft_ax.set_xlim(1,880)
    fft_lin.set_xdata(freq)

    wav_ax.set_ylim(-1.0,1.0)
    wav_ax.set_xlim(min(px),max(px))
    wav_lin, =  wav_ax.plot(px,py,color='#0000ff',label='wav')
    time = np.arange(0,chunk_size) / (sr*2)
    wav_ax.set_xlim(min(time),max(time))
    wav_lin.set_xdata(time)

    frames=[]
    key=keyin.Keyboard()
    chunk=stream.read(chunk_size) # mic入力初期値
    ch='c' 
    state=ch
    now=tm.time()
    start=now
    while ch!='q':
       now=tm.time()
       t=now-start

       # wave in real time
       if state=='c':
          chunk=stream.read(chunk_size) # mic入力
          frames.append(chunk)

       data=np.frombuffer(chunk, dtype="int16") / float(2**13) # 解析用に変換
       wav_lin.set_ydata(data)
       print("\r %5.1f" % (t),end='')

       # fft with small time window
       y_fft = np.fft.fft(data,n=chunk_size) # 離散フーリエ変換
       Amp = abs(y_fft/(chunk_size/2)) # 音の大きさ（振幅の大きさ）
       fft_lin.set_ydata(Amp)

       plt.pause(0.001)

       ch=key.read()
       if ch=='s':
          state='s'
       if ch=='c':
          state='c'

       #tm.sleep(0.001)

    mic.close(stream)
    
    # Binaryに変換して書き込み
    wf.writeframes(b''.join(frames))
    wf.close()
