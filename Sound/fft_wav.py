import time as tm
import os
import sys
from pygame import mixer
from pydub import AudioSegment
import librosa
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
sns.set_style("whitegrid", {'grid.linestyle': '--'})
sns.set(font_scale=2.0)
plt.rcParams["figure.figsize"] = [12, 12]
plt.grid()
plt.legend(loc='upper left')

# 音楽ファイルのパスを設定（例："/foldername/filename.mp3"）
#wav_file = "Dai-Miyata_The-Swan.wav"
wav_file = sys.argv[1]
# 音声ファイルの読み込み
#dirname = os.path.dirname(__file__) # このプログラムの実行directory
#filepath=dirname+"/"+wav_file  # 音声ファイルの絶対pathをつくる
filepath=wav_file

# loadメソッドでy=音声信号の値（audio time series）、sr=サンプリング周波数（sampling rate）を取得
# 参考：https://librosa.org/doc/latest/generated/librosa.load.html?highlight=load#librosa.load
y, sr = librosa.load(filepath)
# 時間 = yのデータ数 / サンプリング周波数
# 参考：https://note.nkmk.me/python-numpy-arange-linspace/
time = np.arange(0,len(y)) / sr


plt.title(wav_file)
# xにtime、yにyとしてプロット
plt.plot(time, y, label="Time sequence")
# x軸とy軸にラベルを設定（x軸は時間、y軸は振幅）
# 参考：https://techacademy.jp/magazine/19316
plt.xlabel("Time(s)")
plt.ylabel("Sound Amplitude")

# グラフを表示
plt.show()


### FFT: tの関数をfの関数にする ###
N=len(y)
dt=1/sr
y_fft = np.fft.fft(y) # 離散フーリエ変換
freq = np.fft.fftfreq(N, d=dt) # 周波数を割り当てる（※後述）
Amp = abs(y_fft/(N/2)) # 音の大きさ（振幅の大きさ）

plt.title("FFT of "+wav_file)
### 音波のスペクトル ###
plt.plot(freq[1:int(N/2)], Amp[1:int(N/2)], label="Power spectral density") # A-f グラフのプロット
plt.xscale("log") # 横軸を対数軸にセット
plt.xlim([50,21000])
plt.show()
