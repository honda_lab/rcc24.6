# cf. https://zenn.dev/foxfoxfox/articles/be23e942cf3192
import os, re
import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk

text_fg="#204030"
text_bg="#d0e0d0"
howto_bg="#fafaa0"

odd_line_num_bg="#404040"
odd_line_num_fg="#00ffff"
even_line_num_bg="#d0d0df"
even_line_num_fg="brown"
#odd_line_bg="#e0e3e0"
#odd_line_fg="#404040"
odd_line_bg="#404542"
odd_line_fg="#ffffff"
even_line_bg="#e7efe7"
even_line_fg="#404040"

## 検索結果を行番号とスクロールバー付きのcanvasとして表示
class Structure: ##
    def __init__(self, path, root):  ##
        #super().__init__(root)
        self.path=path
        for widget in root.winfo_children():
           widget.pack_forget()

        # winfo_height、winfo_widthでmasterのウィンドウ情報を取得
        self.height = root.winfo_height()
        self.width = root.winfo_width()
    
        # Canvasを親とした縦横方向のScrollbar
        self.canvas=tk.Canvas(root,width=640,height=800) ##
        self.canvas.configure(bg=odd_line_bg)
        self.canvas.propagate(True)


        self.frame=tk.Frame(self.canvas) ##
        self.frame.configure(bg=odd_line_bg)
        #self.frame.propagate(True)
        self.frame.bind("<Configure>", lambda e: self.canvas.configure(scrollregion=self.canvas.bbox("all")))  ##

        #self.frame.pack(anchor=tk.NW)
        self.canvas.create_window((0, 0), window=self.frame, anchor="nw")


        self.sbar_v = ttk.Scrollbar(root, orient=tk.VERTICAL) ##
        self.sbar_v.config(command=self.canvas.yview)
        self.sbar_v.pack(side='right', fill=tk.Y)
        self.canvas.configure(yscrollcommand=self.sbar_v.set)

        scrollbar_h = ttk.Scrollbar(root, orient=tk.HORIZONTAL) ##
        scrollbar_h.config(command=self.canvas.xview)
        scrollbar_h.pack(side='bottom', fill=tk.X)
        self.canvas.configure(xscrollcommand=scrollbar_h.set)

        self.canvas.pack(side=tk.LEFT,expand=True,fill=tk.BOTH)
        #self.canvas.create_image(0, 0 , anchor = tk.NW, image=tk_img)        # 画像表示

        #self.markdown2Tkinter() # Canvasの中身Frameを作る

        self.frame.bind("<ButtonPress-1>", self.yup_scrolling,"+")
        self.frame.bind("<ButtonPress-3>", self.yup_scrolling,"+")

        self.frame.bind("<ButtonPress-4>", self.yup_scrolling,"+")    ##
        self.frame.bind("<ButtonPress-5>", self.ydown_scrolling,"+")  ##
        

    def yup_scrolling(self,event): ##
        self.canvas.yview_scroll(-1, 'units')  ##
 
    def ydown_scrolling(self,event): ##
        self.canvas.yview_scroll(+1, 'units')  ##

    def markdown2Tkinter(self): ##
        with open(self.path, 'r', encoding='utf-8') as f:
            text = f.read()

        # textを改行で分割
        text = text.split('\n')

        #print("textを1行ずつlineMdToTkinterに渡す")
        line_num=1
        for line in text:
           if "##" in line: # コメントのある行だけ
              self.line2Tkinter(line_num, line)
           line_num+=1

    def find(self,string): ##
        try: ## fileを開いて検索
           with open(self.path, 'r', encoding='utf-8') as f:
              text = f.read()

           # textを改行で分割
           text = text.split('\n')

           #print("textを1行ずつlineMdToTkinterに渡す")
           line_num=1
           for line in text: ##
              if re.search(string,line): # stringを含む行だけ ##
                 self.line2Tkinter(line_num, line) ##
              line_num+=1

           #self.canvas.postscript(file='canvas.ps',colormode='color') 
        except:
           print("fileを開けません")

    def line2Tkinter(self,line_num, line): ##
        #print("構造化パターンマッチングを使って、textを解析")
        #print("labelを作成")
        pad_val=0 # label間の隙間
        line_frame=tk.Frame(self.frame,bg=odd_line_bg)
        num=str("%4d: " % line_num) 
        m=re.match(' +',num)
        if m and m.end(0)>1:
           space_num=m.end(0) -1
        else:
           space_num=0
        num_space=ttk.Label(line_frame,width=space_num,padding=pad_val,background=odd_line_num_bg)
        num_label = ttk.Label(line_frame, text=num, 
                font=("Noto Sans CJK JP", 10), justify="left", padding=pad_val, 
                compound="right", 
                foreground=odd_line_num_fg, background=odd_line_num_bg)
        m=re.match(' +',line)
        if m :
           space_num=m.end(0)
        else:
           space_num=0
        #print(space_num)
        space=ttk.Label(line_frame,width=space_num,padding=pad_val,background=odd_line_bg)
        line=line.strip(' +')
        line=line.replace("##","")
        line_label = ttk.Label(line_frame, text=line, font=("Noto Sans CJK JP", 10), 
                justify="left", padding=pad_val, compound="right",
                foreground=odd_line_fg, background=odd_line_bg)
        line_label.bind("<ButtonPress-4>", self.yup_scrolling,"+")
        line_label.bind("<ButtonPress-5>", self.ydown_scrolling,"+")
       
        num_space.pack(side='left')  ##
        num_label.pack(side='left')  ##
        space.pack(side='left')  ##
        line_label.pack(side='left',expand=True,fill='x') ##
        line_frame.pack(anchor=tk.NW,expand=True,fill='x') ##

