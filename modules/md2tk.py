# cf. https://zenn.dev/foxfoxfox/articles/be23e942cf3192
import os, re
import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
import webbrowser

text_fg="#204030"
#text_bg="#d0e0d0"
text_bg="#e8f8e8"
mrkdwn_bg="#b0c0b0"
howto_bg="#fafaa0"

imgs=[]
class Markdown2Tkinter:
    def __init__(self, path, root):
        #super().__init__(root)
        for widget in root.winfo_children():
           widget.pack_forget()

        self.asisbox=False # ``` で囲まれた範囲内を示す

        # winfo_height、winfo_widthでmasterのウィンドウ情報を取得
        self.height = root.winfo_height()
        self.width = int(root.winfo_width()*0.95)
    
        # Canvasを親とした縦横方向のScrollbar
        self.canvas=tk.Canvas(root,width=580,height=600,bg=text_bg)
        #self.canvas.configure(bg=text_bg)
        self.canvas.propagate(True)


        self.frame=tk.Frame(self.canvas)
        self.frame.configure(bg=text_bg)
        #self.frame.propagate(True)
        self.frame.bind("<Configure>", lambda e: self.canvas.configure(scrollregion=self.canvas.bbox("all")))

        #self.frame.pack(anchor=tk.NW)
        self.canvas.create_window((0, 0), window=self.frame, anchor="nw")


        self.sbar_v = ttk.Scrollbar(root, orient=tk.VERTICAL)
        self.sbar_v.config(command=self.canvas.yview)
        self.sbar_v.pack(side='right', fill=tk.Y)
        self.canvas.configure(yscrollcommand=self.sbar_v.set)

        scrollbar_h = ttk.Scrollbar(root, orient=tk.HORIZONTAL)
        scrollbar_h.config(command=self.canvas.xview)
        scrollbar_h.pack(side='bottom', fill=tk.X)
        self.canvas.configure(xscrollcommand=scrollbar_h.set)

        self.canvas.pack(side=tk.LEFT,expand=True,fill=tk.BOTH)

        self.markdown2Tkinter(path, self.frame) # Canvasの中身Frameを作る

        self.frame.bind("<ButtonPress-1>", self.yup_scrolling,"+")
        self.frame.bind("<ButtonPress-3>", self.yup_scrolling,"+")

        self.frame.bind("<ButtonPress-4>", self.yup_scrolling,"+")
        self.frame.bind("<ButtonPress-5>", self.ydown_scrolling,"+")

        

    def yup_scrolling(self,event):
        self.canvas.yview_scroll(-1, 'units')

    def ydown_scrolling(self,event):
        self.canvas.yview_scroll(+1, 'units')
    
    def open_browser(self,url):
        webbrowser.open_new(url)

    def markdown2Tkinter(self, path, frame):
        frame.bind("<ButtonPress-4>", self.yup_scrolling,"+")
        frame.bind("<ButtonPress-5>", self.ydown_scrolling,"+")
        # markdownファイルの読み込み
        with open(path, 'r', encoding='utf-8') as f:
            text = f.read()

        # textを改行で分割
        text = text.split('\n')

        tkObject = []

        #print("textを1行ずつlineMdToTkinterに渡す")
        for line in text:
            if self.asisbox:
                if '```' in line: # asisbox 終わり
                    self.asisbox=False
                else:
                    tkObject.append(self.asisbox2Tkinter(line, frame))
            else:
                if '```' in line: # asisbox 始まり
                    self.asisbox=True
                else:
                    tkObject.append(self.line2Tkinter(line, frame, path))

        # tkObjectの要素を1つずつframeに追加
        #for i in tkObject:
        #    i.pack(anchor="nw")

    def asisbox2Tkinter(self, line, frame):
        line="　"+line
        label = tk.Label(frame, text=line, anchor=tk.NW, font=("Noto Sans CJK JP", 10), justify="left")
        label.configure(bg=text_fg,fg=text_bg)
        label.bind("<ButtonPress-4>", self.yup_scrolling,"+")
        label.bind("<ButtonPress-5>", self.ydown_scrolling,"+")
        label.pack(anchor=tk.NW,expand=True,fill='x')
        return label

    def line2Tkinter(self, line, frame, path):
        #print("構造化パターンマッチングを使って、textを解析")
        StrRe(line)
        match StrRe(line):
            # lineが#+半角スペースで始まる場合
            case "#+ ":
                #print("lineが#で始まる場合、#の数を取得")
                count = line.count("#")
                # lineから#を削除
                line = line.replace("#", "")
                # lineの前後の空白を削除
                line = line.strip()
                if count==2:
                    line="■"+line
                elif count==3:
                    line="❏"+line
                label = ttk.Label(frame, text=line, font=("Noto Sans CJK JP", 7 + (6 - count) *2))
                label.configure(background=text_bg,foreground=text_fg,relief="flat",padding=3)
                label.bind("<ButtonPress-4>", self.yup_scrolling,"+")
                label.bind("<ButtonPress-5>", self.ydown_scrolling,"+")
                label.pack(anchor=tk.NW,expand=True,fill='x')
                return label
            
            # lineが半角スペース*+*+半角スペースで始まる場合
            case "^ *\* ":
                print("lineが*で始まる場合、*を・に置換")
                line = line.replace("*", "・")

                # labelを作成
                label = tk.Label(frame, text=line, font=("Noto Sans CJK JP", 10), justify="left")
                label.pack(anchor=tk.NW,expand=True)
                return label

            case "---":
                #print("lineが---で始まる場合、lineを作成")
                #canvas = tk.Canvas(frame, width=500, height=1, background="gray30", highlightthickness=0)
                canvas = tk.Canvas(frame, height=1, background="gray30", highlightthickness=0)
                return canvas

            case r"!\[":
                #print("lineが![で始まる場合、()の中身を取得")
                line = re.findall(r'\((.*?)\)', line)[0].replace(".\\", "")
                dir_name=path[:path.rfind('/')]
                filePath=dir_name+"/"+line
                #print(filePath)
                while not os.path.exists(filePath): # ファイルが存在するディレクトリまで遡る
                    dir_name=dir_name[:dir_name.rfind('/')]
                    filePath=dir_name+"/"+line
                    #print(filePath)

                try: # 画像表示
                    # filePathから画像を読み込み
                    readImage = Image.open(filePath)
                    width=self.width
                    height = int(readImage.height/readImage.width * width)
                    #print(width,height)
                    #global imgs # ガーベージコレクションでメモリから開放されないため必要
                    padding=3
                    showImage = readImage.resize((width-3*padding,height))
                    imagetk = ImageTk.PhotoImage(showImage,master=frame)
                    imgs.append(imagetk) # ガーベージコレクションでメモリから開放されないためリストに追加

                    # canvasを作成
                    canvas = tk.Canvas(frame, bg=text_bg, width=showImage.width+padding, height=showImage.height)

                    # canvasに画像を描画
                    canvas.create_image(padding, 0, anchor=tk.NW, image=imagetk)
                    canvas.bind("<ButtonPress-4>", self.yup_scrolling,"+")
                    canvas.bind("<ButtonPress-5>", self.ydown_scrolling,"+")
                    canvas.pack(anchor=tk.NW,expand=True)
                    return canvas
                except: # 動画？
                    line="　"+line
                    label = tk.Label(frame, text=line, font=("Noto Sans CJK JP", 10), justify="left")
                    label.configure(bg=text_bg,fg=text_fg)
                    label.bind("<ButtonPress-4>", self.yup_scrolling,"+")
                    label.bind("<ButtonPress-5>", self.ydown_scrolling,"+")
                    label.pack(anchor=tk.NW,expand=True)
                    return label

            case r"\[": # web link
                if "\\[" in line:
                    line="　"+line
                    label = tk.Label(frame, text=line, font=("Noto Sans CJK JP", 10), justify="left")
                    label.configure(bg=text_bg,fg=text_fg)
                    label.bind("<ButtonPress-4>", self.yup_scrolling,"+")
                    label.bind("<ButtonPress-5>", self.ydown_scrolling,"+")
                    label.pack(anchor=tk.NW,expand=True)
                    return label

                else:
                    pass

                # ()の中身を取得"
                url = re.findall(r'\((.*?)\)', line)[0].replace(".\\", "")
                print(url)
                # []の中身を取得"
                link = re.findall(r'\[(.*?)\]', line)[0].replace(".\\", "")
                print(link)
                label = tk.Label(frame, text=link, font=("Noto Sans CJK JP", 10), justify="left", cursor="hand2")
                label.configure(bg=text_bg,fg='DodgerBlue')
                label.bind("<ButtonPress-1>", lambda e: self.open_browser(url))
                label.bind("<ButtonPress-4>", self.yup_scrolling,"+")
                label.bind("<ButtonPress-5>", self.ydown_scrolling,"+")
                label.pack(anchor=tk.NW,expand=True)
                return label

            case _:
                #print("labelを作成")
                line="　"+line
                #label = ttk.Label(frame, text=line, font=("Noto Sans CJK JP", 10), justify="left")
                #label.configure(background=mrkdwn_bg,foreground=text_fg,padding=-1)
                label = tk.Message(frame, text=line, font=("Noto Sans CJK JP", 10))
                label.configure(width=self.width, background=text_bg,foreground=text_fg,pady=-2)
                label.bind("<ButtonPress-4>", self.yup_scrolling,"+")
                label.bind("<ButtonPress-5>", self.ydown_scrolling,"+")
                label.pack(anchor=tk.NW,expand=True)
                return label

class StrRe(str):
    def __init__(self, var):
        self.var = var
        pass

    def __eq__(self, pattern):
        return True if re.search(pattern, self.var) is not None else False

global image
