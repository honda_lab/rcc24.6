#!/usr/bin/python3

# rc4mouse.py
# Control left and right motors by a mouse
# Yasushi Honda 2022 6/17

import modules.keyin as keyin # キーボード入力を監視するモジュール
import modules.ssr3_motor as mt # pwmでモーターを回転させるためのモジュール
import numpy as np
import time

class Assign():

   def __init__(self):
      self.mL=mt.Lmotor(17)
      self.mR=mt.Rmotor(18)
      self.csv=mt.Servo(14)
      self.left=0
      self.right=0
      self.angl=0

   def update(self,Rx,Ry):

      gainX=2.0
      gainY=2.0

      # Controll by PS3 controller
      self.left=gainY*Ry+gainX*Rx
      self.right=gainY*Ry-gainX*Rx

      self.angl=0.0
      Run(self.mL,self.mR,self.csv,self.left,self.right,self.angl)
      #print("\r %4d %4d %4d" % (self.left,self.right,self.angl),end='')

      return self.left, self.right

   def stop(self):
      self.mL.run(0)
      self.mR.run(0)
      self.csv.move(0)

def Run(mL,mR,sv,left,right,angl):
   if left<-100: left = -100
   if left>100: left = 100
   mL.run(left)
   if right<-100: right = -100
   if right>100: right = 100
   mR.run(right)
   if angl>120: angl=120
   if angl<-120: angl=-120
   sv.move(angl)

