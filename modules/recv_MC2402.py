#!/usr/bin/python3
# MC_recv class をつくって，他プログラムから使えるように改造
# Python3に対応
# CC BY-SA Yasushi Honda 2024 2/8
# recieve_ver5.py
# CC-BY-SA T.Nomura 2017 11/27
import time
import socket
import datetime
import struct
from datetime import datetime
import re

HOST = '10.1.1.53'    # 自分(受信側)のIPアドレス
PORT = 1001
#multicast_group='224.0.0.1' 
multicast_group='225.1.1.1'

class MC_recv():
   def __init__(self):
      self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
      # 繰り返して同じ(HOST,PORT)を使うために，SO_REUSEADDRオプションが必要
      try:  ## ソケット作成
         self.sock.bind(('', PORT))
         self.sock.setsockopt(socket.IPPROTO_IP,
               socket.IP_ADD_MEMBERSHIP,
               socket.inet_aton(multicast_group)+socket.inet_aton(HOST))
      except:
         print("socket can not be binded. Check your Network setting.")

      self.name_length=[]
      self.marker_length=[]
      self.marker_kazu=[]

   def start(self,f):
      
      #固定する変数
      cut = 12
      offset = 12
      data = self.sock.recv(1024)
      print(data)
      d = datetime.now()
      cut_data = data

      name_loop = data.count(b"\00\05\00\00\00")
      marker_loop = data.count(b"\01\00\00\00")
      if name_loop >= marker_loop:
         loop = marker_loop
      else:
         loop = name_loop

      for x in range(loop):
         cut_data = cut_data[int(cut):]
         num_name = cut_data.find(b"\00\05\00\00\00") + 1
         self.name_length += [num_name - 1]

         num_marker = cut_data.find(b"\01\00\00\00") + 1
         self.marker_length += [num_marker - num_name - 5]
         marker = int(self.marker_length[x] / 4 )
         self.marker_kazu += [marker]

         cut = num_marker + 63

      f.write("#################################################\n")
      for x in range(loop):
         name = struct.unpack_from("<{}sx".format(self.name_length[x]),data,offset)
         name = re.sub('[\'(),]','',str(name))
         f.write(str(name) + " ")
         offset += int(self.name_length[x]) + int(self.marker_length[x]) + 69

      f.write("\n#################################################\n\n")

      return data, loop

   def recv(self,data,loop,f):
      offset = 12
      for x in range(loop):
         name = struct.unpack_from("<{}sx".format(self.name_length[x]),data,)
         offset += int(self.name_length[x]) + 5
         xyz = struct.unpack_from("<{}fx".format(self.marker_kazu[x]),data,offset)

         list_xyz = list(xyz)
         #int_xyz = map(round, list_xyz, [2]*len(list_xyz))
         int_xyz = re.sub('[\[+\],]','',str(list_xyz))
         f.write(str(int_xyz) + '\n')
         print(str(int_xyz))
         offset += int(self.marker_length[x]) + 64
         data = self.sock.recv(1024)   
         # 1024バイトだけデータを受け取っているので、マーカーが増えた場合には受け取るデータ量
         # を増やす必要がある
 
      return data

   def close(self):
      self.sock.close()


if __name__=="__main__":
   f = open('20240208_test_take1.csv', 'w')     #座標データを出力するファイル

   mc=MC_recv() 
   data, loop = mc.start(f)
   d = datetime.now()
   while 1:
      date_str = d.strftime("%H %M %S.%f ")	
      f.write(date_str)
      data = mc.recv(data,loop,f) # MCデータ受信
      d = datetime.now()

   mc.close()
