import colorsys
class ColorSensor:
    def __init__(self, ser):
        self._ser = ser
        self._mode_dic = {"Ambient":1, "Color":2, "Reflect":3, "RGB":4}
        self._ambient_val = 0
        self._color_val = 0
        self._reflect_val = 0
        self._rgb_val = [0, 0, 0]

    def change_mode(self, mode):
        if (mode in self._mode_dic) == True:
            self._ser.send(61, self._mode_dic[mode])
        else:
            print("Invalid ColorSensor Mode!")

    def set_ambient_val(self, value):
        self._ambient_val = value

    def set_color_val(self, value):
        self._color_val = value

    def set_reflect_val(self, value):
        self._reflect_val = value

    def set_r_val(self, value):
        self._rgb_val[0] = value

    def set_g_val(self, value):
        self._rgb_val[1] = value

    def set_b_val(self, value):
        self._rgb_val[2] = value

    def get_ambient_val(self):
        return self._ambient_val

    def get_color_val(self):
        return self._color_val

    def get_reflect_val(self):
        return self._reflect_val

    def get_r_val(self):
        return self._rgb_val[0]

    def get_g_val(self):
        return self._rgb_val[1]

    def get_b_val(self):
        return self._rgb_val[2]
        
    def get_h_val(self):
        return colorsys.rgb_to_hsv(self._rgb_val[0]/255, self._rgb_val[1]/255, self._rgb_val[2]/255)[0] * 360
    
    def get_s_val(self):
        return colorsys.rgb_to_hsv(self._rgb_val[0]/255, self._rgb_val[1]/255, self._rgb_val[2]/255)[1] * 100
    
    def get_v_val(self):
        return colorsys.rgb_to_hsv(self._rgb_val[0]/255, self._rgb_val[1]/255, self._rgb_val[2]/255)[2] * 100

    def notify_modechange(self):
        print("ColorSensor Mode is Changed!")

class TouchSensor:
    def __init__(self):
        self._is_Center_pressed = 0
        self._is_R_pressed = 0
        self._is_L_pressed = 0

    def set_value(self, value):
        if value == 0:
            self._is_Center_pressed = 0
            self._is_R_pressed = 0
            self._is_L_pressed = 0
        elif value == 1:
            self._is_Center_pressed = 0
            self._is_R_pressed = 0
            self._is_L_pressed = 1
        elif value == 2:
            self._is_Center_pressed = 0
            self._is_R_pressed = 1
            self._is_L_pressed = 0
        elif value == 3:
            self._is_Center_pressed = 0
            self._is_R_pressed = 1
            self._is_L_pressed = 1
        elif value == 16:
            self._is_Center_pressed = 1
            self._is_R_pressed = 0
            self._is_L_pressed = 0
        elif value == 17:
            self._is_Center_pressed = 1
            self._is_R_pressed = 0
            self._is_L_pressed = 1
        elif value == 18:
            self._is_Center_pressed = 1
            self._is_R_pressed = 1
            self._is_L_pressed = 0
        elif value == 19:
            self._is_Center_pressed = 1
            self._is_R_pressed = 1
            self._is_L_pressed = 1
        else:
            print("invalid touch sensor value!  value = {}".format(value))

    def get_center_value(self):
        return self._is_Center_pressed

    def get_right_value(self):
        return self._is_R_pressed

    def get_left_value(self):
        return self._is_L_pressed

class UrtraSonicSensor:  # モードごとに何の値を取得しているか不明。
    def __init__(self, ser):
        self._ser = ser
        self._distance = 0  # 1つ目のモード用の変数
        self._value2 = 0  # 2つ目のモード用の変数
        self._mode_dic = {"Distance": 1, "Dammy": 2}

    def change_mode(self, mode):
        if (mode in self._mode_dic) == True:
            self._ser.send(62, self._mode_dic[mode])
        else:
            print("Invalid UrtraSonicSensor Mode!")

    def set_distance(self, value):
        self._distance = value

    def set_value2(self, value):
        self._value2 = value

    def get_distance(self):
        return self._distance

    def get_value2(self):
        return self._value2

    def notify_modechange(self):
        print("UltraSonicSensor Mode is Changed!")

class GyroSensor:
    def __init__(self, ser):
        self._ser = ser
        self._yaw = 0
        self._value2 = 0

    def set_yaw(self, value):
        self._yaw = value

    def set_value2(self, value):
        self._value2 = value

    def get_yaw(self):
        return self._yaw
    
    def get_value2(self):
        return self._value2
    
    def yaw_reset(self):
        self._ser.send(13, 1)

    def notify_reset(self):
        print("GyroSensor is resetted")

    def notify_modechange(self):
        print("GyroSensor Mode is Changed!")

class Battery:
    def __init__(self):
        self.voltage = 0
        self.current = 0

    def set_voltage(self, value):
        self.voltage = value

    def set_current(self, value):
        self.current = value

    def get_voltage(self):
        return self.voltage
    
    def get_current(self):
        return self.current
