import serial
import time

class ser:
    # それぞれのモーター・センサーごとにオブジェクトを作り、
    # そこにspikeから受け取った値を勝手に格納していくクラス
    def __init__(self):
        self._ser = serial.Serial('/dev/ttyAMA5', 115200)

        self._SEND_RATE = 0.0011  # データ送信の間隔
        self._MINUS = 0x20  # マイナスの値を送信するときに使う。2進数表記だと00100000

    def device_set(self, motorL = None, motorR = None, motorCenter = None,
                 color_sensor = None, touch_sensor = None,
                 ultrasonic_sensor = None, gyro_sensor = None):
        self._motorL = motorL
        self._motorR = motorR
        self._motorCenter = motorCenter
        self._color_sensor = color_sensor
        self._touch_sensor = touch_sensor
        self._ultrasonic_sensor = ultrasonic_sensor
        self._gyro_sensor = gyro_sensor

    def ser_close(self):
        self._ser.close()

    def send(self, cmd, value):
        # spike側の受信構造の仕様上、コマンドとして送れるのは0~127まで
        if  0 <= cmd and cmd <= 127:
            cmd = cmd | 0x80  # コマンドの先頭ビットを1にする
            self._ser.write(bytes([cmd]))
            time.sleep(self._SEND_RATE)
        else:
            print("invalid send cmd.")
            return

        #print("value = {}".format(value))

        # spike側の受信構造の仕様上、値として送れるのは-2047~2047まで
        if -2047 <= value and value < 0:
            value *= -1
            # 値を送信用に2つに分ける。
            send_data1 = (value & (0x1F << 7)) >> 7  # 上位4桁目から8桁目まで
            send_data2 = value & 0x7F  # 末尾から7桁
            #print("value = {}".format(value))
            #print("data1 = {}, data2 = {}".format(send_data1, send_data2))
            self._ser.write(bytes([send_data1 | self._MINUS]))
            time.sleep(self._SEND_RATE)
            self._ser.write(bytes([send_data2]))
            time.sleep(self._SEND_RATE)
        elif 0 <= value and value <= 2047:
            # 値を送信用に2つに分ける。
            send_data1 = (value & (0x1F << 7)) >> 7  # 上位4桁目から8桁目まで
            send_data2 = value & 0x7F  # 末尾から7桁
            #print("value = {}".format(value))
            #print("data1 = {}, data2 = {}".format(send_data1, send_data2))
            self._ser.write(bytes([send_data1]))
            time.sleep(self._SEND_RATE)
            self._ser.write(bytes([send_data2]))
            #print("data1 = {}, data2 = {}".format(send_data1, send_data2))
            time.sleep(self._SEND_RATE)
        else:
            print("invalid send value.")

    # 受け取ったコマンドに対応するモーター・センサーに値を格納
    def set_val_to_obj(self, cmd, value):
        if cmd == 4:
            if self._color_sensor != None:  # R値
                self._color_sensor.set_r_val(value)
        elif cmd == 5:
            if self._color_sensor != None:  # B値
                self._color_sensor.set_g_val(value)
        elif cmd == 6:
            if self._color_sensor != None:  # G値
                self._color_sensor.set_b_val(value)
        elif cmd == 1:  # 周辺光
            if self._color_sensor != None:
                self._color_sensor.set_ambient_val(value)
        elif cmd == 2:  # 色
            if self._color_sensor != None:
                self._color_sensor.set_color_val(value)
        elif cmd == 3:
            if self._color_sensor != None:  # 反射光
                self._color_sensor.set_reflect_val(value)
        elif cmd == 22:
            if self._ultrasonic_sensor != None:  # 超音波センサーその１（未完成）
                self._ultrasonic_sensor.set_distance(value)
        elif cmd == 23:
            if self._ultrasonic_sensor != None:  # 超音波センサーその２（未完成）
                self._ultrasonic_sensor.set_value2(value)
        elif cmd == 64:
            if self._motorCenter != None:  # 中央モーター回転数
                self._motorCenter.set_rotate(value)
        elif cmd == 65:
            if self._motorR != None:  # 右モーター回転数
                self._motorR.set_rotate(value)
        elif cmd == 66:
            if self._motorL != None:  # 左モーター回転数
                self._motorL.set_rotate(value)
        elif cmd == 7:
            if self._gyro_sensor != None:  # yaw角
                self._gyro_sensor.set_yaw(value)
        elif cmd == 8:
            if self._gyro_sensor != None:  # 多分yaw角の角速度？
                self._gyro_sensor.set_value2(value)
        elif cmd == 28:  # EV3用タッチセンサー。使わないかも
            pass
        elif cmd == 9:
            if self._motorCenter != None:  # 中央モーターのリセット通知
                self._motorCenter.notify_resetted()
        elif cmd == 10:
            if self._motorCenter != None:  # 右モーターのリセット通知
                self._motorR.notify_resetted()
        elif cmd == 11:
            if self._motorCenter != None:  # 左モーターのリセット通知
                self._motorL.notify_resetted()
        elif cmd == 61:
            if self._color_sensor != None:  # カラーセンサーのモード変更通知
                self._color_sensor.notify_modechange()
        elif cmd == 62:
            if self._gyro_sensor != None:  # 超音波センサーのモード変更通知
                self._ultrasonic_sensor.notify_modechange()
        elif cmd == 13:
            if self._gyro_sensor != None:  # ジャイロセンサーのリセット通知
                self._gyro_sensor.notify_reset()
        elif cmd == 63:
            if self._gyro_sensor != None:  # ジャイロセンサーのモード変更通知
                self._gyro_sensor.notify_modechange()
        elif cmd == 5:
            if self._motorCenter != None:  # 中央モーターの停止通知
                self._motorCenter.notify_stopped()
        elif cmd == 6:
            if self._motorCenter != None:  # 右モーターの停止通知
                self._motorR.notify_stopped()
        elif cmd == 7:
            if self._motorCenter != None:  # 左モーターの停止通知
                self._motorL.notify_stopped()
        elif cmd == 29:  # バッテリー残量の通知
            print("current battery... {}".format(value))
        elif cmd == 30:  # バッテリー電圧の通知
            print("battery voltage... {}".format(value))
        elif cmd == 0:
            if self._touch_sensor != None:  # 本体ボタン
                self._touch_sensor.set_value(value)
        else:  # その他コマンド。間違ったコマンドや通信ミスでおかしくなったコマンド
            print("received command is invalid!   cmd = {}".format(cmd))

    # シリアル通信でspikeからコマンドと値を受け取る
    # 現時点でraspi側のデータ受信には休止を入れてない。受け取ったデータが壊れるなら各自設定すること
    def receive(self):
        start = time.time()
        count = 0
        while True:
            if self._ser.in_waiting:
                try:
                    # コマンドを1行読み取り、bytes文字列から通常の文字列へ変換(デコード)している。
                    # また、rstrip()で改行文字列を削除している。
                    cmd = self._ser.readline().decode().rstrip()
                except UnicodeDecodeError:
                    # デコード失敗時はエラーの旨を表示して制御を手放す
                    print("decode error!")
                else:  # 受け取ったコマンドと値を実際に扱う形に変換
                    if cmd.find("@") != -1:
                        value = int(cmd.split("@")[1].split(":")[1])
                        cmd = int(cmd.split("@")[1].split(":")[0])
                        self.set_val_to_obj(cmd, value)
                    elif cmd.find("<") != -1:
                        value = int(cmd.split("<")[1].split(":")[1])
                        cmd = int(cmd.split("<")[1].split(":")[0])
                        self.set_val_to_obj(cmd, value)
                    else:
                        print("invalid cmd. read again...")
            else:  # データが届いていない場合はsleepを挟む
                time.sleep(0.0001)

    def skip(self):
        while True:
            if self._ser.in_waiting:
                self._ser.read_all()
            else:
                break
