## Robot Command Center (RCC)
ロボットとは，ネットワーク上にあるコンピューターであると言いかえることもできます．このため，
自分の手元にない（ディスプレイもキーボードもない）コンピューター上でプログラムを開発する必
要性が生じます．ネットワークアドレスの設定やファイルの転送など，ローカル環境では生じない煩
雑さが伴います．

本プロジェクト(RCC)はプログラムの編集とテスト，およびリモート実行を支援する開発環境
GUIを提供します．
端末からsshなどでリモートホストにログインすることなく，GUIを用いてプログラムのイン
タラクティブな実行や停止が可能となります．
そのため，本来必要なアルゴリズムの構築やプログラムの開発に集中することが可能になり
ます．

### スクリーンショット
![RCC](pics/rcc246.png)

## インストール
このRCCはPython Tkinterと，下記の一般的なソフトウェアツールを用いて構築されていま
す．

```
sudo apt-get install aptitude python3-tk python3-matplotlib python3-timeout-decorator python3-opencv
```
と実行します．
aptitude は Debianのaptシステムを使いやすくしてくれるコマンドなので，aptなどと併用
しても問題なく便利なコマンドです．

RCCは外部エディターやファイルマネージャを呼び出して実行するので，
まだインストールされていない場合は下記のようにインストールしてください．
```
sudo apt-get install mate-terminal vim-gtk3 gedit caja git
```
その他にも必要なモジュールが追加されるかもしれません．ターミナルから "rccXXXX.py"を
実行すると，モジュールが不足している場合は，エラーメッセージが教えてくれるので，
それに従いましょう．

## 実行
端末(コンソールshell)から，
```
   python3 rccXXXX.py 
```
と実行します．XXXXの部分はヴァージョンを表します．
必要なmoduleなどが足りない場合はエラーメッセージが教えてくれるので，
それらをインストールしましょう．

## gvimをIDE的に
gvimのプラグインを使うと，開発環境(IDE)的にgvimを使えます．

### gvimのプラグインをインストールするために
curlがまだインストールされていない場合は，
```
sudo apt install curl
```

vim-plugをユーザー権限でインストール．
```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
これをroot(su)で実行してしまうと，ユーザーとしてvim-plugが利用できないので注意．

### ディレクトリツリー表示プラグイン NERDTree
.gvimを自分のホームディレクトリにコピー後，:PlugInstall
を実行すると，NERDTree(ディレクトリツリー）表示できます．
Ctrl-nでツリーの表示・非表示を切り替えられます．


### 小技：gvimのマップ機能
.gvimrc に 
```
map <C-p> :PlugInstall<CR>
map <C-t> :NERDTreeToggle<CR>
map <C-i> :IndentGuidesEnable<CR>
map <C-d> :IndentGuidesDisable<CR>
map <C-e> :!python3 % <CR>
```
と記述してあります．
gvim実行中にたとえば，Ctrl-iを実行すると，Indent Guides表示されます．
このようにgvimのショートカットを.gvimrcの中に定義しておけば，より効率的な
プログラム開発が可能です．


## GPUでNNの学習をするために，ubuntu 22.04 へのcudaのインストール
[cudaのインストール](https://zenn.dev/pon_pokapoka/articles/nvidia_cuda_install)



## 構成の概略図
<img src="https://gitlab.com/honda_lab/etrobocon23/-/raw/main/pics/abstract.jpg" width=700>
