#!/usr/bin/python3
#ロボットを操縦する制御パネル．
# CC BY-SA Yasushi Honda 2023 10/11

import time
import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os
import sys
import cv2
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from threading import Thread
import tabs.mskympl as Msky
import modules.mysocket as sk
import socket

mouse_over_color="DarkOliveGreen1"
common_bg="orange"
text_fg="#204030"
text_bg="#d0e0d0"


def construct(root, local_addr, robot_addr, calc_addr):
    # Frameの定義と配置
    canvas_frame = tk.Frame(root, relief=tk.RAISED, bd=0, bg=common_bg)
    button_frame = tk.Frame(root, relief=tk.RAISED, bd=0, bg=common_bg)
    text_frame = tk.Frame(root, relief=tk.RAISED, bd=0, bg=common_bg)

    canvas_frame.pack(fill="both",expand=True)
    button_frame.pack(fill="both",expand=True)
    text_frame.pack(fill="both",expand=True)

    def mouse_leave(event):
        text_editor.configure(fg=text_fg,bg=text_bg)
        #text_editor.delete("1.0",tk.END)
   
    global record_state
    record_state="not"

    global recv_state
    recv_state="off"
    global vw
    # 画像の受信・マウス状態のロボットへの送信・テキストエリアへのリアルタイム表示の
    # ためのスレッド実行関数
    global cam_udp, mouse_udp, quit_udp, str_udp
    def continuous_recv():
        global cam_udp, mouse_udp, quit_udp, str_udp, calc_udp
        global recv_state
        global vw
        global rate
      
        data=[]
        PERIOD=0.1 # リモートにデータを送る周期．小さすぎるとリモートでレイテンシ注意
        now=time.time()
        start=now
        init=now
        cnt=0
        ch='c'
        while recv_state=="on":
            try:
                recv_frame=cam_udp.recv_img()
                frame=cv2.resize(recv_frame,(320,240))
                if image!=None:
                    #pltで画像表示用にRとB要素を入れ替える．
                    pmt_img[:,:,0]=frame[:,:,2]
                    pmt_img[:,:,1]=frame[:,:,1]
                    pmt_img[:,:,2]=frame[:,:,0]
                    image.set_array(pmt_img) # 高速fpsを出すためにpltの画像データだけをセット．

                if record_state=="rec":
                    vw.write(frame)

                cnt+=1
            except (BlockingIOError, socket.error):
                pass

            now=time.time()
            if now-start>PERIOD:
                rate=cnt/PERIOD
                mx,my,wheel,ch,run=msky.get_value()
                msky.reset_key() # chの値を' 'に戻す
                data.append(mx)
                data.append(my)
                data.append(run)
                data.append(wheel)
                mouse_udp.send(data)
                data.clear()
                line=str("%7.1f" % (now-init)) + ", " + str("%7.1f" % rate) + ", " + \
                str("%7.1f" % mx)+", "+str("%7.1f" % my)+", "+str(ch)+", "+str(record_state)
                text_editor.delete("1.0","1.end")
                text_editor.insert("1.0", "  time,   rate,    mx,      my")
                text_editor.delete("2.0","2.end")
                text_editor.insert("2.0", line)
                text_editor.delete("3.0", "3.end")
                text_editor.insert("3.0", "------------------------------")

                str_udp.send_str(ch) # レイトを落として周期的にchを送信
                quit_udp.send_str(ch) 
                calc_udp.send_str(ch) 
                start=now
                cnt=0

        cam_udp.close()
        mouse_udp.close()
        quit_udp.close()
        str_udp.close()
        calc_udp.close()
        msky.close()

    global rate
    def pnl_record():
        global record_state
        global vw
        global rate

        home_dir=os.path.expanduser("~")
        if record_state=="not": 
            OUT_FILE=home_dir+"/Execute/from_picam.mp4"
            fmt = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
            record_fps=rate
            vw = cv2.VideoWriter(OUT_FILE, fmt, record_fps, size)
            record_state="rec"
            pnl_record_button.config(bg='red',fg='white',text='録画中')
        else:
            record_state="not"
            pnl_record_button.config(bg='white',fg='black', text='録画on/off')
            vw.release()        
      

    # コントロールパネルを起動する関数
    def recv_onoff():
        global recv_state
        global cam_udp, mouse_udp, quit_udp,str_udp, calc_udp
        if recv_state=="off":
            try:
                cam_udp=sk.UDP_Recv(local_addr,sk.cam_port)
                mouse_udp=sk.UDP_Send(robot_addr,sk.mouse_port)
                quit_udp=sk.UDP_Send(robot_addr,sk.quit_port)
                str_udp=sk.UDP_Send(robot_addr,sk.str_port)
                calc_udp=sk.UDP_Send(calc_addr,sk.calc_port)
            except:
                text_editor.insert("0.0","\n udp failed \n")
            time.sleep(0.2)
            # paneru監視スレッド実行
            recv_state="on"
            Thread(target=continuous_recv, daemon=True).start()
            recv_onoff_button.config(text="受信 OFF",bg="yellow")

        elif recv_state=="on":
            recv_state="off"
            recv_onoff_button.config(text="受信 ON",bg='white')

    def destroy():
        #cmd="pkill cpanel"
        #proc=subprocess.Popen(cmd.strip().split(' '))
        # フレーム閉じる
        root.quit()
        root.destroy()

    recv_onoff_button = tk.Button(button_frame, activebackground='DarkOliveGreen1', bg='white', fg='black', text="受信 on/off", padx=0, command=recv_onoff,borderwidth=4)
    recv_onoff_button.pack(side="right")

    pnl_record_button = tk.Button(button_frame, bg='white', fg='black', text="録画on/off", padx=0, command=pnl_record,borderwidth=4)
    pnl_record_button.pack(side="right")

    close_button = tk.Button(button_frame,text="閉じる", command=destroy)
    close_button.pack(side="right")


    # ------ Control Plane --------
    max=100
    fig, ax = plt.subplots()
    #fig.set_figwidth(6.0)    # 4   7
    #fig.set_figheight(4.0)   # 3  5.25
    ax.set_facecolor('pink') 
    fig.canvas.manager.set_window_title("ロボット・コントロールパネル")
    plt.text(-0.98*max,0.90*max,"LEFT click:  Start/Keep",fontsize=13,color="yellow")
    plt.text(-0.98*max,0.80*max,"CENTER click: Reset",fontsize=13,color="yellow")
    plt.text(-0.98*max,0.70*max,"RIGHT click: Quit",fontsize=13,color="yellow")
    plt.subplots_adjust(left=0, right=1, bottom=0, top=1)
    plt.ion()  # Turn the interactive mode on.
    ax.set_xlim(-max,max)
    ax.set_ylim(-max,max)
    xlim=ax.get_xlim()
    ylim=ax.get_ylim()
    #plt.pause(0.001)

    # plt.canvas
    width=320
    height=240
    size = (width, height)
    show=np.random.randn(width,height)
    image=plt.imshow(show,extent=[*xlim,*ylim], aspect='auto',alpha=0.8)
    canvas = FigureCanvasTkAgg(fig, master=canvas_frame)
    canvas.get_tk_widget().pack(side="top",expand=True,fill="both")
    #canvas.get_tk_widget().grid(columnspan=7,row=1, column=0)
    pmt_img=np.zeros((size[1], size[0], 3), np.uint8)
# -----------------------------

    #text_editor = scrolledtext.ScrolledText(text_frame,bg=text_bg,fg=text_fg,width=100,height=7,padx=20,pady=20)
    text_editor = scrolledtext.ScrolledText(text_frame,bg=text_bg,fg=text_fg,height=4,padx=20,pady=20)
    #text_editor.grid(columnspan=7,row=0, column=0, sticky="ew")
    text_editor.pack(expand=True,fill="both")

    copy_right="これはLinuxPCからラズパイ(ロボット)のプログラムを開発するためのGUIです．"+ \
    "ロボットと，このRCC2を起動しているLinuxPCは，ローカルネットワークで接続" + \
    "されている必要があります．\n \n" + \
    "(1) 上の「ロボットIPアドレス」にプログラミングしたいロボット(リモート)"+ \
    "のIPアドレスを入力してください．ping ボタンをクリックして通信時間が表示"+\
    "されれば，ロボットとネットワーク接続が完了していることを確認できます．\n \n" + \
    "(2) スタートボタンをクリックすると，Execute/start.pyが実行されます．" + \
    "start.pyからsubprocessを用いて，いくつかのプログラムがロボット上で実行 "+\
    "されます．"+\
    "正常にスタートすると，「ロボット制御」のパネルが表示されます．"+\
    "開始後のロボットの制御はこのパネル上で行います．\n\n"+\
    "(3) start後，ロボットはカメラ画像を送り返してきます．" +\
    "「ファイル」から実行するProgramを選んで（あるいは新規作成して），"+\
    "「ローカル実行」をクリックすると，そのProgramがLinuxPCで実行されます．\n\n"+\
    "Gitレポジトリ: https://gitlab.com/honda_lab/rcc23.11.git  \n \n"+ \
    "CC BY-SA Yasushi Honda 2023 11/21．\n" + \
    "icons: https://icons8.com/icons/pulsar-color  \n \n"
    text_editor.insert(tk.END, copy_right)


    # button Frame
    # マウスとキーボードの値を読み込む
    msky=Msky.Mouse_and_keyinput(fig,ax)
    msky.connect()


    # img オブジェクトもreturnしないと，ガベージコレクションに引っかかるようで，labelに表示されない． 

if __name__=="__main__":
    root=tk.Tk() 
    root.geometry("+250+240")
    root.title("ロボット制御パネル")
    root.attributes("-topmost", True)
    local_addr=sys.argv[1]
    robot_addr=sys.argv[2]
    calc_addr=sys.argv[3]
    construct(root,local_addr,robot_addr, calc_addr)
    root.mainloop()
