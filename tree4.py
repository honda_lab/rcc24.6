#!/usr/bin/python3
## ファイルツリーを表示するクラス
# 参考：https://qiita.com/dede-20191130/items/327fbeb181f13ee4ba2b
# cc by-sa Yasushi Honda 2023 12.20
import cv2
import glob
import os,time
import subprocess
import tkinter as tk
import tkinter.ttk as ttk
import linecache
import threading
from PIL import Image,ImageTk
from modules import md2tk
from modules import pycode

text_fg="#204030"
text_bg="#d0e0d0"
howto_bg="#fafaa0"
button_bg="#ffc666"

class Tree(): ## pythonファイルを別窓で開くクラス
    def __init__(self,root, text_frame, myPath,color,robot_id,robot_addr,calc_id,calc_addr,text_editor,canvas,select_entry): ## コンストラクター
        self.path=myPath
        self.home=myPath
        self.color=color
        self.robot_id=robot_id
        self.robot_addr=robot_addr
        self.calc_id=calc_id
        self.calc_addr=calc_addr
        self.text_editor=text_editor
        self.canvas=canvas
        self.select_entry=select_entry  ## 選択フアイル表示エントリー
        self.selected_file=""
        self.text_frame=text_frame

        tree_frame = tk.Frame(root, relief=tk.RAISED, bd=0,bg=text_bg) ##
        cmd_frame = tk.Frame(root, relief=tk.RAISED, bd=0,bg="orange") ##
        btn_frame = tk.Frame(root, relief=tk.RAISED, bd=0,bg="orange") ##

        self.style = ttk.Style()
        self.style.configure("Treeview", fieldbackground=text_bg,indent=12)
        '''
        self.tree = ttk.Treeview(tree_frame, height=27, selectmode="browse")
        '''
        self.tree = ttk.Treeview(tree_frame, selectmode="browse") ## Treeviewインスタンス
        self.tree.bind("<<TreeviewSelect>>",self.select_record) ## File Treeからマウスで選んだとき実行
        self.tree.bind("<Double-Button-1>", self.double_click)  ## ダブルクリックしたとき実行
        self.tree.bind("<Button-3>", self.click3)

        cmd_frame.pack(side="top", fill="x")
        tree_frame.pack(side="bottom", expand=True, fill="both")
        btn_frame.pack(side="bottom", fill="x")

        #self.tree["columns"] = (1) #列インデックスの作成

        self.tree["show"] = ("tree") # 表スタイルの設定 headingsを指定すると，tree表示できない．

        '''
        self.tree.column(1,width=80) # 各列の設定
        self.tree.column(2,width=500)

        self.tree.heading(1, text="コメント") # 各列のヘッダー設定
        self.tree.heading(2, text="絶対パス")
        '''

        # tagの色指定
        self.tree.tag_configure("dir_row", background='#b0b4b0',foreground='#040804')
        self.tree.tag_configure("latex_row", background='darkkhaki',foreground='black')
        self.tree.tag_configure("python_row", background=button_bg,foreground=text_fg)
        self.tree.tag_configure("csv_row", background="white",foreground='DodgerBlue')
        self.tree.tag_configure("md_row", background='white',foreground='black')
        self.tree.tag_configure("img_row", background='white',foreground='black')
        self.tree.tag_configure("etc_row", background='white',foreground='black')
 
        # text_editorでつかう色定義
        self.text_editor.tag_config('finfo_line', background=howto_bg, foreground=text_fg)
        self.text_editor.tag_config('section', background=text_bg, foreground=text_fg,font=(22))
        self.com_color=[]
        com_num=0
        self.com_color.append("import_color")
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="violet")
        self.com_color.append("from_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="violet")
        self.com_color.append("while_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="green")
        self.com_color.append("for_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="green")
        self.com_color.append("try_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="green")
        self.com_color.append("except_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="green")
        self.com_color.append("if_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="red")
        self.com_color.append("elif_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="red")
        self.com_color.append("else_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="red")
        self.com_color.append("figure_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="green")
        self.com_color.append("class_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="green", foreground="white")
        self.com_color.append("def_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="blue")
        self.com_color.append("eqnarray_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="blue")
        self.com_color.append("print_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="blue")
        self.com_color.append("self_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="brown")
        self.com_color.append("begin_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="red")
        self.com_color.append("end_color"); com_num+=1
        self.text_editor.tag_config(self.com_color[com_num], background="#e0e3e0", foreground="red")
        self.com_num=com_num
        self.text_editor.tag_config('odd_line_num', background="#dfd0df", foreground="brown")
        self.text_editor.tag_config('even_line_num', background="#d0d0df", foreground="brown")
        self.text_editor.tag_config('odd_line', background="#e0e3e0", foreground="#404040")
        self.text_editor.tag_config('even_line', background="#e7efe7", foreground="#404040")

        self.command=[]
        self.command.append("import ") #1
        self.command.append("from ") 
        self.command.append("while ")
        self.command.append("for ")
        self.command.append("try:")   #5
        self.command.append("except ") 
        self.command.append("if ")    
        self.command.append("elif")
        self.command.append("else")
        self.command.append("{figure}") #10
        self.command.append("class ")  
        self.command.append("def")    
        self.command.append("{eqnarray}")
        self.command.append("print")
        self.command.append("self.")  #15
        self.command.append("begin{") 
        self.command.append("end{")  

        '''
        # cmd frame
        Label=tk.Label(cmd_frame,text="Program: ")
        Label.pack(side="left")
        self.program_entry=tk.Entry(cmd_frame, width=50, bd=2, bg="white",fg="black")
        self.program_entry.insert(0,"")
        self.program_entry.pack(side="top")
        '''

        send2robot_button = tk.Button(cmd_frame,text="To Robot", bg="DarkOliveGreen3", fg="black", command=lambda: self.send(robot_id,robot_addr),font=("",8))
        send2calc_button = tk.Button(cmd_frame,text="To Calc. Server", command=lambda: self.send(calc_id,calc_addr), bg="LightPink",fg="black",font=("",8))
        edit_button = tk.Button(cmd_frame,text="編集", command=self.edit,bg="orange",fg="black",font=("",8))


        editor_frame = ttk.LabelFrame(cmd_frame, text='エディター')
        self.editor = tk.StringVar()
        if "EV3" in self.home :
            self.editor.set("gedit")
        else:
            self.editor.set("gvim")
        rb1 = ttk.Radiobutton(editor_frame, text='gvim', variable=self.editor,value="gvim")
        rb2 = ttk.Radiobutton(editor_frame, text='gedit', variable=self.editor,value="gedit")
        rb3 = ttk.Radiobutton(editor_frame, text='nano', variable=self.editor,value="nano")
        rb1.pack(side="left",expand=True,fill="x",padx=5)
        rb2.pack(side="left",expand=True,fill="x",padx=5)
        rb3.pack(side="left",expand=True,fill="x",padx=5)

        editor_frame.pack(side="top",expand=True,fill="x")
        edit_button.pack(side="left",expand=True,fill='x')
        send2robot_button.pack(side="left",expand=True,fill='x')
        send2calc_button.pack(side="left",expand=True,fill='x')

        # ボタンの作成
        #exec_button = tk.Button(button_frame,text="実行", command=self.execute)
        parent_button = tk.Button(btn_frame,text="<<", command=self.parent_dir,bg="white",fg="black")
        reload_button = tk.Button(btn_frame,text="reload", command=self.reload_mydir,bg="white",fg="black")
        home_button = tk.Button(btn_frame,text="home", command=self.home_dir,bg="orange",fg="black")
        child_button = tk.Button(btn_frame,text=">>", command=self.child_dir,bg="white",fg="black")
        # ボタンの配置
        #exec_button.grid(padx=5, pady=5, row=2,column=0)
        #select_button.grid(padx=2, pady=5, row=0,column=1)
        child_button.pack(side="right",expand=True,fill="x")
        reload_button.pack(side="right",expand=True,fill="x")
        home_button.pack(side="right",expand=True,fill="x")
        parent_button.pack(side="right",expand=True,fill="x")


    def send(self,user,addr): ## Execute/ に scpする
        try:
            selectedItems = self.tree.selection()
            path = self.tree.item(selectedItems[0])['values'][1]
            print(path)
            cmd="scp -r " + path + " "+user.get()+"@"+addr.get() + ":Execute/ "
            #cmd="rsync -av -h --progress " + path + " "+user.get()+"@"+addr.get() + ":Execute/ "
            self.text_editor.insert("1.0",cmd+"\n")
            #pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
            #print(proc.stderr)

            pro=subprocess.Popen(cmd.strip().split(' '),stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
            #line=pro.stdout.readline().decode('utf8').replace("\r","")
            #print(line)

            '''
            def show_output():
                cmd2="progress"
                while True:
                    time.sleep(1)
                    #pro2=subprocess.Popen(cmd2.strip().split(' '),stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                    pro2 = subprocess.run(cmd2,shell=True ,capture_output=True,text=True)
                    lines=pro2.stdout.split("\n")
                    self.text_editor.insert("1.0", lines[1]+"\n")
                    if pro.poll() is not None:
                        # textの背景色をもとに戻す．
                        self.text_editor.configure(fg="black", bg=text_bg)
                        break

            thread=threading.Thread(target=show_output)
            thread.start()
            '''
        except:
            print(user+"@"+addr+" にファイル転送失敗")

    def edit(self): ## File Treeで選んだファイルをエディターを開く
        try:
            selectedItems = self.tree.selection()
            program = self.tree.item(selectedItems[0])['values'][1]
            print(program)
            if self.editor.get()=="gvim":
                cmd="gvim " + program + " -geometry 100x40+740+0 " 
                proc=subprocess.Popen(cmd.strip().split(' '))
            elif self.editor.get()=="gedit":
                cmd="gedit " + program
                proc=subprocess.Popen(cmd.strip().split(' '))
            elif self.editor.get()=="nano":
                cmd="mate-terminal --command \"nano " + program + "\" " + \
                     " --geometry 100x25+740+120 "
                #cmd="xterm -geometry 100x25+740+120" + \
                #" -e \"nano " + program + "\" " 
                pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
            print(cmd)
        except:
            comment="ファイルを選んでください．"
            self.text_editor.configure(bg=text_bg)
            self.text_editor.delete("1.0", tk.END)
            self.text_editor.tag_config('comment_line', background="yellow", foreground="red")
            self.text_editor.insert("1.0", "   "+comment+" \n","comment_line")

    def reload_mydir(self):
        self.tree.delete(*self.tree.get_children())
        self.open_list("",self.path)

    def home_dir(self):
        #my_dir=os.path.dirname(__file__).replace("/tabs","")
        filePath=os.path.expanduser("~")
        self.select_entry.delete(0,tk.END)
        self.select_entry.insert(0,filePath)
        self.tree.delete(*self.tree.get_children())
        self.open_list("",filePath)
        self.output_work_dir(filePath)

    def open_list(self,parent,path): ## ディレクトリを再帰的に呼び出して実際にtreeviewの内容をつくる
        #print("%s in open_list" % path)
        # 選択可能なpythonファイルを辞書の値として設定
        # 指定フォルダ直下のすべてのpythonファイル（再帰的に検索）
        file_Dic = {} # ファイルリスト用の辞書
        dir_Dic = {} # ディレクトリリスト用の辞書
        if os.path.isdir(path): ## pathがディレクトリの場合，その内容を辞書に格納
            for file in glob.glob(path+'/**'): ## pathの中のすべての名前(file)に対して
                # fileは絶対パス付きのファイル名 
                #print(file)
                nameRoot, ext = os.path.splitext(os.path.basename(file))
                if os.path.isfile(file) and ext in ('.py', '.pyw'): ## fileがPythonの場合
                    fp=open(file,"r",encoding="utf8",errors='ignore')
                    fp.readline()
                    comment=fp.readline() ## 2行目をコメントとして読み込む
                    fp.close()
                else:
                    comment="none"
                #if os.path.isfile(file) and ext in ('.py', '.pyw') and nameRoot != '__init__':
                if os.path.isfile(file): ## 名前がファイルの場合
                    file_Dic[file] = {'name':os.path.basename(file), ## 要素をfile_Dicに格納（２次元辞書）
                                  'comment':comment,
                                  'abspath':file,
                                  'ext':ext}
                # selectで絶対パス付きのファイル名を必要とするので，abspathも辞書に格納
                elif os.path.isdir(file): ## 名前がディレクトリの場合
                    #print(file)
                    dir_Dic[file] = {'name':os.path.basename(file), ## 要素をdir_Dicに格納(commend,extはなし)
                        'comment':'' ,'abspath':file}

            for k, v in sorted(dir_Dic.items(), key=lambda x: x[1]['name']): ## dirレコードの挿入
                if str(v['name'])=='Calc':
                    child=self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])), tag='dir_row' ,open=True)
                elif str(v['name'])=='Robot':
                    child=self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])), tag='dir_row' ,open=True)
                else:
                    child=self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])), tag='dir_row' ,open=False)
                #print(str(v['abspath']))
                self.open_list(child, str(v['abspath']))  ## 再帰的にchildデイレクトリを開く

            # fileを拡張子で分類してレコードの挿入
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']): ## fileレコードの挿入
                if v['ext']=='.py' :
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="python_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.csv' :
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="csv_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.trc' :
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="trc_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.tex' :
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="latex_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.md' :
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="md_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.txt' :
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="md_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.pdf' or v['ext']=='.PDF' :
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="pdf_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.mp4' or v['ext']=='.MP4':
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="mp4_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.webm' or v['ext']=='.WEBM':
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="mp4_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.mp3' or v['ext']=='.MP3':
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="mp4_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.wav' or v['ext']=='.WAV':
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="mp4_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.jpg' or v['ext']=='.JPG' or v['ext']=='.jpeg' or v['ext']=='.JPEG':
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="img_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.png' or v['ext']=='.PNG':
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="img_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='':
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="etc_row" )
        else:
            print("%s is NOT dir." % path)

        self.tree.pack(fill="both",expand=True,side="top") ## File Treeを配置


    def select_record(self,event): ## 選んだファイルの拡張子に応じて表示する
        try:
            selectedItems = self.tree.selection()
            filePath = self.tree.item(selectedItems[0])['values'][1]  ##
            self.select_entry.delete(0,tk.END)
            self.select_entry.insert(0,filePath)  ## Selected entry に選ばれたfilePathを挿入

            self.text_editor.delete("1.0", tk.END)
            self.text_editor.configure(bg=text_bg)

            self.selected_file=filePath
            if os.path.isdir(filePath):  ## ディレクトリの場合には
                my_dir=filePath
                os.chdir(my_dir)
                # canvasが表示されている場合があるので，text_editoをpackし直す．
                for widget in self.text_frame.winfo_children():
                    try:
                        widget.pack_forget()
                    except:
                        print('widgetをpack_forgetできません')
                self.text_editor.pack(side="top",expand=True,fill="both")

                # File Tree 表示
                #self.path=filePath 
                #self.tree.delete(*self.tree.get_children())
                #self.open_list("",filePath)

                try:
                    cmd="git branch"  ## branchを調べて表示
                    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
                    items=pro.stdout.split("\n")
                    for item in items:
                        if item[0]=="*":
                            current=item.split()[1]
                            self.text_editor.insert("1.0","Current branch: \'"+current+"\' \n","warning_line")
                            break
                except:
                    self.text_editor.insert("1.0","git ディレクトリではありません．\n","warning_line")
                
                cmd="ls -lh " + filePath  ## ディレクトリ情報を表示
                proc = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
                info=proc.stdout.replace(filePath,"")
                #self.text_editor.tag_config('finfo_line', background="#a0a7a0", foreground="#0e0a0a")
                self.text_editor.insert("1.0", info ,"finfo_line")

            elif os.path.isfile(filePath): ## 選ばれたのがファイルの場合
                my_dir=os.path.dirname(filePath)
                os.chdir(my_dir)

                nameRoot, ext = os.path.splitext(os.path.basename(filePath))
                print(nameRoot,ext)
                if ext=='.py': ## 拡張子に応じて表示関数実行
                    self.show_py(filePath) ##
                elif ext=='.txt':
                    self.show_py(filePath)
                elif ext=='.csv' or ext=='.trc':
                    self.show_py(filePath)
                elif ext=='.md' or ext=='.MD':
                    self.show_md(filePath)
                elif ext=='.jpg' or ext=='.JPG' or ext=='.jpeg' or ext=='.JPEG':
                    self.show_img(filePath)
                elif ext=='.png' or ext=='.PNG' :
                    self.show_img(filePath)
                elif ext=='.pdf' or ext=='.PDF' :
                    self.show_pdf(filePath)
                elif ext=='.mp4' or ext=='.MP4' or ext=='.webm' or ext=='.WEBM' :
                    self.play_video(filePath)
                elif ext=='':
                    self.show_py(filePath)

            self.output_work_dir(my_dir)

        except:
            pass
            #self.select_entry.delete(0,tk.END)
            #self.select_entry.insert(0,filePath)

    def output_work_dir(self,path):
        exec_dir=os.path.dirname(__file__)
        fp=open(exec_dir+"/work_dir","w")
        fp.write(path)
        fp.close()

    def show_pdf(self,filePath):
        for widget in self.text_frame.winfo_children():
            try:
                widget.pack_forget()
            except:
                print('widgetのpack_forgetできません')
        # filePathから画像を読み込み
        readImage = Image.open(filePath)

    def play_video(self,filePath):
        for widget in self.text_frame.winfo_children():
            try:
                widget.pack_forget()
            except:
                print('widgetのpack_forgetできません')

        cap = cv2.VideoCapture(filePath)

        if (cap.isOpened()== False):  
            print("ビデオファイルを開くとエラーが発生しました") 

        # 1枚目のframeを表示
        ret, frame = cap.read()
        cv_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # NumPyのndarrayからPillowのImageへ変換
        readImage = Image.fromarray(cv_image)

        width=self.text_frame.winfo_width()
        height = int(readImage.height/readImage.width * width)
        print(width,height)

        showImage = readImage.resize((width,height))
        global imagetk # ガーベージコレクションでメモリから開放されないため必要
        imagetk = ImageTk.PhotoImage(showImage,master=self.text_frame)

        canvas = tk.Canvas(self.text_frame, width=width, height=height)
        #canvas.pack(anchor=tk.NW,expand=True)
        canvas.pack(side='top')

        canvas.create_image(width/2, height/2, image=imagetk)

        total_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        fps = int(cap.get(cv2.CAP_PROP_FPS))
        #print("fps, length=%d, %5.1f" % (fps,total_frame/fps)) 
        global speed
        speed=int(total_frame/30)

        length=total_frame/fps
        minute=length//60
        second=length%60
        length_str=str("/ %d\':%d\" (min:sec)"%(minute,second))
        Position=tk.Label(self.text_frame,bg=text_bg)
        Position.pack(side='top',fill='x')
        Position.configure(text=length_str)

        cmd='ls -lh ' + filePath
        proc = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
        line=proc.stdout.replace(filePath,'').strip('\n')
        Label=tk.Label(self.text_frame,text=line,bg=text_bg)
        Label.pack(side='top',fill='x')

        global frame_count
        frame_count=0
        #frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        #print(frame_count)

        def show_fw(event):
            global imagetk # ガーベージコレクションでメモリから開放されないため必要
            global frame_count
            #print("%5.2f ％" % (frame_count/total_frame*100)) 
            if frame_count<total_frame:
                cap.set(cv2.CAP_PROP_POS_FRAMES, frame_count)
                ret, frame = cap.read()
                frame_count+=speed
                # BGR→RGB変換
                cv_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                # NumPyのndarrayからPillowのImageへ変換
                readImage = Image.fromarray(cv_image)

                showImage = readImage.resize((width,height))
                imagetk = ImageTk.PhotoImage(showImage,master=self.text_frame)

                canvas.create_image(width/2, height/2, image=imagetk)

                length=frame_count/fps
                minute=length//60
                second=length%60
                pos_str=str("再生時間: %d\':%d\" "%(minute,second))
                Position.configure(text=pos_str+length_str)

        def show_bk(event):
            global imagetk # ガーベージコレクションでメモリから開放されないため必要
            global frame_count
            #print("%5.2f ％" % (frame_count/total_frame*100)) 
            if frame_count>speed:
                frame_count-=speed
                cap.set(cv2.CAP_PROP_POS_FRAMES, frame_count)
                ret, frame = cap.read()
                # BGR→RGB変換
                cv_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                # NumPyのndarrayからPillowのImageへ変換
                readImage = Image.fromarray(cv_image)

                showImage = readImage.resize((width,height))
                imagetk = ImageTk.PhotoImage(showImage,master=self.text_frame)

                canvas.create_image(width/2, height/2, image=imagetk)

                length=frame_count/fps
                minute=length//60
                second=length%60
                pos_str=str("再生時間: %d\':%d\" "%(minute,second))
                Position.configure(text=pos_str+length_str)

        canvas.bind("<ButtonPress-4>", show_bk)
        canvas.bind("<ButtonPress-5>", show_fw)

    def show_img(self,filePath):
        for widget in self.text_frame.winfo_children():
            try:
                widget.pack_forget()
            except:
                print('widgetのpack_forgetできません')
        # filePathから画像を読み込み
        readImage = Image.open(filePath)
        width=self.text_frame.winfo_width()
        height = int(readImage.height/readImage.width * width)
        print(width,height)
        showImage = readImage.resize((width,height))
        global imagetk # ガーベージコレクションでメモリから開放されないため必要
        imagetk = ImageTk.PhotoImage(showImage,master=self.text_frame)

        # canvasを作成
        #canvas = tk.Canvas(frame, width=readImage.width, height=readImage.height,
        #        highlightthickness=0)
        canvas = tk.Canvas(self.text_frame, width=showImage.width, height=showImage.height)

        # canvasに画像を描画
        canvas.create_image(width/2, height/2, image=imagetk)
        #canvas.bind("<ButtonPress-4>", self.yup_scrolling,"+")
        #canvas.bind("<ButtonPress-5>", self.ydown_scrolling,"+")
        canvas.pack(side='top',fill='y')

        readImage.close()

        cmd='ls -lh ' + filePath
        proc = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
        line=proc.stdout.replace(filePath,'').strip('\n')
        Label=tk.Label(self.text_frame,text=line,bg=text_bg)
        Label.pack(side='top',fill='x')

    def show_md(self,filePath):
        #print("Markdownフォーマットで記述されたテキストを表示")
        for widget in self.text_frame.winfo_children():
            widget.pack_forget()
        #self.text_editor.pack_forget()
        indct=md2tk.Markdown2Tkinter(filePath,self.text_frame)

    def show_struct(self,filePath):
        for widget in self.text_frame.winfo_children():
            widget.pack_forget()
        self.text_editor.pack(side="top",expand=True,fill="both")
        indct=pycode.Structure(filePath,self.text_frame)


    def show_py(self,filePath):
        for widget in self.text_frame.winfo_children():
            widget.pack_forget()
        self.text_editor.pack(side="top",expand=True,fill="both")

        fp=open(filePath,"r")
        line=fp.readline()
        line_num=1

        while line!="":
            if line_num % 2 ==1: # odd
                pos=str(line_num+1)+'.0'
                if line_num<10:
                    self.text_editor.insert(pos, "      "+str(line_num)+":   ","odd_line_num")
                    pos=str(line_num+1)+'.11'
                    self.text_editor.insert(pos, line,"odd_line")
                elif line_num<100:
                    self.text_editor.insert(pos, "   "+str(line_num)+":   ","odd_line_num")
                    pos=str(line_num+1)+'.11'
                    self.text_editor.insert(pos, line,"odd_line")
                else:
                    self.text_editor.insert(pos, str(line_num)+":   ","odd_line_num")
                    pos=str(line_num+1)+'.11'
                    self.text_editor.insert(pos, line,"odd_line")
            else: # even
                pos=str(line_num+1)+'.0'
                if line_num<10:
                    self.text_editor.insert(pos, "      "+str(line_num)+":   ","even_line_num")
                elif line_num<100:
                    self.text_editor.insert(pos, "   "+str(line_num)+":   ","even_line_num")
                else:
                    self.text_editor.insert(pos, str(line_num)+":   ","even_line_num")
                pos=str(line_num+1)+'.11'
                self.text_editor.insert(pos, line,"even_line")
                 
            for i in range(self.com_num):
                if line_num<10:
                    if self.command[i] in line:
                        com_pos=str(line_num)+"."+str(line.find(self.command[i])+11)
                        com_end=str(line_num)+"."+str(line.find(self.command[i])+11+len(self.command[i]))
                        #print(line_num,self.command[i],com_pos,com_end)
                        self.text_editor.delete(com_pos,com_end)
                        self.text_editor.insert(com_pos, self.command[i], self.com_color[i])
                elif line_num<100:
                    if self.command[i] in line:
                        com_pos=str(line_num)+"."+str(line.find(self.command[i])+9)
                        com_end=str(line_num)+"."+str(line.find(self.command[i])+9+len(self.command[i]))
                        #print(line_num,self.command[i],com_pos,com_end)
                        self.text_editor.delete(com_pos,com_end)
                        self.text_editor.insert(com_pos, self.command[i], self.com_color[i])
                else:
                    if self.command[i] in line:
                        com_pos=str(line_num)+"."+str(line.find(self.command[i])+7)
                        com_end=str(line_num)+"."+str(line.find(self.command[i])+7+len(self.command[i]))
                        #print(line_num,self.command[i],com_pos,com_end)
                        self.text_editor.delete(com_pos,com_end)
                        self.text_editor.insert(com_pos, self.command[i], self.com_color[i])
                     

            line=fp.readline()
            line_num+=1
        fp.close()

    def double_click(self,event): ## ダブルクリックした場合，拡張子に応じてアプリを起動
        try:
            selectedItems = self.tree.selection()
            filePath = self.tree.item(selectedItems[0])['values'][1]
            print(filePath,"double-click")
            if os.path.isdir(filePath):
                self.path=filePath 
                self.tree.delete(*self.tree.get_children())
                self.open_list("",filePath)
            else:
                nameRoot, ext = os.path.splitext(os.path.basename(filePath))
                print(nameRoot,ext)
                if ext=='.mp4' or ext=='.MP4' or ext=='.webm' or ext=='.WEBM':
                    cmd="vlc " + filePath
                    print(cmd)
                    proc=subprocess.Popen(cmd.strip().split(' '))
                elif ext=='.jpg' or ext=='.JPG' or ext=='.jpeg' or ext=='.JPEG':
                    image = Image.open(filePath)
                    w_size=640
                    h_size=480
                    tk_image = ImageTk.PhotoImage(image=image.resize((w_size,h_size)))
                    self.canvas.create_image(0, 0, anchor=tk.NW, image=tk_image)
                    print("create_image")
                    #cmd="geeqie " + filePath
                    #print(cmd)
                    #proc=subprocess.Popen(cmd.strip().split(' '))
                elif ext=='.pdf' or ext=='.PDF' :
                    cmd="atril " + filePath
                    print(cmd)
                    proc=subprocess.Popen(cmd.strip().split(' '))
                elif ext=='.tex' or ext=='.TEX' :
                    current_dir=os.path.dirname(filePath)
                    print(current_dir)
                    os.chdir(current_dir)
                    cmd="mate-terminal --title='Execute' --command \"bash -c 'platex " + filePath + "';bash\" --geometry 80x10+200+600 "
                    self.text_editor.delete("1.0",tk.END)
                    self.text_editor.insert("1.0",cmd+"\n")
                    proc = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
                    self.text_editor.insert("1.0",proc.stdout)

                    cmd="mate-terminal --title='Execute' --command \"bash -c 'dvipdfmx " + \
                        filePath.replace("tex","dvi") + \
                        "; atril " + filePath.replace("tex","pdf") + \
                        " ';bash\" --geometry 80x10+200+600 &"
                    proc = subprocess.run(cmd,shell=True ,capture_output=True,text=True)


        except:
            print("ダブルクリック失敗")

    def click3(self,event):
        try:
            selectedItems = self.tree.selection()
            filePath = self.tree.item(selectedItems[0])['values'][1]
            print(filePath,"click-3")
            if os.path.isdir(filePath):  ## ディレクトリの場合には
                my_dir=filePath
                os.chdir(my_dir)
                # canvasが表示されている場合があるので，text_editoをpackし直す．
                for widget in self.text_frame.winfo_children():
                    try:
                        widget.pack_forget()
                    except:
                        print('widgetをpack_forgetできません')
                self.text_editor.pack(side="top",expand=True,fill="both")

                try:
                    cmd="git branch"  ## branchを調べて表示
                    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
                    items=pro.stdout.split("\n")
                    for item in items:
                        if item[0]=="*":
                            current=item.split()[1]
                            self.text_editor.insert("1.0","Current branch: \'"+current+"\' \n","warning_line")
                            break
                except:
                    self.text_editor.insert("1.0","git ディレクトリではありません．\n","warning_line")
                
                cmd="ls -lh " + filePath  ## ディレクトリ情報を表示
                proc = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
                info=proc.stdout.replace(filePath,"")
                #self.text_editor.tag_config('finfo_line', background="#a0a7a0", foreground="#0e0a0a")
                self.text_editor.insert("1.0", info ,"finfo_line")
        except:
            pass

    def get_selected(self):
        return self.selected_file

    def execute(self):
        # 表から選んだファイルパスを取得
        try:
            selectedItems = self.tree.selection()
            filePath = self.tree.item(selectedItems[0])['values'][1]
            print(filePath)

            # pyファイル呼び出し実行
            #subprocess.check_call(['python3', filePath])
            cmd="mate-terminal --title='Execute' --command \"bash -c 'python3 " + filePath + "';bash\" --geometry 80x10+200+600 &"
            pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
        except:
            print("ファイルを選んでください")

    def parent_dir(self):
        self.path=self.path[:self.path.rfind('/')]
        if len(self.path)==0:
            self.path="/home"
        #print(self.path)
        self.tree.delete(*self.tree.get_children())
        self.open_list("",self.path)

    def child_dir(self):
        try:
            selectedItems = self.tree.selection()
            self.path = self.tree.item(selectedItems[0])['values'][1]
        except:
            print("選択失敗")

        #print(self.path)
        if os.path.isdir(self.path):
            self.tree.delete(*self.tree.get_children())
            self.open_list("",self.path)
        else:
            print("ディレクトリを選んでください")

    def reload_mydir(self):
        self.tree.delete(*self.tree.get_children())
        self.open_list("",self.path)

    def font_mod(self,size):
        font = ('Yu Gothic UI', size)
        self.style.configure("Treeview", font=font, rowheight=size*2)
        self.tree.update()

    def line_spacing(self,spacing):
        self.style.configure("Treeview", rowheight=spacing)

if __name__ == '__main__': ## 実行されたときのメインルーチン
    my_tree=Tree('/home/honda/GitLab/')  ##
