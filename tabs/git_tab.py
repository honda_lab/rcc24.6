# CC BY-SA Yasushi Honda 2023 9/11

import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os

common_bg="orange"
text_fg="#071407"
text_bg="#d0e0d0"
button_bg="#ffc666"
howto_bg="#fafaa0"

# ==== git_operation_tab をつくる =====
def const_tab(tab, selected, text, text_frame, local, robot_id, robot_addr, calc_id, calc_addr):

    frame_button = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)
    frame_button.pack(side="top", fill="x")

    text.tag_config('finfo_line', background="#a0a7a0", foreground="#0e0a0a")
    text.tag_config('warning_line', background=howto_bg, foreground=text_fg)

    Label=tk.Label(frame_button,text="Project: ", bg="orange",fg=text_fg)
    #Label.pack(side="left",fill='x',padx=4, pady=4)
    project_entry=tk.Entry(frame_button, width=10, bd=2, bg=text_bg, fg="black")
    project=selected.get()[selected.get().rfind('/'):].strip('/')
    project_entry.insert(0,project)
    project_entry.pack(expand=True, side="left", fill="both", padx=4, pady=4)

    Label=tk.Label(frame_button,text="Branch: ", bg="orange",fg=text_fg)
    Label.pack(side="left",fill='x',pady=4)

    def text_pack():
        for widget in text_frame.winfo_children():
            widget.pack_forget()
        text.pack(side="top",expand=True,fill="both")

    def get_branch():
        if os.path.isdir(selected.get()):
            my_dir=selected.get()
        elif os.path.isfile(selected.get()):
            my_dir=os.path.dirname(selected.get())
        else:
            comment="File Treeからファイルまたはディレクトリを選んでください\n"
            text.insert("1.0", comment, 'warning_line')
            return
        os.chdir(my_dir)

        cmd="git branch -a"
        pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)

        items=pro.stdout.strip('\n').split("\n")
        branches=[]
        for item in items:
            branch=item.split('/')[-1]
            branches.append(branch) 
        branch_cmbbx["values"]=branches
        branches.clear()

    branch_list = ['main', 'develop']
    branch_cmbbx = ttk.Combobox(frame_button, width=10, values=branch_list, postcommand=get_branch, style="office.TCombobox")
    branch_cmbbx.current(0)
    branch_cmbbx.pack(side="left",fill='y', padx=6,pady=4)

    #   text.pack(side="top",expand=True,fill="both")

    def get_log():
        if os.path.isdir(selected.get()):
            my_dir=selected.get()
        elif os.path.isfile(selected.get()):
            my_dir=os.path.dirname(selected.get())
        else:
            comment="File Treeからファイルまたはディレクトリを選んでください\n"
            text.insert("1.0", comment, 'warning_line')
            return
        os.chdir(my_dir)

        cmd="git log | grep commit"
        pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)

        items=pro.stdout.strip('\n').split("\n")
        logs=[]
        for item in items:
            log=item.strip('commit ')
            logs.append(log) 
        log_cmbbx["values"]=logs
        logs.clear()

    Label=tk.Label(frame_button,text="commit log: ", bg="orange",fg=text_fg)
    Label.pack(side="left",fill='both',pady=4)
    log_list = []
    log_cmbbx = ttk.Combobox(frame_button, width=30, values=log_list, postcommand=get_log, style="office.TCombobox")
    #log_cmbbx.current(0)
    log_cmbbx.pack(expand=True, side="left",fill='both', padx=6,pady=4)

    def how_to():
        my_dir=os.path.dirname(__file__)
        text.tag_config('finfo_line', background=howto_bg, foreground=text_fg)
        fp=open(my_dir+"/howto_git.txt","r")
        howto_txt=""
        for line in fp.readlines():
            howto_txt+=line
        fp.close()
        text.delete("1.0",tk.END)
        text.insert(tk.END, howto_txt,'finfo_line')

    howto_button = tk.Button(frame_button, text="How To", width=7,
           activebackground='DarkOliveGreen1', activeforeground="red", 
           bg=button_bg, fg=text_fg,
           borderwidth=0, padx=5, command=how_to)
    howto_button.pack(side="right",padx=3,pady=0)

    def project_name():
        project=selected.get()[selected.get().rfind('/'):].strip('/')
        project_entry.delete(0,tk.END)
        project_entry.insert(0,project)

    def execute(event):
        if os.path.isdir(selected.get()):
            my_dir=selected.get()
        elif os.path.isfile(selected.get()):
            my_dir=os.path.dirname(selected.get())
        else:
            comment="File Treeからファイルまたはディレクトリを選んでください\n"
            text.insert("1.0", comment, 'warning_line')
            return

        if combobox.get()=="log":
            cmd="git log"
        elif combobox.get()=="commit":
            cmd="mate-terminal --command \"bash -c 'git commit -a';bash\" --geometry +730+0 &"
        elif combobox.get()=="push":
           cmd="git push" 
        elif combobox.get()=="add":
           cmd="git add " + selected.get()
        elif combobox.get()=="pull":
           cmd="git pull "
        elif combobox.get()=="store":
           cmd="git config --global credential.helper store"
        elif combobox.get()=="checkout branch":
           cmd="git checkout " + branch_cmbbx.get()
        elif combobox.get()=="checkout commit":
           cmd="git checkout " + log_cmbbx.get()

        text_pack()
        os.chdir(my_dir)
        text.insert("1.0", cmd+" を実行します．\n", 'finfo_line')
        text.insert("1.0", "File Treeからcsvファイルを選んでください．\n",'finfo_line')

        text.yview_moveto(0)
        pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
        if len(pro.stderr)>0:
            err = pro.stderr + "\n"
            text.insert("1.0", err, 'warning_line')
        res=pro.stdout + '\n'
        text.insert("1.0", res,'finfo_line')

        #text.insert("1.0",pro.stdout)

    Label=tk.Label(frame_button,text="command: ", bg="orange",fg=text_fg)
    Label.pack(side="left",fill='both',pady=4)

    # Button Frame
    # Combobox
    style = ttk.Style()
    style.configure("office.TCombobox", selectbackground="Dodgerblue", fieldbackground=text_bg)
    style.configure("office.TCombobox", selectforeground="white", fieldforeground='white')
    style.configure("office.TCombobox", arrowsize=20, arrowcolor='white', background='Dodgerblue4')
    style.configure("office.TCombobox", foreground=text_fg)
    style.configure("office.TCombobox", justify=tk.CENTER, padding=5)

    cmd_list = ['log', 'commit', 'push', 'add', 'pull', 'store', 'checkout branch', 'checkout commit']
    v = tk.StringVar()
    #combobox = ttk.Combobox(frame_button, width=5, textvariable= v, values=module)
    combobox = ttk.Combobox(frame_button, font=(16), width=12, postcommand=project_name, values=cmd_list, style="office.TCombobox")
    combobox.current()
    combobox.bind('<<ComboboxSelected>>', execute)
    combobox.pack(side="left",fill='both',padx=6,pady=4)

    execute_button = tk.Button(frame_button, text="実行", 
           activebackground='DarkOliveGreen1', activeforeground="red", 
           bg=button_bg, fg=text_fg,
           borderwidth=4, padx=5, command=execute)

    #execute_button.pack(side="left",fill="x",expand=True,padx=3,pady=3)

