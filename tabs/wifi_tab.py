# CC BY-SA Yasushi Honda 2023 9/11

import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os
import timeout_decorator

common_bg="orange"
text_fg="#071407"
text_bg="#d0e0d0"
button_bg="#ffc666"
howto_bg="#fafaa0"

def const_tab(tab,text_frame, text_editor,wifi,local_entry):

    text_editor.tag_config('section', background="#a0a7a0", foreground="#0e0a0a")

    button_frame = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)
    input_frame = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)

    input_frame.pack(side="left",expand=True,fill='x')
    button_frame.pack(side="left",expand=True,fill="x")

    intf=StringVar()
    intf=wifi
    intf_entry=tk.Entry(input_frame, textvariable=intf, width=50, bd=2, bg="IndianRed",fg="white")
    Label=tk.Label(input_frame,text="Interface: ",bg="orange",fg="black",width=15)
    intf_entry.insert(0,intf)
    Label.grid(row=0, column=0, sticky="ew")
    intf_entry.grid(row=0, column=1, sticky=tk.EW)


    iwconfig_info="各ネットワークインターフェイスのアドレスなどを確認します．\n"  + \
      "wで始まるインタフェイスが無線ネットワークのものです．" + \
      "eで始まるインタフェイスが有線ネットワークのものです．" + \
      "inet の後ろの４桁の数字がIPアドレス(IPv4)です．\n\n"

    iwlist_info="無線ネットワークの状態を確認します．\n" + \
    "ESSID:に示されているのが，接続されているアクセスポイントです．" + \
    "ESSID:off/any 	の場合には，wpa_supplicantを実行して接続する必要があります． \n\n"

    arp_scan_info="\nネットワーク上のアドレスをスキャンします．\n" + \
    "sudo を利用します．password を求められることがあります．" + \
    "参考: /etc/sudoers \n\n"

    @timeout_decorator.timeout(15)
    def execute(event):
        for widget in text_frame.winfo_children():
            widget.pack_forget()
        text_editor.pack(side="top",expand=True,fill="both")
        text_editor.yview_moveto(0)

        global intf
        my_dir=os.path.dirname(__file__).replace("/tabs","")
        if combobox.get()=="iwconfig":
            cmd='iwconfig ' + ' &'
            pro = subprocess.run(cmd,shell=True, capture_output=True,text=True)
            interface=pro.stdout.split()[0]
            intf_entry.delete(0,tk.END)
            intf_entry.insert(0,interface)
            with open("wifi_interface",mode="w") as fp:
                fp.write(intf_entry.get()+"\n")
            #text_editor.delete("1.0",tk.END)
            text_editor.insert("1.0", pro.stdout,'finfo_line')
        elif combobox.get()=="ifconfig":
            cmd='ifconfig ' + intf_entry.get() + ' &'
            pro = subprocess.run(cmd,shell=True, capture_output=True,text=True)
            #text_editor.delete("1.0",tk.END)
            text_editor.insert("1.0", pro.stdout,'finfo_line')
            local=pro.stdout.split("\n")[1].split()[1]
            local_entry.delete(0,tk.END)
            local_entry.insert(0,local)
            print(local)
        elif combobox.get()=="iwlist":
            cmd='sudo iwlist ' + intf_entry.get() + ' scan | grep ESSID'
            pro = subprocess.run(cmd,shell=True, capture_output=True,text=True)
            #text_editor.delete("1.0",tk.END)
            text_editor.insert("1.0", pro.stdout,'finfo_line')
        elif combobox.get()=="arp-scan":
            #text_editor.delete("1.0",tk.END)
            text_editor.insert("1.0", arp_scan_info, 'finfo_line')
            cmd="sudo arp-scan -I " + intf_entry.get() + " -l "
            pro = subprocess.run(cmd,shell=True, capture_output=True,text=True)
            #text_editor.insert("1.0", pro.stdout,'finfo_line')
            lines=pro.stdout.strip('\n').split('\n')
            addr_list=[]
            i=0
            for line in lines:
                if i>=2 and len(line)>0:
                    addr_list.append(line.split()[0])
                    text_editor.insert("1.0", line+'\n', "finfo_line")
                if line=="":
                    break
                i+=1

    execute_button = tk.Button(button_frame, text="実行",command=execute, 
           activebackground='DarkOliveGreen1', activeforeground="red",
           bg=button_bg, fg=text_fg, borderwidth=4, padx=5, width=10)

    def how_to():
        for widget in text_frame.winfo_children():
            widget.pack_forget()
        text_editor.pack(side="top",expand=True,fill="both")

        my_dir=os.path.dirname(__file__)
        text_editor.config(background=howto_bg, foreground=text_fg)
        fp=open(my_dir+"/howto_wifi.txt","r")
        text_editor.delete("1.0",tk.END)
        for line in fp.readlines():
            if "===" in line:
                line=line.strip("===")
                text_editor.insert(tk.END, line,'section')
            else:
                text_editor.insert(tk.END, line)
        fp.close()

    howto_button = tk.Button(button_frame, text="How To", width=7,
           activebackground='DarkOliveGreen1', activeforeground="red", 
           bg=button_bg, fg=text_fg,
           borderwidth=0, padx=5, command=how_to)
    howto_button.pack(side="right",padx=2,fill="both")
    #execute_button.pack(side="right",padx=2)

    label_cmd=tk.Label(button_frame, text='Command: ', bg='orange')
    label_cmd.pack(side='left')

    style = ttk.Style()
    style.configure("office.TCombobox", selectbackground="Dodgerblue", fieldbackground=text_bg)
    style.configure("office.TCombobox", selectforeground="white", fieldforeground='white')
    style.configure("office.TCombobox", arrowsize=20, arrowcolor='white', background='Dodgerblue4')
    style.configure("office.TCombobox", foreground=text_fg)
    style.configure("office.TCombobox", justify=tk.CENTER, padding=5)

    plt_list = ['iwconfig', 'ifconfig', 'iwlist', 'arp-scan']
    v = tk.StringVar()
    combobox = ttk.Combobox(button_frame, font=(16), width=10, values=plt_list, style="office.TCombobox")
    combobox.current()
    combobox.bind('<<ComboboxSelected>>', execute)
    combobox.pack(side="left",padx=4,pady=4)
