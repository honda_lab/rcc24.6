# CC BY-SA Yasushi Honda 2023 9/11

import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os

common_bg="orange"
text_fg="#071407"
text_bg="#d6e0d6"
button_bg="#ffc666"
howto_bg="#fafaa0"

# ===== sound_analysis_tab をつくる =====
def const_tab(tab, text, selected, local,robot_id, robot_addr, calc_id, calc_addr):

   play_frame = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)
   rec_frame = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)

   # フレームの配置

   play_frame.pack(side="left", expand=True, fill="x")
   rec_frame.pack(side="left", expand=True, fill="x")

   msg="  これは，音解析のタブです． \n" + \
    "  既存のwavファイルを開いて，波形のグラフ化とそのスペクトルをリアルタイムに可視化します． \n" + \
    "  「再生」ボタンを選択することで，play_chunk.py というプログラムが実行されます． \n" + \
    "  「プログラム編集」ボタンをクリックすると，そのコードを編集してCHUNKサイズや描画範囲などの詳細を \n" + \
    "  調整します． \n" + \
    "  「mic録音」を選択すると，マイクから音声を入力しながらその波形とスペクトルをリアルタイムに確認できます． \n" + \
    "  (mic_wav_rec.py) \n"

   '''
   # Wav Entry
   Label=tk.Label(play_frame,text="Play WAV File: ",bg="white",fg="black")
   entry_sound=tk.Entry(play_frame, textvariable='', bd=2, bg="IndianRed",fg="white")
   entry_sound.insert(0,'')
   Label.pack(side='left')
   entry_sound.pack(side='left',expand=True,fill='x')

   sound_file=os.path.dirname(__file__)+"/sound/tmp.wav"
   Label=tk.Label(rec_frame,text="Rec WAV File: ",bg="white",fg="black")
   entry_rec=tk.Entry(rec_frame, textvariable='', bd=2, bg="IndianRed",fg="white")
   entry_rec.insert(0,sound_file)
   Label.pack(side='left')
   entry_rec.pack(side='left',expand=True,fill='x')
   '''

   # Combobox
   style = ttk.Style()
   style.configure("office.TCombobox", selectbackground="Dodgerblue", fieldbackground=text_bg)
   style.configure("office.TCombobox", selectforeground="white", fieldforeground='white')
   style.configure("office.TCombobox", arrowsize=20, arrowcolor='white', background='Dodgerblue4')
   style.configure("office.TCombobox", foreground=text_fg)
   style.configure("office.TCombobox", justify=tk.CENTER, padding=5)

   plt_list = ['再生', 'wavに変換', '全波形とFFT', 'mic録音']
   v = tk.StringVar()
   combobox = ttk.Combobox(play_frame, font=(16), width=15, values=plt_list, style="office.TCombobox")
   combobox.current(0)
   #combobox.bind('<<ComboboxSelected>>', data_plot_execute)
   combobox.pack(side="left")


   def mouse_leave(event):
      text.configure(fg=text_fg,bg=text_bg)

   # Button Frame
   def execute():
      my_dir=os.path.dirname(__file__).replace("/tabs","")
      text.tag_config('finfo_line', background="#a0a7a0", foreground="#0e0a0a")
      if combobox.get()=="再生":
         pgm=my_dir+"/Sound/play_chunk.py "
         cmd="mate-terminal --title='Play Sound' --command \"bash -c 'python3 "+ pgm + selected.get() + "';bash\" --geometry 80x10+200+700 &"
      elif combobox.get()=="wavに変換":
         pgm="ffmpeg -i "
         filePath=selected.get()
         nameRoot, ext = os.path.splitext(os.path.basename(filePath))
         output_wav=filePath.replace(ext,".wav")
         cmd="mate-terminal --title='Play Sound' --command \"bash -c '"+ pgm + \
             selected.get() + " " + output_wav + " ';bash\" --geometry 80x10+200+700 &"
      elif combobox.get()=="全波形とFFT":
         pgm=my_dir+"/Sound/fft_wav.py "
         cmd="mate-terminal --title='Play Sound' --command \"bash -c 'python3 "+ pgm + selected.get() + "';bash\" --geometry 80x10+200+700 &"
      elif combobox.get()=="mic録音":
         pgm=my_dir+"/Sound/mic_wav_rec.py "
         cmd="mate-terminal --title='Play Sound' --command \"bash -c 'python3 "+ pgm + selected.get() + "';bash\" --geometry 80x10+200+700 &"

      #text.delete("1.0",tk.END)
      text.insert("1.0", pgm+"を実行します．\n", 'finfo_line')


      comment=selected.get()+"を再生します\n"
      text.insert("1.0", comment,'finfo_line')
      pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
      res = pro.stderr + '\n'
      text.insert("1.0", res)

   def modify():
      my_dir=os.path.dirname(__file__).replace("/tabs","")
      text.tag_config('finfo_line', background="#a0a7a0", foreground="#0e0a0a")
      if combobox.get()=="再生":
         pgm=my_dir+"/Sound/play_chunk.py"
      elif combobox.get()=="wavに変換":
         text.insert("1.0", "編集対象はありません．\n")
      elif combobox.get()=="全波形とFFT":
         pgm=my_dir+"/Sound/fft_wav.py"
      elif combobox.get()=="mic録音":
         pgm=my_dir+"/Sound/mic_wav_rec.py"

      with open(pgm, "r") as open_file:
          code=open_file.read()
          text.insert("1.0", code)
      cmd='gvim -geometry 120x30+500+300 ' + pgm + ' & '
      pro = subprocess.run(cmd,shell=True)

   play_button = tk.Button(play_frame, text="実行", 
           activebackground='DarkOliveGreen1', activeforeground='red', 
           bg=button_bg, fg=text_fg, 
           padx=5, borderwidth=4,
           command=execute)
   modify_button = tk.Button(rec_frame, text="Modify", 
           activebackground='DarkOliveGreen1', activeforeground='red', 
           bg=button_bg, fg=text_fg, 
           padx=5, borderwidth=4,
           command=modify)

   play_button.pack(side='left',expand=True, fill='x')
   modify_button.pack(side='left')

   def mouse_over_sound_pgm(event):
      text.configure(bg="yellow")
      msg="   [ play_chunk.py を編集します．]\n"+ \
      "   play_chunk.py は「再生」ボタンで実行されます． \n" 
      text.insert("1.0", msg)
   modify_button.bind('<Enter>', mouse_over_sound_pgm)
   modify_button.bind('<Leave>', mouse_leave)

   def mouse_over_sound_play(event):
      text.configure(bg="yellow")
      msg="   [ wav ファイルを再生します．「選ぶ」ボタンでwavファイルが選択されている必要があります． ]\n"+ \
      "   実行ターミナル(Play Sound) 内のキー操作で再生を制御できます． \n"+ \
      "   停止：s \n" + \
      "   再開：c \n" + \
      "   終了：q \n" + \
      "   1-chunk 進む：j \n" + \
      "   1-chunk 戻る：k \n" + \
      "   10-chunk 進む：J \n" + \
      "   10-chunk 戻る：K \n" 
      text.insert("1.0", msg)
   play_button.bind('<Enter>', mouse_over_sound_play)
   play_button.bind('<Leave>', mouse_leave)

