#!/usr/bin/python3
# マウスとキーボードからの入力を読み取る
# 2022 10/18 Yasushi Honda

import matplotlib.pyplot as plt
import time

class Mouse_and_keyinput():
    def __init__(self,fig,ax):
        self.fig=fig
        self.ax=ax
        self.wheel=0
        self.x=0
        self.y=0
        self.key='c'
        self.run=0
        self.ln_v = self.ax.axvline(0)
        self.ln_h = self.ax.axhline(0)

    def motion(self,event):
        if event.xdata!=None:
            self.x=event.xdata
            #print(self.x)

        if event.ydata!=None:
            self.y=event.ydata
            #print(self.y)

        #self.ln_v.set_xdata(self.x)
        #self.ln_h.set_ydata(self.y)

 
    def wheel_scroll(self,event):
 
        if event.button == "up":
            self.wheel -= 1
 
        if event.button == "down":
            self.wheel += 1

    def key_press(self,event):
        self.key=event.key

    def button(self,event):
        if event.button==1:
            if self.run==1:
                self.run=0 # 送信停止
                self.ax.set_facecolor('pink') 
            else:
                self.run=1 # 送信開始
                self.ax.set_facecolor('black') 
        if event.button==2:
            self.key='c'
        if event.button==3:
            self.key='q'
            if self.run==1:
                self.run=0 # 送信停止
                self.ax.set_facecolor('pink') 

    def connect(self):
        self.fig.canvas.mpl_connect('button_press_event', self.button)
        self.fig.canvas.mpl_connect('motion_notify_event', self.motion)
        self.fig.canvas.mpl_connect('scroll_event', self.wheel_scroll)
        self.fig.canvas.mpl_connect('key_press_event', self.key_press)

    def get_value(self):
        return self.x,self.y,self.wheel,self.key,self.run

    def reset_key(self):
        self.key=' '

    def close(self):
        self.wheel=0
        self.x=0
        self.y=0
        self.key=' '
        self.run=0

if __name__=='__main__':

    msky=Mouse_and_keyinput()
 
    plt.title('Control plane',fontsize=20)
    plt.text(s="Input 'e' to execute",x=-0.9,y=0.9,fontsize=20)
    plt.text(s="Input 'q' to quit",x=-0.9,y=0.8,fontsize=20)
    plt.xlim(-1,1)
    plt.ylim(-1,1)
    ln_v = plt.axvline(0,color='red')
    ln_h = plt.axhline(0,color='green')
   
    msky.connect() 

    key='c'
    while key!='q':
        x,y,wheel,key,run=msky.get_value()
        print("\r %6.3f %6.3f %6d %6s" % (x,y,wheel,key),end='')
        plt.pause(0.05)
