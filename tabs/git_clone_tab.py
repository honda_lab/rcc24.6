# CC BY-SA Yasushi Honda 2023 9/11

import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os
import requests, json

common_bg="orange"
text_fg="#071407"
text_bg="#d0e0d0"
button_bg="#ffc666"
howto_bg="#fafaa0"

# ==== git_operation_tab をつくる =====
def const_tab(tab, selected, text, text_frame, local, robot_id, robot_addr, calc_id, calc_addr):

    frame_button = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)
    frame_button.pack(side="top", fill="x")

    text.tag_config('finfo_line', background="#a0a7a0", foreground="#0e0a0a")
    text.tag_config('warning_line', background=howto_bg, foreground=text_fg)

    Label=tk.Label(frame_button,text="URL: ", bg="orange",fg=text_fg)
    Label.pack(side="left",fill='x',padx=4, pady=4)
    url_entry=tk.Entry(frame_button, width=18, bd=2, bg=text_bg, fg="black")
    #url=selected.get()[selected.get().rfind('/'):].strip('/')
    url="http://gitlab.com/honda_lab/"
    url_entry.insert(0,url)
    url_entry.pack(expand=True, side="left", fill="both", padx=4, pady=4)

    def text_pack():
        for widget in text_frame.winfo_children():
            widget.pack_forget()
        text.pack(side="top",expand=True,fill="both")

    def get_project():
        # GitLabのURLとアクセストークンを設定
        gitlab_url = "https://gitlab.com/api/v4/groups/honda_lab/projects"
        access_token = ""
        # リクエストヘッダーの設定
        headers = {"Private-Token": access_token}
        # APIを呼び出してプロジェクト一覧を取得
        response = requests.get(gitlab_url, headers=headers)
        # レスポンスをJSONとして取得
        projects = response.json()
        # 各プロジェクトのnameプロパティだけを含むリストを作成
        names = [project['name'] for project in projects]
        descriptions = [project['description'] for project in projects]
        print(descriptions) 
        project_list=[]
        for name in names:
            print(name)
            project_list.append(name)
        project_cmbbx["values"]=project_list

        '''
        # JSONファイルに書き込む
        with open("project_names.json", "w") as json_file:
            json.dump(project_names, json_file, indent=4)
        '''

    Label=tk.Label(frame_button,text="Project: ", bg="orange",fg=text_fg)
    Label.pack(side="left",fill='both',pady=4)
    project_list=[]
    project_cmbbx = ttk.Combobox(frame_button, width=18, values=project_list, postcommand=get_project, style="office.TCombobox")
    #project_cmbbx.current(0)
    project_cmbbx.pack(expand=True, side="left",fill='both', padx=6,pady=4)

    def how_to():
        my_dir=os.path.dirname(__file__)
        text.tag_config('finfo_line', background=howto_bg, foreground=text_fg)
        fp=open(my_dir+"/howto_clone.txt","r")
        howto_txt=""
        for line in fp.readlines():
            howto_txt+=line
        fp.close()
        text.delete("1.0",tk.END)
        text.insert(tk.END, howto_txt,'finfo_line')

    howto_button = tk.Button(frame_button, text="How To", width=7,
           activebackground='DarkOliveGreen1', activeforeground="red", 
           bg=button_bg, fg=text_fg,
           borderwidth=0, padx=5, command=how_to)
    howto_button.pack(side="right",padx=3,pady=0)

    def execute(event):
        if os.path.isdir(selected.get()):
            my_dir=selected.get()
        elif os.path.isfile(selected.get()):
            my_dir=os.path.dirname(selected.get())
        else:
            comment="File Treeからファイルまたはディレクトリを選んでください\n"
            text.insert("1.0", comment, 'warning_line')
            return

        if combobox.get()=="clone":
            cmd="git clone " + url_entry.get() + project_cmbbx.get()
        elif combobox.get()=="description":
            cmd="mate-terminal --command \"bash -c 'ls ';bash\" --geometry +730+0 &"

        text_pack()
        os.chdir(my_dir)
        text.insert("1.0", cmd+" を実行します．\n", 'finfo_line')
        text.insert("1.0", "File Treeからcsvファイルを選んでください．\n",'finfo_line')

        text.yview_moveto(0)
        pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
        if len(pro.stderr)>0:
            err = pro.stderr + "\n"
            text.insert("1.0", err, 'warning_line')
        res=pro.stdout + '\n'
        text.insert("1.0", res,'finfo_line')

        #text.insert("1.0",pro.stdout)

    Label=tk.Label(frame_button,text="command: ", bg="orange",fg=text_fg)
    Label.pack(side="left",fill='both',pady=4)

    # Button Frame
    # Combobox
    style = ttk.Style()
    style.configure("office.TCombobox", selectbackground="Dodgerblue", fieldbackground=text_bg)
    style.configure("office.TCombobox", selectforeground="white", fieldforeground='white')
    style.configure("office.TCombobox", arrowsize=20, arrowcolor='white', background='Dodgerblue4')
    style.configure("office.TCombobox", foreground=text_fg)
    style.configure("office.TCombobox", justify=tk.CENTER, padding=5)

    cmd_list = ['clone', 'description']
    v = tk.StringVar()
    #combobox = ttk.Combobox(frame_button, width=5, textvariable= v, values=module)
    combobox = ttk.Combobox(frame_button, font=(16), width=12, values=cmd_list, style="office.TCombobox")
    combobox.current()
    combobox.bind('<<ComboboxSelected>>', execute)
    combobox.pack(side="left",fill='both',padx=6,pady=4)

    execute_button = tk.Button(frame_button, text="実行", 
           activebackground='DarkOliveGreen1', activeforeground="red", 
           bg=button_bg, fg=text_fg,
           borderwidth=4, padx=5, command=execute)

    #execute_button.pack(side="left",fill="x",expand=True,padx=3,pady=3)

