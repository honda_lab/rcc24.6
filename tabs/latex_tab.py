# CC BY-SA Yasushi Honda 2023 10/11

import time
import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os
import sys
import cv2
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from threading import Thread
import tabs.mskympl as Msky
import modules.mysocket as sk
#import tree4
import socket

mouse_over_color="DarkOliveGreen1"
common_bg="orange"
text_fg="#071407"
text_bg="#d6e0d6"


# ==== Latex tab をつくる =====
def const_tab(tab,selected,local,robot_id, robot_addr, calc_id, calc_addr):
   # Frameの定義と配置
   button_frame = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)

   button_frame.pack(side="top",fill="x")

   '''
   # How To を読み込む
   my_dir=os.path.expanduser("~")
   fp=open(my_dir+"/howto.txt","r")
   copy_right=""
   for line in fp.readlines():
      copy_right+=line
   fp.close()
   text_editor.insert(tk.END, copy_right)
   '''

   # Latex commands
   def latex():
      filePath=selected.get()
      print(filePath)
      current_dir=os.path.dirname(filePath)
      print(current_dir)
      os.chdir(current_dir)
      cmd="mate-terminal --title='Execute' --command \"bash -c 'platex " + filePath + "';bash\" --geometry 80x10+200+600 "
      #cmd="platex " + filePath
      proc = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
      text_editor.delete("1.0",tk.END)
      text_editor.insert("1.0",cmd+"\n")
      text_editor.insert("1.0",proc.stdout)

   latex_button = tk.Button(button_frame, activebackground='DarkOliveGreen1', bg='white', fg='black', text="platex", padx=0, command=latex,borderwidth=4)
   latex_button.pack(side="left",padx=3,pady=4)

   def show_pdf():
      filePath=selected.get()
      print(filePath)
      current_dir=os.path.dirname(filePath)
      print(current_dir)
      os.chdir(current_dir)
      cmd="mate-terminal --title='Execute' --command \"bash -c 'dvipdfmx " + filePath.replace("tex","dvi") + \
         "; atril " + filePath.replace("tex","pdf") + \
         " ';bash\" --geometry 80x10+200+600 &"

      '''
      cmd="dvipdfmx " + filePath.replace("tex","dvi") 
      text_editor.insert("1.0",cmd+"\n")
      proc = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
      #text_editor.delete("1.0",tk.END)
      text_editor.insert("1.0",proc.stdout)
      text_editor.insert("1.0",proc.stderr)
      cmd="atril " + filePath.replace("tex","pdf") 
      text_editor.insert("1.0",cmd+"\n")
      #proc = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
      '''
      proc = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
      #proc=subprocess.Popen(cmd.strip().split(' '))

   # ボタンフレーム
   pdf_button = tk.Button(button_frame, activebackground='DarkOliveGreen1', bg='white', fg='black', text="show pdf", padx=0, command=show_pdf, borderwidth=4)
   pdf_button.pack(side="left",padx=3,pady=4)

   def mouse_leave(event):
      text_editor.configure(fg=text_fg,bg=text_bg)
      #text_editor.delete("1.0",tk.END)

