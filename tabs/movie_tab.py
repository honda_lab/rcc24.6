# CC BY-SA Yasushi Honda 2023 9/11
# cf https://qiita.com/yutaka_m/items/f3bb883a5ffc860fcfca
import time
import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os
import cv2
from PIL import Image,ImageTk
import threading

common_bg="orange"
text_fg="#071407"
text_bg="#d0e0d0"
button_bg="#ffc666"
howto_bg="#fafaa0"

# ==== git_operation_tab をつくる =====
def const_tab(tab, text_frame, selected, text, local,robot_id, robot_addr, calc_id, calc_addr):

   frame_button = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)
   frame_button.pack(side="top", fill="x")

   text.tag_config('finfo_line', background="#a0a7a0", foreground="#0e0a0a")
   text.tag_config('warning_line', background=howto_bg, foreground=text_fg)

   Label=tk.Label(frame_button,text="コメント: ", bg="orange",fg=text_fg)
   Label.pack(side="left",fill='x')

   comment_entry=tk.Entry(frame_button,bg=text_bg,fg=text_fg)
   comment_entry.pack(side='left',padx=6)

   global frame_num
   frame_num=0
   global video_set
   video_set=False 
   global cap
   def play_video(filePath):
       for widget in text_frame.winfo_children():
           try:
              widget.pack_forget()
           except:
              print('widgetのpack_forgetできません')

       global cap
       cap = cv2.VideoCapture(filePath)
       if (cap.isOpened()== False):  
          print("ビデオファイルを開くとエラーが発生しました") 

       # 1枚目のframe
       ret, frame = cap.read()
       cv_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
       # NumPyのndarrayからPillowのImageへ変換
       readImage = Image.fromarray(cv_image)

       width=text_frame.winfo_width()
       height = int(readImage.height/readImage.width * width)
       print(width,height)


       showImage = readImage.resize((width,height))
       global imagetk # ガーベージコレクションでメモリから開放されないため必要
       imagetk = ImageTk.PhotoImage(showImage,master=text_frame)

       canvas = tk.Canvas(text_frame, width=width, height=height)
       #canvas.pack(anchor=tk.NW,expand=True)
       canvas.pack(side='top')
       created_img=canvas.create_image(width/2, height/2, image=imagetk)

       total_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
       global fps
       fps = int(cap.get(cv2.CAP_PROP_FPS))
       print("fps, length=%d, %5.1f" % (fps,total_frame/fps)) 
       global speed
       speed=fps

       length=total_frame/fps
       total_minute=length//60
       total_second=length%60
       Position=tk.Label(text_frame,bg=text_bg)
       Position.pack(side='top',fill='x')

       def cap_read():
          global video_pause
          global frame_num
          global video_set
          global length_str
          global fps
          video_set=True
          video_pause=False
          frame_num=0
          now=time.time()
          start=now
          cap.set(cv2.CAP_PROP_POS_FRAMES, frame_num)
          while video_set:
             if video_pause==False:
                ret, frame = cap.read()
                cv_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                readImage = Image.fromarray(cv_image)
                showImage = readImage.resize((width,height))
                imagetk = ImageTk.PhotoImage(showImage)
                created_img=canvas.create_image(width/2, height/2, image=imagetk)
                canvas.itemconfig(created_img, image=imagetk)

                play_time = int(cap.get(cv2.CAP_PROP_POS_MSEC)) # 再生位置の時間取得
                now=time.time()
                elapsed=(now-start)*1000  # msec
                sleep = max(1, int(play_time - elapsed))
                #cv2.waitKey(sleep)

                length=frame_num/fps
                minute=length//60; second=length%60
                length_str=str(" %d\' %d\" / %d\' %d\" (min:sec)" %
                        (minute,second,total_minute,total_second))
                Position.configure(text=length_str)

                now=time.time()
                period=now-start
                if period<1/fps:
                   time.sleep(1/fps-period)
                start=now
                frame_num+=1

       video_set=True
       global cap_thread
       cap_thread = threading.Thread(target=cap_read)
       cap_thread.setDaemon(True)
       cap_thread.start()

       def show_fw(event):
          global imagetk # ガーベージコレクションでメモリから開放されないため必要
          global frame_num
          global length_str
          #print("%5.2f ％" % (frame_num/total_frame*100)) 
          if frame_num<total_frame:
             cap.set(cv2.CAP_PROP_POS_FRAMES, frame_num)
             ret, frame = cap.read()
             frame_num+=speed
             # BGR→RGB変換
             cv_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
             # NumPyのndarrayからPillowのImageへ変換
             readImage = Image.fromarray(cv_image)

             showImage = readImage.resize((width,height))
             imagetk = ImageTk.PhotoImage(showImage,master=text_frame)

             canvas.create_image(width/2, height/2, image=imagetk)

             pos=frame_num/fps
             minute=pos//60
             second=pos%60
             pos_str=str("再生時間: %d\':%d\" "%(minute,second))
             Position.configure(text=pos_str+length_str)

       def show_bk(event):
          global imagetk # ガーベージコレクションでメモリから開放されないため必要
          global frame_num
          global length_str
          #print("%5.2f ％" % (frame_num/total_frame*100)) 
          if frame_num>speed:
             frame_num-=speed
             cap.set(cv2.CAP_PROP_POS_FRAMES, frame_num)
             ret, frame = cap.read()
             # BGR→RGB変換
             cv_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
             # NumPyのndarrayからPillowのImageへ変換
             readImage = Image.fromarray(cv_image)

             showImage = readImage.resize((width,height))
             imagetk = ImageTk.PhotoImage(showImage,master=text_frame)

             canvas.create_image(width/2, height/2, image=imagetk)

             pos=frame_num/fps
             minute=pos//60
             second=pos%60
             pos_str=str("再生時間: %d\':%d\" "%(minute,second))
             Position.configure(text=pos_str+length_str)

       canvas.bind("<ButtonPress-4>", show_bk)
       canvas.bind("<ButtonPress-5>", show_fw)

   def stop():
      global video_set
      global cap_thread
      global cap
      video_set=False  ## threadを止める
      cap.release()

   def pause():
      global video_pause
      video_pause=True

   def restart():
      global video_pause
      video_pause=False

   def how_to():
      my_dir=os.path.dirname(__file__)
      text.tag_config('finfo_line', background=howto_bg, foreground=text_fg)
      fp=open(my_dir+"/howto_git.txt","r")
      howto_txt=""
      for line in fp.readlines():
         howto_txt+=line
      fp.close()
      text.delete("1.0",tk.END)
      text.insert(tk.END, howto_txt,'finfo_line')

   howto_button = tk.Button(frame_button, text="How To", width=7,
           activebackground='DarkOliveGreen1', activeforeground="red", 
           bg=button_bg, fg=text_fg,
           borderwidth=0, padx=0, command=how_to)
   howto_button.pack(side="right",fill="both", padx=0,pady=0)

   def execute():
      if os.path.isdir(selected.get()):
         my_dir=selected.get()
      elif os.path.isfile(selected.get()):
         my_dir=os.path.dirname(selected.get())
      else:
         comment="File Treeからファイルまたはディレクトリを選んでください\n"
         text.insert("1.0", comment, 'warning_line')
         return

      root=os.path.dirname(__file__).replace('/tabs','')
      if combobox.get()=="play":
         play_video(selected.get())
         cmd="None"
      elif combobox.get()=="push":
         cmd="git push" 
      elif combobox.get()=="add":
         cmd="git add " + selected.get()
      elif combobox.get()=="pull":
         cmd="git pull "
      elif combobox.get()=="store":
         cmd="git config --global credential.helper store"
      elif combobox.get()=="checkout":
         cmd="git checkout " + branch_cmbbx.get()

      #os.chdir(my_dir)
      if cmd!="None":
         text.insert("1.0", cmd+" を実行します．\n", 'finfo_line')

         pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
         if len(pro.stderr)>0:
            err = pro.stderr + "\n"
            text.insert("1.0", err, 'warning_line')
         res=pro.stdout + '\n'
         text.insert("1.0", res,'finfo_line')

   Label=tk.Label(frame_button,text="command: ", bg="orange",fg=text_fg)
   Label.pack(side="left",fill='x')

   # Button Frame
   # Combobox
   style = ttk.Style()
   style.configure("office.TCombobox", selectbackground="Dodgerblue", fieldbackground=text_bg)
   style.configure("office.TCombobox", selectforeground="white", fieldforeground='white')
   style.configure("office.TCombobox", arrowsize=15, arrowcolor='white', background='Dodgerblue4')
   style.configure("office.TCombobox", foreground=text_fg)
   style.configure("office.TCombobox", justify=tk.CENTER, padding=0)

   cmd_list = ['play', 'cut left', 'cut right', 'comment ', 'cut']
   v = tk.StringVar()
   #combobox = ttk.Combobox(frame_button, width=5, textvariable= v, values=module)
   combobox = ttk.Combobox(frame_button, font=(10), width=10, values=cmd_list, style="office.TCombobox")
   combobox.current(0)
   #combobox.bind('<<ComboboxSelected>>', execute)
   combobox.pack(side="left",padx=6,pady=0)

   execute_button = tk.Button(frame_button, text="実行", 
           activebackground='DarkOliveGreen1', activeforeground="red", 
           bg=button_bg, fg=text_fg,
           borderwidth=0, padx=0, command=execute)
   execute_button.pack(side="left",fill="both",expand=True,padx=0,pady=0)

   stop_button = tk.Button(frame_button, text="stop", 
           activebackground='DarkOliveGreen1', activeforeground="red", 
           bg=button_bg, fg=text_fg,
           borderwidth=0, padx=0, command=stop)
   stop_button.pack(side="left",fill="both",expand=True,padx=0,pady=0)

   pause_button = tk.Button(frame_button, text="pause", 
           activebackground='DarkOliveGreen1', activeforeground="red", 
           bg=button_bg, fg=text_fg,
           borderwidth=0, padx=0, command=pause)
   pause_button.pack(side="left",fill="both",expand=True,padx=0,pady=0)

   restart_button = tk.Button(frame_button, text="restart", 
           activebackground='DarkOliveGreen1', activeforeground="red", 
           bg=button_bg, fg=text_fg,
           borderwidth=0, padx=0, command=restart)
   restart_button.pack(side="left",fill="both",expand=True,padx=0,pady=0)
