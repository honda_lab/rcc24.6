# CC BY-SA Yasushi Honda 2023 10/11
# メインタブ
import time
import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import tabs.mskympl as Msky
#import modules.mysocket as sk
#import socket
import threading
from modules import pycode

mouse_over_color="DarkOliveGreen1"
common_bg="orange"
text_fg="#071407"
text_bg="#d6e0d6"
button_bg="#ffc666"
howto_bg="#fafaa0"


# ==== メイン tab をつくる =====
def const_tab(tab, text_frame, text_editor,
        local,robot_id, robot_addr, calc_id, calc_addr, selected_entry):
    # Frameの定義と配置
    button_frame = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)
    button_frame.pack(fill='x')

    # Local entry
    Label=tk.Label(button_frame,text=" Address: ", bg="orange",fg=text_fg, height=1, padx=5)
    Label.pack(side="left",padx=0)
    local_entry=tk.Entry(button_frame, width=17, bd=2, bg="IndianRed",fg="white")
    local_entry.insert(0,local)
    local_entry.pack(side="left",fill='y',padx=0,pady=4)

    def how_to():
        for widget in text_frame.winfo_children():
            widget.pack_forget()
        text_editor.pack(side="top",expand=True,fill="both")

        my_dir=os.path.dirname(__file__)
        fp=open(my_dir+"/howto_main.txt","r")
        howto_txt=""
        for line in fp.readlines():
            howto_txt+=line
        fp.close()
        text_editor.tag_config('finfo_line', background=howto_bg, foreground=text_fg)
        text_editor.delete("1.0",tk.END)
        text_editor.insert(tk.END, howto_txt,'finfo_line')

    def mouse_leave(event):
        text_editor.configure(fg=text_fg,bg=text_bg)
        #text_editor.delete("1.0",tk.END)
   
    global panel_state
    panel_state="off"

    # コントロールパネルを起動する関数
    def pnl_onoff():
        global panel_state
        my_dir=os.path.dirname(__file__).replace("/tabs","")
        #my_dir=os.getcwd()

        program='cpanel.py'

        try: # program がすでに実行されている場合
            cmd='pgrep -c ' + program
            prc=subprocess.run(cmd,shell=True,capture_output=True,text=True)
            print("The number of %s : %3d" % (program,int(prc.stdout)),flush=True)
            if int(prc.stdout)>0:
                text_editor.configure(bg='red')
                text_editor.insert("0.0","\n 制御パネルはすでに実行中です． \n \
                                    再実行するためには，すでに実行中の制御パネルを閉じてください．\n")
            else:
                text_editor.configure(bg=text_bg)
                try:
                    cmd=my_dir+"/"+program+" " + local_entry.get() + " " + robot_addr.get() + " " + calc_addr.get()
                    print(cmd)
                    proc=subprocess.Popen(cmd.strip().split(' '))
                except:
                    text_editor.configure(bg='red')
                    text_editor.insert("0.0","\n 制御パネルの起動に失敗 \n")
        except:
            print("%s 実行中の確認に失敗しました．" % program)


    def text_pack():
        for widget in text_frame.winfo_children():
            widget.pack_forget()
        text_editor.pack(side="top",expand=True,fill="both")

    def remote_execute(exec_type):
        filepath=selected_entry.get()
        try:
            program=re.findall('[^/]+.py',filepath, flags=re.IGNORECASE)[0]
            # filepath の末尾 .py から遡って最初の/までの文字列を抜き出す．
        except:
            text_pack()
            text_editor.configure(bg="red",fg="white")   
            msg="   [ プログラムの指定にエラーがあります ]\n \n"
            text_editor.insert("1.0", msg)
            return

        cmd="scp " + filepath + " "+robot_id.get()+"@" + robot_addr.get() + ": "
        comment=program + "をリモートにコピーします．\n"
        text_editor.yview_moveto(1)
        text_editor.configure(fg="black", bg="yellow")
        text_editor.insert(tk.END, comment)
        pro=subprocess.Popen(cmd.strip().split(' '),stdout=subprocess.PIPE,stderr=subprocess.STDOUT)

        pb.start(20) # バーを動かす間隔を指定してスタート

        def exec_rcp():
            while True:
                line=pro.stdout.readline()
                err=pro.stderr
                if line:
                    #print(line)
                    text_editor.configure(bg='orange',fg='black')
                    text_editor.yview_moveto(1)
                    text_editor.insert(tk.END, line)
                    #text_editor.insert("1.0", err)
                if not line and pro.poll() is not None:
                    text_editor.configure(bg='orange',fg='black')
                    # textの背景色をもとに戻す．
                    #text_editor.configure(fg="black", bg=text_bg)
                    break
            pb.stop()  #destroy sub-window

        thread=threading.Thread(target=exec_rcp)
        thread.start()

        text_editor.configure(fg="black", bg="DarkOliveGreen3")

        if exec_type=="micropython": 
            cmd="ssh "+robot_id.get()+"@" + robot_addr.get() + " micropython " + program + " & "
            pro=subprocess.Popen(cmd.strip().split(' '),stdout=subprocess.PIPE,stderr=subprocess.STDOUT,text=True)
            pb.start(20) # バーを動かす間隔を指定してスタート
        elif exec_type=="terminal":
            command="ssh " + robot_id.get()+"@" + robot_addr.get() + " python3 -u " + program \
            + "; bash"
            cmd="mate-terminal -e '" + command + "' " + \
                  " --geometry 100x25+740+120 "
            #cmd="gnome-terminal " + \
            #" -- bash -c \"" + command + "\" &"
            #pro = subprocess.run(cmd,shell=True ,text=True,stdout=subprocess.PIPE)
            pro=subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE,stderr=subprocess.STDOUT,text=True)
            #pro=subprocess.Popen(cmd.strip().split(' '),stdout=subprocess.PIPE,stderr=subprocess.STDOUT,text=True)
        else:
            print("exec_typeが正しくありません。")
    
        # リモートでpythonを実行する場合 -u オプションをつけてバッファリングさせない．ー＞リアルタイム表示可

        print(cmd)
        comment=program + "をリモート実行します．プログラムからの出力はここに表示されます．\n"
        text_editor.insert(tk.END, comment)
        #pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)

        # リアルタイムにtext_editorに表示するためにthreadにしてinsertする．
        def exec_print():
            while True:
                line=pro.stdout.readline()
                if line:
                    #print(line)
                    text_editor.configure(bg='orange',fg='black')
                    text_editor.yview_moveto(1)
                    text_editor.insert(tk.END, line)
                if not line and pro.poll() is not None:
                    text_editor.configure(bg='orange',fg='black')
                    # textの背景色をもとに戻す．
                    #text_editor.configure(fg="black", bg=text_bg)
                    #stdout, stderr = pro.communicate()
                    #print(stderr)
                    break
            pb.stop()  #destroy sub-window

        thread=threading.Thread(target=exec_print)
        thread.start()

   # ボタンフレーム
    pnl_onoff_button = tk.Button(button_frame, text="制御パネル", 
           activebackground='DarkOliveGreen1', bg=button_bg, fg=text_fg, 
           padx=0, command=pnl_onoff,borderwidth=2)
    pnl_onoff_button.pack(side="left", expand=True, fill='x', padx=0,pady=4)

    '''
    comb_list = ['##', 'while|for|try|if', 'class', 'def', 'while', 'for', 'if']
    v = tk.StringVar()
    combobox = ttk.Combobox(button_frame, font=(16), width=25, values=comb_list, #: コンボボックス
            style="office.TCombobox")
    combobox.current(0)
    combobox.bind('<<ComboboxSelected>>', data_plot_execute)
    combobox.pack(side="left", padx=0,pady=0)
    '''

    exec_button = tk.Button(button_frame, text="端末実行", 
           activebackground='DarkOliveGreen1', bg='orange', fg=text_fg, 
                           padx=5, command=lambda:remote_execute("terminal"), borderwidth=2)
    exec_button.pack(side="left", fill='x', padx=0,pady=4)

    ev3exec_button = tk.Button(button_frame, text="EV3実行", 
           activebackground='DarkOliveGreen1', bg='DodgerBlue', fg="white", 
           padx=5, command=lambda:remote_execute("micropython"),borderwidth=2)
    ev3exec_button.pack(side="left", fill='x', padx=0,pady=4)

    pb = ttk.Progressbar(button_frame, mode="indeterminate",length=200)
    pb.pack(side="left",fill="both",pady=5)

    howto_button = tk.Button(button_frame, text="How to", 
           activebackground='DarkOliveGreen1', bg=button_bg, fg=text_fg, 
           padx=5, command=how_to,borderwidth=2)
    howto_button.pack(side="right", fill='x', padx=4,pady=4)


    return local_entry

