# CC BY-SA Yasushi Honda 2023 9/11

import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os


# ===== pos_ident_tab をつくる =====
def const_tab(tab):
   frame_button = tk.Frame(tab, relief=tk.RAISED, bd=2, bg='NavajoWhite2')
   frame_input = tk.Frame(tab, relief=tk.RAISED, bd=2, bg="pink", width=500)
   frame_radio = tk.Frame(tab, relief=tk.RAISED, bd=2, bg="white", width=500)
   frame_text = tk.Frame(tab, relief=tk.RAISED, bd=2, bg="white", width=500)
   # フレームの配置
   frame_input.grid(row=0, column=1,sticky="ns")
   frame_radio.grid(row=1, column=1,sticky="ns")
   frame_text.grid(row=2, column=1,sticky="ns")
   frame_button.grid(rowspan=3,  row=0, column=0,sticky="ns")

   # text Frame
   text = scrolledtext.ScrolledText(frame_text,bg="LightSkyBlue1",fg="black",width=102,height=12)
   text.grid(row=0, column=0, sticky="ew")

   msg=" -------------------- \n" + \
       " これは，celloの運指候補のタブです． \n" + \
       " 音階から運指候補を決める試みです．\n" + \
       " 指位置は，弦(A=1,D=2,G=3,G=4)，ポジション(1,L2,H2,L3,H3(3),4,5,6,7,8)，指番号(1,2,3,4)の\n" + \
       " 3つの組み合わせで表します．\n"
   text.insert("1.0",msg)

   # Entry Frame
   global sound_file
   Label=tk.Label(frame_input,text=" WAV File: ",bg="white",fg="black")
   entry_wav=tk.Entry(frame_input, textvariable='', width=55, bd=7, bg="IndianRed",fg="white")
   entry_wav.insert(0,'')
   Label.grid(row=0, column=0, sticky="ew")
   entry_wav.grid(row=0, column=1, sticky=tk.EW)

   sound_file=os.path.dirname(__file__)+"/sound/tmp.wav"
   Label=tk.Label(frame_input,text="Rec WAV File: ",bg="white",fg="black")
   entry_rec=tk.Entry(frame_input, textvariable='', width=55, bd=10, bg="IndianRed",fg="white")
   entry_rec.insert(0,sound_file)
   Label.grid(row=1, column=0, sticky="ew")
   entry_rec.grid(row=1, column=1, sticky=tk.EW)

   def mouse_leave(event):
      text.configure(fg="black",bg="LightSkyBlue")

   def scale_click():
      # ラジオボタンの値を取得
      value = scale_value.get()
      text.insert("1.0","運指は "+ str(value) +" です．\n")
      play_file=os.path.dirname(__file__)+"/sound/cello_wav/"+str(value)+".wav"
      text.insert("1.0",play_file+"\n")
      entry_wav.delete(0,tk.END)
      entry_wav.insert(0,play_file)
      play_wav()
   

   # 音階を選ぶラジオボタン
   scale_value = tk.IntVar(value = 0)     # 初期値を設定する場合
   Label=tk.Label(frame_radio,text="A線:",bg="white",fg="black",width=7)
   Label.grid(row=2, column=0, sticky=tk.E)
   A3_radiob = tk.Radiobutton(frame_radio,text="A3",command = scale_click,variable=scale_value,
                           value=100, width=2, height=2,bg="orange")
   A3_radiob.grid(row=2, column=1, sticky=tk.EW)
   A3sh_radiob = tk.Radiobutton(frame_radio,text="A3#",command = scale_click,variable=scale_value,
                           value=101, width=2, height=2,bg="LightBlue1",fg="black")
   A3sh_radiob.grid(row=2, column=2, sticky=tk.EW)
   B3_radiob = tk.Radiobutton(frame_radio,text="B3",command = scale_click,variable=scale_value,
                           value=111, width=2, height=2,bg="DarkOliveGreen1")
   B3_radiob.grid(row=2, column=3, sticky=tk.EW)
   C4_radiob = tk.Radiobutton(frame_radio,text="C4",command = scale_click,variable=scale_value,
                           value=112, width=2, height=2,bg="DarkOliveGreen1")
   C4_radiob.grid(row=2, column=4, sticky=tk.EW)
   C4sh_radiob = tk.Radiobutton(frame_radio,text="C4#",command = scale_click,variable=scale_value,
                           value=113, width=2, height=2,bg="DarkOliveGreen1")
   C4sh_radiob.grid(row=2, column=5, sticky=tk.EW)
   D4_radiob = tk.Radiobutton(frame_radio,text="D4",command = scale_click,variable=scale_value,
                           value=114, width=2, height=2,bg="DarkOliveGreen1")
   D4_radiob.grid(row=2, column=6, sticky=tk.EW)
   D4sh_radiob = tk.Radiobutton(frame_radio,text="D4#",command = scale_click,variable=scale_value,
                           value=131, width=2, height=2,bg="cornsilk2")
   D4sh_radiob.grid(row=2, column=7, sticky=tk.EW)
   E4_radiob = tk.Radiobutton(frame_radio,text="E4",command = scale_click,variable=scale_value,
                           value=141, width=2, height=2,bg="lightsalmon")
   E4_radiob.grid(row=2, column=8, sticky=tk.EW)
   F4_radiob = tk.Radiobutton(frame_radio,text="F4",command = scale_click,variable=scale_value,
                           value=142, width=2, height=2,bg="lightsalmon")
   F4_radiob.grid(row=2, column=9, sticky=tk.EW)
   F4sh_radiob = tk.Radiobutton(frame_radio,text="F4#",command = scale_click,variable=scale_value,
                           value=143, width=2, height=2,bg="lightsalmon")
   F4sh_radiob.grid(row=2, column=10, sticky=tk.EW)
   G4_radiob = tk.Radiobutton(frame_radio,text="G4",command = scale_click,variable=scale_value,
                           value=144, width=2, height=2,bg="lightsalmon")
   G4_radiob.grid(row=2, column=11, sticky=tk.EW)

   G4sh_radiob = tk.Radiobutton(frame_radio,text="G4#",command = scale_click,variable=scale_value,
                           value=161, width=4, height=2,bg="coral")
   G4sh_radiob.grid(row=3, column=2, sticky=tk.EW)
   A4_radiob = tk.Radiobutton(frame_radio,text="A4",command = scale_click,variable=scale_value,
                           value=162, width=2, height=2,bg="coral")
   A4_radiob.grid(row=3, column=3, sticky=tk.EW)
   A4sh_radiob = tk.Radiobutton(frame_radio,text="A4#",command = scale_click,variable=scale_value,
                           value=163, width=3, height=2,bg="coral")
   A4sh_radiob.grid(row=3, column=4, sticky=tk.EW)
   B4_radiob = tk.Radiobutton(frame_radio,text="B4",command = scale_click,variable=scale_value,
                           value=171, width=2, height=2,bg="orangered",fg="black")
   B4_radiob.grid(row=3, column=5, sticky=tk.EW)
   C5_radiob = tk.Radiobutton(frame_radio,text="C5",command = scale_click,variable=scale_value,
                           value=172, width=2, height=2,bg="orangered",fg="black")
   C5_radiob.grid(row=3, column=6, sticky=tk.EW)
   C5sh_radiob = tk.Radiobutton(frame_radio,text="C5#",command = scale_click,variable=scale_value,
                           value=173, width=3, height=2,bg="orangered",fg="black")
   C5sh_radiob.grid(row=3, column=7, sticky=tk.EW)
   D5_radiob = tk.Radiobutton(frame_radio,text="D5",command = scale_click,variable=scale_value,
                           value=181, width=2, height=2,bg="hotpink")
   D5_radiob.grid(row=3, column=8, sticky=tk.EW)

   Label=tk.Label(frame_radio,text="D線:",bg="white",fg="black",width=7)
   Label.grid(row=4, column=0, sticky=tk.E)
   D3_radiob = tk.Radiobutton(frame_radio,text="D3",command = scale_click,variable=scale_value,
                           value=200, width=2, height=2,bg="orange")
   D3_radiob.grid(row=4, column=1, sticky=tk.EW)
   D3sh_radiob = tk.Radiobutton(frame_radio,text="D3#",command = scale_click,variable=scale_value,
                           value=201, width=3, height=2,bg="LightBlue1",fg="black")
   D3sh_radiob.grid(row=4, column=2, sticky=tk.EW)
   E3_radiob = tk.Radiobutton(frame_radio,text="E3",command = scale_click,variable=scale_value,
                           value=211, width=2, height=2,bg="DarkOliveGreen1")
   E3_radiob.grid(row=4, column=3, sticky=tk.EW)
   F3_radiob = tk.Radiobutton(frame_radio,text="F3",command = scale_click,variable=scale_value,
                           value=212, width=2, height=2,bg="DarkOliveGreen1")
   F3_radiob.grid(row=4, column=4, sticky=tk.EW)
   F3sh_radiob = tk.Radiobutton(frame_radio,text="F3#",command = scale_click,variable=scale_value,
                           value=213, width=3, height=2,bg="DarkOliveGreen1")
   F3sh_radiob.grid(row=4, column=5, sticky=tk.EW)
   G3_radiob = tk.Radiobutton(frame_radio,text="G3",command = scale_click,variable=scale_value,
                           value=214, width=2, height=2,bg="DarkOliveGreen1")
   G3_radiob.grid(row=4, column=6, sticky=tk.EW)
   G3sh_radiob = tk.Radiobutton(frame_radio,text="G3#",command = scale_click,variable=scale_value,
                           value=231, width=3, height=2,bg="cornsilk2")
   G3sh_radiob.grid(row=4, column=7, sticky=tk.EW)
   A32_radiob = tk.Radiobutton(frame_radio,text="A3",command = scale_click,variable=scale_value,
                           value=241, width=3, height=2,bg="lightsalmon")
   A32_radiob.grid(row=4, column=8, sticky=tk.EW)
   A3sh2_radiob = tk.Radiobutton(frame_radio,text="A3#",command = scale_click,variable=scale_value,
                           value=242, width=3, height=2,bg="lightsalmon")
   A3sh2_radiob.grid(row=4, column=9, sticky=tk.EW)
   B32_radiob = tk.Radiobutton(frame_radio,text="B3",command = scale_click,variable=scale_value,
                           value=243, width=3, height=2,bg="lightsalmon")
   B32_radiob.grid(row=4, column=10, sticky=tk.EW)
   C42_radiob = tk.Radiobutton(frame_radio,text="C4",command = scale_click,variable=scale_value,
                           value=244, width=3, height=2,bg="lightsalmon")
   C42_radiob.grid(row=4, column=11, sticky=tk.EW)


   Label=tk.Label(frame_radio,text="G線:",bg="white",fg="black",width=7)
   Label.grid(row=5, column=0, sticky=tk.E)
   G2_radiob = tk.Radiobutton(frame_radio,text="G2",command = scale_click,variable=scale_value,
                           value=300, width=2, height=2,bg="orange")
   G2_radiob.grid(row=5, column=1, sticky=tk.EW)
   G2sh_radiob = tk.Radiobutton(frame_radio,text="G2#",command = scale_click,variable=scale_value,
                           value=301, width=4, height=2,bg="LightBlue1",fg="black")
   G2sh_radiob.grid(row=5, column=2, sticky=tk.EW)
   A2_radiob = tk.Radiobutton(frame_radio,text="A2",command = scale_click,variable=scale_value,
                           value=311, width=2, height=2,bg="DarkOliveGreen1")
   A2_radiob.grid(row=5, column=3, sticky=tk.EW)
   A2sh_radiob = tk.Radiobutton(frame_radio,text="A2#",command = scale_click,variable=scale_value,
                           value=312, width=3, height=2,bg="DarkOliveGreen1")
   A2sh_radiob.grid(row=5, column=4, sticky=tk.EW)
   B2_radiob = tk.Radiobutton(frame_radio,text="B2",command = scale_click,variable=scale_value,
                           value=313, width=2, height=2,bg="DarkOliveGreen1")
   B2_radiob.grid(row=5, column=5, sticky=tk.EW)
   C3_radiob = tk.Radiobutton(frame_radio,text="C3",command = scale_click,variable=scale_value,
                           value=314, width=2, height=2,bg="DarkOliveGreen1")
   C3_radiob.grid(row=5, column=6, sticky=tk.EW)
   C3sh_radiob = tk.Radiobutton(frame_radio,text="C3#",command = scale_click,variable=scale_value,
                           value=331, width=3, height=2,bg="cornsilk2")
   C3sh_radiob.grid(row=5, column=7, sticky=tk.EW)


   Label=tk.Label(frame_radio,text="C線:",bg="white",fg="black",width=7)
   Label.grid(row=6, column=0, sticky=tk.E)
   C2_radiob = tk.Radiobutton(frame_radio,text="C2",command = scale_click,variable=scale_value,
                           value=400, width=2, height=2,bg="orange")
   C2_radiob.grid(row=6, column=1, sticky=tk.EW)
   C2sh_radiob = tk.Radiobutton(frame_radio,text="C2#",command = scale_click,variable=scale_value,
                           value=401, width=3, height=2,bg="LightBlue1",fg="black")
   C2sh_radiob.grid(row=6, column=2, sticky=tk.EW)
   D2_radiob = tk.Radiobutton(frame_radio,text="D2",command = scale_click,variable=scale_value,
                           value=411, width=2, height=2,bg="DarkOliveGreen1")
   D2_radiob.grid(row=6, column=3, sticky=tk.EW)
   D2sh_radiob = tk.Radiobutton(frame_radio,text="D2#",command = scale_click,variable=scale_value,
                           value=412, width=3, height=2,bg="DarkOliveGreen1")
   D2sh_radiob.grid(row=6, column=4, sticky=tk.EW)
   E2_radiob = tk.Radiobutton(frame_radio,text="E2",command = scale_click,variable=scale_value,
                           value=413, width=2, height=2,bg="DarkOliveGreen1")
   E2_radiob.grid(row=6, column=5, sticky=tk.EW)
   F2_radiob = tk.Radiobutton(frame_radio,text="F2",command = scale_click,variable=scale_value,
                           value=414, width=2, height=2,bg="DarkOliveGreen1")
   F2_radiob.grid(row=6, column=6, sticky=tk.EW)
   F2sh_radiob = tk.Radiobutton(frame_radio,text="F2#",command = scale_click,variable=scale_value,
                           value=431, width=3, height=2,bg="cornsilk2")
   F2sh_radiob.grid(row=6, column=7, sticky=tk.EW)


   # Button Frame
   def select_wav():
      global filepath,dirname
      typ = [("Wav Files", "*.wav")]
      filepath = askopenfilename(defaultextension="*.wav",filetypes=typ)
      dirname = os.path.dirname(filepath)    

      entry_wav.delete(0,tk.END)
      entry_wav.insert(0, filepath)
      text.insert("1.0", filepath + "を認識します．\n" + "------ \n")

   def play_wav():
      pgm=os.path.dirname(__file__)+"/sound/play_chunk.py "
      cmd="mate-terminal --title='Play Sound' --command \"bash -c 'python3 "+ pgm + entry_wav.get() + "';bash\" --geometry 80x10+200+700 &"
      comment=entry_wav.get()+"を再生します\n" + "------ \n"
      text.insert("1.0", comment)
      pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
      res = pro.stderr + '\n'
      text.insert("1.0", res)

   def fft_wav():
      pgm=os.path.dirname(__file__)+"/sound/fft_wav.py "
      cmd="mate-terminal --title='FFT Sound' --command \"bash -c 'python3 " + pgm  + entry_wav.get() + "';bash\" --geometry 20x10+300+200 &"
      comment=entry_wav.get()+"を解析(fft)します\n"
      text.insert("1.0", comment)
      pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
      res = pro.stderr + '\n'
      text.insert("1.0", res)

   def pident_pgm_open():
      pgm_file=os.path.dirname(__file__)+"/sound/pos_from_wav.py"
      cmd='gvim -geometry 120x30+500+300 ' + pgm_file + ' & '
      pro = subprocess.run(cmd,shell=True)

   def pos_from_wav():
      pgm_file=os.path.dirname(__file__)+"/sound/pos_from_wav.py"
      cmd=pgm_file + ' & '
      pro = subprocess.run(cmd,shell=True)

   open_wav_button = tk.Button(frame_button, activebackground='DarkOliveGreen1', width=13, text="選ぶ", padx=5, command=select_wav)
   play_wav_button = tk.Button(frame_button, activebackground='DarkOliveGreen1', width=11, text="再生", padx=5, command=play_wav)
   fft_wav_button = tk.Button(frame_button, activebackground='DarkOliveGreen1', width=11, text="全波形とFFT", padx=5, command=fft_wav)
   pgm_wav_button = tk.Button(frame_button, activebackground='DarkOliveGreen1', width=15, text="プログラム編集", padx=5, command=pident_pgm_open)
   learn_wav_button = tk.Button(frame_button, activebackground='DarkOliveGreen1', width=15, text="学習", padx=5, command=pos_from_wav)

   open_wav_button.grid(row=0, column=0, sticky="ew", padx=5, pady=1)
   play_wav_button.grid(row=1, column=0, sticky="ew", padx=5, pady=1)
   fft_wav_button.grid(row=2, column=0, sticky="ew", padx=5, pady=1)
   pgm_wav_button.grid(row=3, column=0, sticky="ew", padx=5, pady=1)
   learn_wav_button.grid(row=4, column=0, sticky="ew", padx=5, pady=1)

   def mouse_over_pgm_wav(event):
      text.configure(bg="yellow")
      msg=" ----------------------------\n" + \
          "   [ pident_torch.py を編集します．]\n"
      text.insert("1.0", msg)
   pgm_wav_button.bind('<Enter>', mouse_over_pgm_wav)
   pgm_wav_button.bind('<Leave>', mouse_leave)

   def mouse_over_play_wav(event):
      text.configure(bg="yellow")
      msg="   [ wav ファイルを再生します．「選ぶ」ボタンでwavファイルが選択されている必要があります． ]\n"+ \
      "   実行ターミナル(Play Sound) 内のキー操作で再生を制御できます． \n"+ \
      "   停止：s \n" + \
      "   再開：c \n" + \
      "   終了：q \n" + \
      "   1-chunk 進む：j \n" + \
      "   1-chunk 戻る：k \n" + \
      "   10-chunk 進む：J \n" + \
      "   10-chunk 戻る：K \n" 
      text.insert("1.0", msg)
   play_wav_button.bind('<Enter>', mouse_over_play_wav)
   play_wav_button.bind('<Leave>', mouse_leave)
