# CC BY-SA Yasushi Honda 2023 10/11
# EV3プログラミング用のタブ
import time
import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os
import sys
import cv2
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from threading import Thread
import tabs.mskympl as Msky
import modules.mysocket as sk
#import tree4
import socket

mouse_over_color="DarkOliveGreen1"
common_bg="orange"
text_fg="#071407"
text_bg="#d6e0d6"


# ==== EV3 プログラミング tab をつくる =====
def const_tab(tab,local_entry,robot_id, robot_addr, calc_id, calc_addr):
   # Frameの定義と配置
   #tree_frame = ttk.LabelFrame(tab, text="File Tree", relief=tk.RAISED, padding=0)
   button_frame = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)
   #text_frame = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)
   #text_frame = ttk.LabelFrame(tab, text="Content Indicator", relief=tk.RAISED, padding=0)


   button_frame.pack(side="top",fill="x")
   #tree_frame.pack(side="left",fill="both",expand=True)
   #text_frame.pack(side="right",fill="both",expand=True)

   # Local entry
   Label=tk.Label(button_frame,text=" Local:", bg="orange",fg="black", height=1)
   Label.pack(side="left",padx=3)
   local_entry_ev3=tk.Entry(button_frame, width=17, bd=2, bg="IndianRed",fg="white")
   local_entry_ev3.insert(0,local_entry.get())
   local_entry_ev3.pack(side="left",padx=3)

   select_entry=tk.Entry(button_frame,  bd=2, bg="white",fg="black")
   select_entry.insert(0,"")
   select_entry.pack(side="right",expand=True,fill="x",padx=3)

   # テキストフレーム
   #text_editor = scrolledtext.ScrolledText(text_frame,bg=text_bg,fg=text_fg,padx=5,pady=5)
   #text_editor.pack(side="top",expand=True,fill="both")

   '''
   # How To を読み込む
   my_dir=os.path.dirname(__file__).replace("/tabs","")
   fp=open(my_dir+"/howto.txt","r")
   copy_right=""
   for line in fp.readlines():
      copy_right+=line
   fp.close()
   text_editor.insert(tk.END, copy_right)

   # ロボットツリーフレーム
   tree_wig=tree4.Tree(tree_frame, my_dir+'/EV3samples', text_bg,robot_id,robot_addr,calc_id,calc_addr,
           text_editor,select_entry)
   '''

   def mouse_leave(event):
      text_editor.configure(fg=text_fg,bg=text_bg)
      #text_editor.delete("1.0",tk.END)
   
   global panel_state
   panel_state="off"

   # コントロールパネルを起動する関数
   def pnl_onoff():
      global panel_state
      my_dir=os.path.dirname(__file__).replace("/tabs","")
      if panel_state=="off":
         try:
            cmd=my_dir+"/cpanel.py " + local_entry.get() + " " + robot_addr.get() + " " + calc_addr.get()
            print(cmd)
            #pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
            proc=subprocess.Popen(cmd.strip().split(' '))
            pnl_onoff_button.config(text="パネルOFF",bg="yellow")
            panel_state="on"
         except:
            text_editor.insert("0.0","\n panel failed \n")

      elif panel_state=="on":
         cmd="pkill cpanel"
         print(cmd)
         #pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
         proc=subprocess.Popen(cmd.strip().split(' '))
         panel_state="off"
         pnl_onoff_button.config(text="制御パネル",bg='white')

   # ボタンフレーム
   pnl_onoff_button = tk.Button(button_frame, activebackground='DarkOliveGreen1', bg='white', fg='black', text="制御パネル", padx=0, command=pnl_onoff,borderwidth=4)
   pnl_onoff_button.pack(side="left",padx=3,pady=4)


   #return text_editor
