# CC BY-SA Yasushi Honda 2023 10/11
# メインタブ
import time
import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import tabs.mskympl as Msky
import modules.mysocket as sk
import socket
from modules import pycode

mouse_over_color="DarkOliveGreen1"
common_bg="orange"
text_fg="#071407"
text_bg="#d6e0d6"
button_bg="#ffc666"
howto_bg="#fafaa0"


# ==== メイン tab をつくる =====
def const_tab(tab, text_frame, text_editor,
        robot_id, robot_addr, calc_id, calc_addr, selected_entry):
   # Frameの定義と配置
   button_frame = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)
   button_frame.pack(fill='x')

   # Local entry
   Label=tk.Label(button_frame,text=" Local: ", bg="orange",fg=text_fg, height=1, padx=5)
   #Label.pack(side="left",padx=0)

   def how_to():
      for widget in text_frame.winfo_children():
         widget.pack_forget()
      text_editor.pack(side="top",expand=True,fill="both")

      my_dir=os.path.dirname(__file__)
      fp=open(my_dir+"/howto_main.txt","r")
      howto_txt=""
      for line in fp.readlines():
         howto_txt+=line
      fp.close()
      text_editor.tag_config('finfo_line', background=howto_bg, foreground=text_fg)
      text_editor.delete("1.0",tk.END)
      text_editor.insert(tk.END, howto_txt,'finfo_line')

   def mouse_leave(event):
      text_editor.configure(fg=text_fg,bg=text_bg)
      #text_editor.delete("1.0",tk.END)
   
   # コントロールパネルを起動する関数
   def show_struct():
      for widget in text_frame.winfo_children():
         widget.pack_forget()
      filePath=selected_entry.get()
      indct=pycode.Structure(filePath,text_frame)
      #indct.markdown2Tkinter()
      indct.find(combobox.get())

   # ボタンフレーム
   comb_list = ['while|for|try:|if ', 'class', 'def', 'while', 'for', 'if', '##']
   v = tk.StringVar()
   combobox = ttk.Combobox(button_frame, font=(16), width=25, values=comb_list, #: コンボボックス
           style="office.TCombobox")
   combobox.current(0)
   #combobox.bind('<<ComboboxSelected>>', data_plot_execute)
   combobox.pack(side="left", padx=0,pady=0)

   exec_button = tk.Button(button_frame, text="検索表示", 
           activebackground='DarkOliveGreen1', bg=button_bg, fg=text_fg, 
           padx=5, command=show_struct,borderwidth=0)
   exec_button.pack(side="left", fill='y', padx=0,pady=0)

   howto_button = tk.Button(button_frame, text="How to", 
           activebackground='DarkOliveGreen1', bg=button_bg, fg=text_fg, 
           padx=5, command=how_to,borderwidth=0)
   howto_button.pack(side="right", fill='y', padx=0,pady=0)

