# CC BY-SA Yasushi Honda 2023 9/11

import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os

#common_bg="#307060"
common_bg="orange"
text_fg="#071407"
text_bg="#d6e0d6"
button_bg="#ffc666"


# ===== sympy_graph_tab をつくる =====
def const_tab(tab,text,selected,tree):
    button_frame = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)
    scale_frame = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)

    # フレームの配置
    button_frame.pack(side="left")
    scale_frame.pack(side="left",expand=True,fill="x")

    # Scale
    Label=tk.Label(scale_frame,text="Beta: ",bg=common_bg,fg="black")
    Label.pack(side="left")
 
    beta=tk.StringVar()

    def write_beta_value(event=None):
        my_dir=os.path.dirname(__file__).replace("/tabs","")
        fp=open(my_dir+"/Plot_Data/beta_value","w")
        fp.write(beta.get())
        fp.close()

    scale_beta = tk.Scale(scale_frame, 
                    bg=common_bg,
                    variable = beta,
                    command = write_beta_value,
                    orient=tk.HORIZONTAL,   # 配置の向き、水平(HORIZONTAL)、垂直(VERTICAL)
                    #length = 250,           # 全体の長さ
                    width = 15,             # 全体の太さ
                    sliderlength = 40,      # スライダー（つまみ）の幅
                    from_ = 0.0,            # 最小値（開始の値）
                    to = 10.0,              # 最大値（終了の値）
                    resolution=0.05,        # 変化の分解能(初期値:1)
                    #tickinterval=2         # 目盛りの分解能(初期値0で表示なし)
                    )
    scale_beta.pack(side="left",expand=True,fill='x')

    Label=tk.Label(scale_frame,text="Offset: ",bg=common_bg,fg="black")
    Label.pack(side="left")
    offset=tk.StringVar()

    def write_offset_value(event=None):
        my_dir=os.path.dirname(__file__).replace("/tabs","")
        fp=open(my_dir+"/Plot_Data/offset_value","w")
        fp.write(offset.get())
        fp.close()

    scale_offset = tk.Scale(scale_frame, 
                    variable = offset,
                    command = write_offset_value,
                    orient=tk.HORIZONTAL,   # 配置の向き、水平(HORIZONTAL)、垂直(VERTICAL)
                    #length = 250,           # 全体の長さ
                    width = 15,             # 全体の太さ
                    sliderlength = 40,      # スライダー（つまみ）の幅
                    from_ = -10.0,          # 最小値（開始の値）
                    to = 10.0,              # 最大値（終了の値）
                    resolution=0.05,        # 変化の分解能(初期値:1)
                    #tickinterval=2         # 目盛りの分解能(初期値0で表示なし)
                    )
    scale_offset.pack(side="left",expand=True,fill='x')

    def plot_execute():
        global plot_file
        my_dir=os.path.dirname(__file__).replace("/tabs","")
        plot_file=my_dir+"/Plot_Data/fplot_sympy.py"
        cmd="mate-terminal --title='Graph' --command \"bash -c 'python3 " + plot_file + "';bash\" --geometry 60x20+300+200 &"
        pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
        res = pro.stderr + '\n'
        text.insert("1.0", res)
     
        selected.delete(0,tk.END)
        selected.insert(0,plot_file)
        tree.show_py(plot_file)
        dir_name=plot_file[:plot_file.rfind("/")]
        print(dir_name)
        tree.path=dir_name
        tree.reload_mydir()

    def plot_sigmoid():
        my_dir=os.path.dirname(__file__).replace("/tabs","")
        plot_prog=my_dir+"/Plot_Data/plot_sigmoid.py"
        cmd="mate-terminal --title='Sigmoid' --command \"bash -c 'python3 " + plot_prog + "';bash\" --geometry 20x10+300+500 &"
        pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
        res = pro.stderr + '\n'
        text.insert("1.0", res)

   # Button Frame
    button_plot = tk.Button(button_frame, text="プロット", 
           activebackground='DarkOliveGreen1', activeforeground='red', 
           bg=button_bg, fg=text_fg,
           borderwidth=2, padx=2, command=plot_execute)
    button_sigmoid = tk.Button(button_frame, text="sigmoid", 
           activebackground='DarkOliveGreen1', activeforeground='red', 
           bg=button_bg, fg=text_fg,
           borderwidth=2, padx=2, command=plot_sigmoid)

    button_plot.grid(row=0, column=1, sticky="ew", padx=2, pady=1)
    button_sigmoid.grid(row=0, column=2, sticky="ew", padx=2, pady=1)

