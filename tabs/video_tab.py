#!/usr/bin/python3
# rcc2.py にimportするコード
# CC BY-SA Yasushi Honda 2023 7/29

import tkinter as tk
import tkinter.font as tkFont
#from ttkthemes import ThemedTk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import time
import os

def mouse_leave(event):
   text_editor.configure(fg="black",bg="LightSkyBlue")
   #text_editor.insert("1.0"," ------------------- \n")
   sound_text.configure(fg="black",bg="LightSkyBlue")
   #sound_text.insert("1.0"," ------------------- \n")
   pident_text.configure(fg="black",bg="LightSkyBlue")


def open_wiki():
    wiki_addr="https://gitlab.com/honda_lab/robot-intelligence/-/wikis/home"
    #cmd="firefox https://gitlab.com/honda_lab/robot-intelligence/-/wikis/home &"
    cmd="mate-terminal --title='Wiki Honda Lab' --hide-menubar --command \"bash -c 'firefox https://gitlab.com/honda_lab/robot-intelligence/-/wikis/home';bash\" --geometry 77x10+300+660 &"
    pro = subprocess.run(cmd,shell=True, capture_output=True,text=True)
    if len(pro.stderr)>0:
       text_editor.configure(bg="red",fg="white")   
       res = pro.stderr + "\n \n"
       text_editor.insert("1.0", res)
    else:
       text_editor.configure(fg="black", bg="LightSkyBlue1")

def open_res_terminal():
    global res_tty
    cmd="mate-terminal --title='Result' --hide-menubar --command \"bash -c 'tty>" + os.path.dirname(__file__) + "/mytty';bash\" --geometry 78x10+0+470 &"
    pro = subprocess.run(cmd,shell=True, capture_output=True,text=True)
    if len(pro.stderr)>0:
       text_editor.configure(bg="red",fg="white")   
       res = pro.stderr + "\n \n"
       text_editor.insert("1.0", res)
    else:
       text_editor.configure(fg="black", bg="LightSkyBlue1")
       
    # ttyの値を知るためにcatするが，terminalを開いた直後だと内容が最新になってないので，0.5秒sleepする．
    cmd="sleep 0.5 ; cat " + os.path.dirname(__file__) + "/mytty &"
    pro = subprocess.run(cmd,shell=True, capture_output=True,text=True)
    res_tty=pro.stdout.strip("\n")
    text_editor.insert("1.0", "   プログラムの実行結果はResult(" + res_tty+")に出力されます．\n")
    #print(program[0])

def new_file():
    global filepath
    global program
    program=entry_prog.get()
    dir=askdirectory()
    filepath=dir+'/' + program

    cmd='gedit ' + filepath +' &'
    pro = subprocess.run(cmd,shell=True)
    entry_prog.delete(0,tk.END)
    entry_prog.insert(0, filepath)

def file_save():
    typ = [("Text Files", "*.txt")]
    filepath = asksaveasfilename(defaultextension="*",filetypes=typ)
    if not filepath:
        return
    with open(filepath, "w") as save_file:
        text = text_editor.get("1.0", tk.END)
        save_file.write(text)
    root.title(f"Text Editor - {filepath}")

def data_open():
    global data_file
    data_file=os.path.dirname(__file__)+"/samples/data.csv"
    entry_data.delete(0,tk.END)
    entry_data.insert(0, data_file)
    cmd='gvim -geometry 120x30+500+300 ' + data_file + ' & '
    pro = subprocess.run(cmd,shell=True)

def sympy_open():
    global plot_file
    plot_file=os.path.dirname(__file__)+"/samples/fplot_sympy.py"
    with open(plot_file, "r") as open_file:
        sympy_code=open_file.read()
        sympy_text.insert("1.0", sympy_code)
    cmd='gvim -geometry 120x30+500+300 ' + plot_file + ' & '
    pro = subprocess.run(cmd,shell=True)

def select_file():
    global filepath,dirname
    typ = [("Wav Files", "*.wav")]
    filepath = askopenfilename(defaultextension="*.wav",filetypes=typ)
    dirname = os.path.dirname(filepath)    

    entry_sound.delete(0,tk.END)
    entry_sound.insert(0, filepath)
    sound_text.insert("1.0", "\n" + filepath + "を解析します．\n")

def gedit():
    global filepath,program,dirname
    typ = [("Text Files", "*.py")]
    filepath = askopenfilename(defaultextension="py",filetypes=typ)
    dirname = os.path.dirname(filepath)    

    program=re.findall('[^/]+.py',filepath, flags=re.IGNORECASE)[0]
    cmd='gedit ' + filepath + ' &' 
    pro = subprocess.run(cmd,shell=True)

    #text_editor.configure(fg="black", bg="LightSkyBlue1")
    entry_prog.delete(0,tk.END)
    entry_prog.insert(0, filepath)
    text_editor.insert("1.0", "\n" + program + "を編集します．\n")
    #print(program[0])

def open_gvim():
    global filepath,program,git_filepath
    typ = [("Python", "*.py"), ("C", "*.c"),("Text", "*.txt"),("Shell","*.sh"),("Data","*.dat"),("All","*")]
    filepath = askopenfilename(defaultextension="*.py",filetypes=typ)
    git_filepath = filepath
    try:
       program=re.findall('[^/]+.py',filepath, flags=re.IGNORECASE)[0]
    except:
       program=''
    cmd='gvim -geometry 80x25+300+300 ' + filepath + ' &'
    pro = subprocess.run(cmd,shell=True)
    entry_prog.delete(0,tk.END)
    entry_prog.insert(0, filepath)
    git_entry.delete(0,tk.END)
    git_entry.insert(0, git_filepath)
    text_editor.insert("1.0", filepath + "を開きます．\n\n")
    git_text.configure(bg="LightSkyBlue1",fg="black")   
    git_text.insert("1.0", git_filepath + "をgvimで開きます．\n\n")

def execute():
    cmd="mate-terminal --title='Execute' --command \"bash -c 'python3 " + entry_prog.get() + "';bash\" --geometry 80x10+200+600 &"
    #cmd="python3 " + entry_prog.get() + " > " + res_tty + " &"
    comment=entry_prog.get()+"をローカル実行します\n"
    text_editor.insert("1.0", comment)
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    res = pro.stderr + '\n'
    text_editor.insert("1.0", res)

def data_initialize():
    cmd="echo \"0.0, 0,0, 0.0\" >  " + os.path.dirname(__file__) + "/samples/data.csv"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)

def data_plot_execute():
    plot_data_code=os.path.dirname(__file__)+"/samples/plot_data.py"

    cmd="mate-terminal --title='Graph of Data from remote' --command \"bash -c 'python3 " + plot_data_code + "';bash\" --geometry 60x20+300+200 &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    res = pro.stderr + '\n'
    text_editor.insert("1.0", res)

def plot_execute():
    global plot_file
    plot_file=os.path.dirname(__file__)+"/samples/fplot_sympy.py"
    cmd="mate-terminal --title='Graph' --command \"bash -c 'python3 " + plot_file + "';bash\" --geometry 60x20+300+200 &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    res = pro.stderr + '\n'
    text_editor.insert("1.0", res)

def plot_sigmoid():
    plot_prog=os.path.dirname(__file__)+"/samples/plot_sigmoid.py"
    cmd="mate-terminal --title='Sigmoid' --command \"bash -c 'python3 " + plot_prog + "';bash\" --geometry 20x10+300+200 &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    res = pro.stderr + '\n'
    text_editor.insert("1.0", res)

def remote_execute_2file():
    filepath=entry_prog.get()
    try:
       program=re.findall('[^/]+.py',filepath, flags=re.IGNORECASE)[0]
        # filepath の末尾 .py から遡って最初の/までの文字列を抜き出す．
    except:
       text_editor.configure(bg="red",fg="white")   
       msg="   [ プログラムの指定にエラーがあります ]\n \n"
       text_editor.insert("1.0", msg)
       return

    cmd="rcp " + filepath + " robot@" + addr.get() + ": &"
    comment=program + "をリモートにコピーします．\n"
    text_editor.insert("1.0", comment)
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    if len(pro.stderr)>0:
       text_editor.configure(bg="red",fg="white")   
       res = pro.stderr + "\n \n"
       text_editor.insert("1.0", res)
    else:
       text_editor.configure(fg="black", bg="LightSkyBlue1")

       cmd="ssh robot@" + addr.get() + " micropython " + program + " > " + data_file + " &"
       comment=program + "をリモート実行します．\n"
       text_editor.insert("1.0", comment)
       pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
       if len(pro.stderr)>0:
          text_editor.configure(bg="red",fg="white")   
          res = pro.stderr + "\n \n"
          text_editor.insert("1.0", res)
       else:
          text_editor.configure(fg="black", bg="LightSkyBlue1")

def remote_execute():
    filepath=entry_prog.get()
    try:
       program=re.findall('[^/]+.py',filepath, flags=re.IGNORECASE)[0]
        # filepath の末尾 .py から遡って最初の/までの文字列を抜き出す．
    except:
       text_editor.configure(bg="red",fg="white")   
       msg="   [ プログラムの指定にエラーがあります ]\n \n"
       text_editor.insert("1.0", msg)
       return

    cmd="rcp " + filepath + " robot@" + addr.get() + ": &"
    comment=program + "をリモートにコピーします．\n"
    text_editor.insert("1.0", comment)
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    if len(pro.stderr)>0:
       text_editor.configure(bg="red",fg="white")   
       res = pro.stderr + "\n \n"
       text_editor.insert("1.0", res)
    else:
       text_editor.configure(fg="black", bg="LightSkyBlue1")

       cmd="ssh robot@" + addr.get() + " micropython " + program + " > " + res_tty + " &"
       comment=program + "をリモート実行します．\n"
       text_editor.insert("1.0", comment)
       pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
       if len(pro.stderr)>0:
          text_editor.configure(bg="red",fg="white")   
          res = pro.stderr + "\n \n"
          text_editor.insert("1.0", res)
       else:
          text_editor.configure(fg="black", bg="LightSkyBlue1")

def show_addr():
    global addr
    print(addr.get())
    text_editor.insert(tk.END, "\n"+addr.get()+"\n")

def ping():
    global addr
    #cmd="mate-terminal --command \"bash -c 'ping -c 7 " + addr.get() + "';bash\" --geometry +300+200 &"
    cmd="ping -c 5 " + addr.get() + " > " + res_tty + " &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    text_editor.insert("1.0", addr.get() + "との接続を確認します．\n\n")

def open_ssh():
    global addr
    cmd="mate-terminal --command \"bash -c 'ssh robot@" + addr.get() + "';bash\" --geometry +300+300 &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    text_editor.insert("1.0", " ------------------------- \n")
    text_editor.insert("1.0", "   [ " + addr.get() + "にsshでログインします ]\n")
    text_editor.insert("1.0", "   [ EV3DEVのデフォルトパスワードは \"maker\"です ]\n")

def ssh_key():
    global addr
    #cmd="mate-terminal --command \"bash -c 'ssh-keygen -t rsa -f ~/.ssh/" + addr.get() + "_rsa';bash\" --geometry +300+200 &"
    rsa_file=os.path.expanduser('~/.ssh/')+addr.get()+"_rsa"
    if os.path.isfile(rsa_file):
       msg="すでにssh-keyが存在します"
       text_editor.insert("1.0", "\n" + msg + "\n")

       cmd="mate-terminal --command \"bash -c 'ssh-copy-id -i ~/.ssh/" + addr.get() + "_rsa.pub robot@" + addr.get() + "';bash\" --geometry +300+200 &"
       pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
       text_editor.insert("1.0", "\n" + cmd + "\n")
    else:

       cmd="ssh-keygen -f ~/.ssh/" + addr.get() + "_rsa -t rsa -N '' "
       pro = subprocess.run(cmd,shell=True,capture_output=True,text=True)
       text_editor.insert("1.0", "\n" + pro.stdout + "\n")
       time.sleep(1)

       cmd="mate-terminal --command \"bash -c 'ssh-copy-id -i ~/.ssh/" + addr.get() + "_rsa.pub robot@" + addr.get() + "';bash\" --geometry +300+200 &"
       pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
       text_editor.insert("1.0", "\n" + cmd + "\n")

def reboot():
    cmd="pkill rcc2.py;" + os.path.dirname(__file__) + "/rcc2.py &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    if len(pro.stderr)>0:
       text_editor.configure(bg="red",fg="white")   
       res = pro.stderr + "\n"
       text_editor.insert("1.0", res)
    else:
       text_editor.configure(fg="black", bg="LightSkyBlue1")

def git_add():
    cmd="git add " + git_filepath + " > " + res_tty
    git_text.insert("1.0", cmd+"\n")
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    if len(pro.stderr)>0:
       git_text.configure(bg="red",fg="white")   
       res = pro.stderr + "\n"
       git_text.insert("1.0", res)
    else:
       git_text.configure(fg="black", bg="LightSkyBlue1")
       comment="更新されたファイルをgit addします\n"
       res = git_filepath + "をgit add しました\n"
       git_text.insert("1.0", res)
       git_text.insert("1.0", comment)

def git_commit():
    cmd="mate-terminal --command \"bash -c 'git commit -a';bash\" --geometry +300+200 &"
    #cmd="git commit -m '" + git_comment_entry.get() + "' >  " + res_tty 
    #cmd="git commit -a > " + res_tty
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    if len(pro.stderr)>0:
       git_text.configure(bg="red",fg="white")   
       res = pro.stderr + "\n"
       git_text.insert("1.0", res)
    else:
       git_text.configure(fg="black", bg="LightSkyBlue1")
       res = git_filepath + "をgit commitしました\n"
       git_text.insert("1.0", res)

def git_push():
    cmd="git push" 
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    if len(pro.stderr)>0:
       git_text.configure(bg="red",fg="white")   
       res = pro.stderr + "\n"
       git_text.insert("1.0", res)
    else:
       git_text.configure(fg="black", bg="LightSkyBlue1")
       res = git_filepath + "をgit pushしました\n"
       git_text.insert("1.0", res)
   
def show_copy_right():
   global copy_right
   text_editor.insert("1.0", copy_right)
   filepath=os.path.dirname(__file__)+"/pics/hnd.txt"
   fp=open(filepath,"r")
   msg=fp.read()
   text_editor.insert("1.0", msg)
   fp.close()


# rootをつくる
default_font_size=8
root = tk.Tk()
#root=ThemedTk(theme="default")
root.title('Robot Command Center 2 (RCC2)')
root.geometry("+0+0")
root.option_add( "*font", "lucida "+str(default_font_size) ) # 全体のdefault font

style = ttk.Style()
#style.configure('TLabel', foreground='red')
#style.configure('TEntry', foreground='red')
#style.configure('TMenubutton', foreground='red')
style.configure('TButton', foreground='black')
style.configure('TNotebook.Tab', expand=[(10,10),(10,10)],tabmargins=[20,20])

# フォントサイズを変更
font = ('Yu Gothic UI', default_font_size)
style = ttk.Style()
style.configure('TNotebook.Tab', font=font)


#root.rowconfigure(0, minsize=500, weight=1)
#root.columnconfigure(1, minsize=500, weight=1)

style.map(
   "rcc2.TNotebook.Tab",
   foreground=[('active','black'),('disabled','gray'),('selected','black')],
   background=[('active','lightgreen'),('disabled','black'),('selected','orange')]
)

# タブの作成
class Video_tab():
   def __init__(self,notebook):
      video_tab = tk.Frame(notebook, bg='DarkOliveGreen3')
      img_file=os.path.dirname(__file__)+"/pics/Robot48.png"
      if os.path.isfile(img_file):
         ev3_img=tk.PhotoImage(file=img_file)
      else:
         ev3_img=tk.PhotoImage(file=os.path.dirname(__file__)+"/pics/hnd.png")
      notebook.add(video_tab, text=' video ', image=ev3_img, compound=tk.LEFT)

#タブを構成

# ==== spike プログラミング tab をつくる =====
# Frameの定義と配置
spike_button_frame = tk.Frame(spike_execute_tab, relief=tk.RAISED, bd=2, bg="DarkOliveGreen3")
spike_input_frame = tk.Frame(spike_execute_tab, relief=tk.RAISED, bd=2, bg="pink")
spike_text_frame = tk.Frame(spike_execute_tab, relief=tk.RAISED, bd=2, bg="white")

spike_input_frame.grid(row=0, column=1,sticky="ns")
spike_text_frame.grid(row=1, column=1,sticky="ns")
spike_button_frame.grid(rowspan=2, row=0, column=0,sticky="ns")

#text_editor = tk.Text(frame_text,bg="LightSkyBlue1",fg="black")
spike_text_editor = scrolledtext.ScrolledText(spike_text_frame,bg="LightSkyBlue1",fg="black",width=102)
spike_text_editor.grid(row=0, column=0, sticky="ew")

copy_right="  **********************************************．\n"+ \
   "   IP Address: にリモートホストのアドレスを入力してください．\n" + \
   "   これはspike(のラズパイ)でプログラムを開発するためのGUIです．\n"+ \
   "   Gitレポジトリ: https://gitlab.com/honda_lab/etrobocon.git  \n"+ \
   "   CC BY-SA Yasushi Honda 2023 7/1．\n" + \
   "   icons: https://icons8.com/icons/pulsar-color  \n"
spike_text_editor.insert(tk.END, copy_right)

# Program Entry 
spike_prog=StringVar()
spike_prog='プログラム名（ディレクトリは自動的に補完されます）'
Label=tk.Label(spike_input_frame,text="Program: ",bg="white",fg="black")
spike_prog_entry=tk.Entry(spike_input_frame, textvariable=spike_prog, width=88, bd=10, bg="IndianRed",fg="white")
spike_prog_entry.insert(0,spike_prog)
Label.grid(row=0, column=0, sticky="ew")
spike_prog_entry.grid(row=0, column=1, sticky=tk.EW)
button_new = tk.Button(spike_input_frame, activebackground='DarkOliveGreen1', text='New', command=new_file, bg="RoyalBlue",fg="white")
button_new.grid(row=0, column=2, sticky="ew")

def spike_ping():
    global spike_addr
    cmd="ping -c 5 " + spike_addr.get() + " > " + res_tty + " &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    spike_text_editor.insert("1.0", addr.get() + "との接続を確認します．\n\n")

# IP Address Entry
spike_addr=StringVar()
Label=tk.Label(spike_input_frame,text="IP Address: ",bg="white",fg="black")
spike_addr_entry=tk.Entry(spike_input_frame, textvariable=spike_addr, width=88, bd=10, bg="IndianRed",fg="white")
spike_addr_entry.insert(0,'172.16.7.xxx')
Label.grid(row=1, column=0, sticky="ew")
spike_addr_entry.grid(row=1, column=1, sticky="ew")
button_ok = tk.Button(spike_input_frame, activebackground='DarkOliveGreen1', text='ping', command=spike_ping)
button_ok.grid(row=1, column=2, sticky="ew")

def spike_ssh():
    global spike_addr
    cmd="mate-terminal --command \"bash -c 'ssh pi@" + spike_addr.get() + "';bash\" --geometry +300+300 &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    spike_text_editor.insert("1.0", " ------------------------- \n")
    spike_text_editor.insert("1.0", "   [ " + spike_addr.get() + "にsshでログインします ]\n")
    spike_text_editor.insert("1.0", "   [ Spikeのデフォルトパスワードは \" xxxx \"です ]\n")

def spike_key():
    global spike_addr
    rsa_file=os.path.expanduser('~/.ssh/')+spike_addr.get()+"_rsa"
    if os.path.isfile(rsa_file):
       msg="すでにssh-keyが存在します"
       spike_text_editor.insert("1.0", "\n" + msg + "\n")

       cmd="mate-terminal --command \"bash -c 'ssh-copy-id -i ~/.ssh/" + spike_addr.get() + "_rsa.pub pi@" + spike_addr.get() + "';bash\" --geometry +300+200 &"
       pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
       spike_text_editor.insert("1.0", "\n" + cmd + "\n")
    else:

       cmd="ssh-keygen -f ~/.ssh/" + spike_addr.get() + "_rsa -t rsa -N '' "
       pro = subprocess.run(cmd,shell=True,capture_output=True,text=True)
       spike_text_editor.insert("1.0", "\n" + pro.stdout + "\n")
       time.sleep(1)

       cmd="mate-terminal --command \"bash -c 'ssh-copy-id -i ~/.ssh/" + spike_addr.get() + "_rsa.pub pi@" + spike_addr.get() + "';bash\" --geometry +300+200 &"
       pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
       spike_text_editor.insert("1.0", "\n" + cmd + "\n")

def spike_execute():
    filepath=spike_prog_entry.get()
    filepath=filepath.strip("/run/user/1000/gvfs/sftp:host="+spike_addr_entry.get())
    filepath=filepath.strip(",user=pi")
    try:
       program=re.findall('[^/]+.py',filepath, flags=re.IGNORECASE)[0]
        # filepath の末尾 .py から遡って最初の/までの文字列を抜き出す．
    except:
       spike_text_editor.configure(bg="red",fg="white")   
       msg="   [ プログラムの指定にエラーがあります ]\n \n"
       spike_text_editor.insert("1.0", msg)
       return

    #cmd="rcp " + filepath + " pi@" + spike_addr.get() + ": &"
    #pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    comment=filepath + "をリモート実行します．\n"
    spike_text_editor.insert("1.0", comment)
    cmd="ssh pi@" + spike_addr.get() + " python3 " + filepath + " > " + res_tty + " &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    if len(pro.stderr)>0:
       spike_text_editor.configure(bg="red",fg="white")   
       res = pro.stderr + "\n \n"
       spike_text_editor.insert("1.0", res)
    else:
       spike_text_editor.configure(fg="black", bg="LightSkyBlue1")

    '''
    if len(pro.stderr)>0:
       spike_text_editor.configure(bg="red",fg="white")   
       res = pro.stderr + "\n \n"
       spike_text_editor.insert("1.0", res)
    else:
       spike_text_editor.configure(fg="black", bg="LightSkyBlue1")

       cmd="ssh pi@" + spike_addr.get() + " python3 " + program + " > " + res_tty + " &"
       comment=program + "をspikeで実行します．\n"
       spike_text_editor.insert("1.0", comment)
       pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
       if len(pro.stderr)>0:
          spike_text_editor.configure(bg="red",fg="white")   
          res = pro.stderr + "\n \n"
          spike_text_editor.insert("1.0", res)
       else:
          spike_text_editor.configure(fg="black", bg="LightSkyBlue1")
    '''

def open_spike():
    try:
       cmd="gio mount sftp://pi@" + spike_addr.get() +'/'
       pro = subprocess.run(cmd,shell=True)
    except:
       pass
    typ = [("Python", "*.py"), ("C", "*.c"),("Text", "*.txt"),("Shell","*.sh"),("Data","*.dat"),("All","*")]
    remote_dir="/run/user/1000/gvfs/sftp:host=" + spike_addr.get() + ",user=pi/home/pi"
    filepath = askopenfilename(initialdir=remote_dir,defaultextension="*.py",filetypes=typ)
    try:
       program=re.findall('[^/]+.py',filepath, flags=re.IGNORECASE)[0]
    except:
       program=''
    cmd='gedit ' + filepath + ' &'
    pro = subprocess.run(cmd,shell=True)
    spike_prog_entry.delete(0,tk.END)
    spike_prog_entry.insert(0, filepath)
    spike_text_editor.insert("1.0", filepath + "を開きます．\n\n")
    spike_text_editor.configure(bg="LightSkyBlue1",fg="black")   
    spike_text_editor.insert("1.0", filepath + "を開きます．\n\n")


# button Frame
spike_save_button = tk.Button(spike_button_frame, text="Save As", padx=5, command=file_save)
spike_gvim_button = tk.Button(spike_button_frame, activebackground='DarkOliveGreen1', bg="white", fg="black", borderwidth=7, text="vimで編集", padx=5, command=open_gvim,width=13)
spike_rexec_button = tk.Button(spike_button_frame, activebackground='DarkOliveGreen1', bg="RoyalBlue3", fg="white", borderwidth=7, text="リモート実行", padx=5, height=4, command=spike_execute)
spike_redit_button = tk.Button(spike_button_frame, activebackground='DarkOliveGreen1', bg="RoyalBlue3", fg="white", borderwidth=7, text="リモートファイル編集", padx=5, command=open_spike,width=13)

spike_exec_button = tk.Button(spike_button_frame, activebackground='DarkOliveGreen1', text="ローカル実行", padx=5, command=execute)
spike_ssh_button = tk.Button(spike_button_frame, activebackground='DarkOliveGreen1', text="ssh", padx=5, command=spike_ssh)
spike_key_button = tk.Button(spike_button_frame, activebackground='DarkOliveGreen1', text="ssh-key", padx=5, command=spike_key)
spike_open_res_button = tk.Button(spike_button_frame, activebackground='DarkOliveGreen1', text="open res", padx=5, command=open_res_terminal)
spike_wiki_button = tk.Button(spike_button_frame, activebackground='DarkOliveGreen1', text="wiki", padx=5, command=open_wiki)
spike_reboot_button = tk.Button(spike_button_frame, activebackground='DarkOliveGreen1', text="再起動", padx=5, command=reboot)

#spike_suggest_menu=tk.Menu(spike_button_frame)

#button_open.grid(row=0, column=0, sticky='ew', padx=5, pady=5)
#button_save.grid(row=1, column=0, sticky="ew", padx=5)
spike_redit_button.grid(row=0, column=0, sticky="ew", padx=5, pady=1)
spike_rexec_button.grid(row=1, column=0, sticky="ew", padx=5, pady=5)
spike_gvim_button.grid(row=2, column=0, sticky="ew", padx=5, pady=1)
spike_exec_button.grid(row=3, column=0, sticky="ew", padx=5, pady=1)
spike_ssh_button.grid(row=4, column=0, sticky="ew", padx=5, pady=1)
spike_key_button.grid(row=5, column=0, sticky="ew", padx=5, pady=1)
spike_open_res_button.grid(row=6, column=0, sticky="ew", padx=5, pady=1)
spike_wiki_button.grid(row=7, column=0, sticky="ew", padx=5, pady=1)
#spike_suggest_menu.grid(row=7, column=0, sticky="ew", padx=5, pady=1)
spike_reboot_button.grid(row=10, column=0, sticky="ew", padx=5, pady=1)

def mouse_over_button_reboot(event):
   msg="   [ このロボットコマンドセンターRCC2自体を再起動 ]\n"
   text_editor.configure(bg="yellow")
   text_editor.insert("1.0", msg)
spike_reboot_button.bind('<Enter>', mouse_over_button_reboot)
spike_reboot_button.bind('<Leave>',mouse_leave)

def mouse_over_button_key(event):
   text_editor.configure(bg="yellow")
   msg="   [ ssh-key をつくります．１度実行したら以後は必要ありません ]\n"+ \
   "   ssh-key-gen -t rsa ~/.ssh/" + addr.get() + "_rsa \n"+ \
   "   ssh-copy-id -i ~/.ssh/" + addr.get() + "_rsa.pub robot@" + addr.get() + "\n"
   text_editor.insert("1.0", msg)
spike_key_button.bind('<Enter>', mouse_over_button_key)
spike_key_button.bind('<Leave>',mouse_leave)

def mouse_over_button_ssh(event):
   msg="   [ IP addressで指定されたロボットにsshでログインします ]\n"
   text_editor.configure(bg="yellow")
   text_editor.insert("1.0", msg)
spike_ssh_button.bind('<Enter>', mouse_over_button_ssh)
spike_ssh_button.bind('<Leave>',mouse_leave)

def mouse_over_button_open_res(event):
   msg="   [ 結果出力terminalを起動し直します ]\n"
   text_editor.configure(bg="yellow")
   text_editor.insert("1.0", msg)
spike_open_res_button.bind('<Enter>', mouse_over_button_open_res)
spike_open_res_button.bind('<Leave>',mouse_leave)

def mouse_over_button_wiki(event):
   global wiki_description
   wiki_description="   Honda Lab のGitLab wikiを開きます．\n" + \
   "   行動のためのロボット知能などについて知ることができます．\n" + \
   "   またこのRCC2については https://gitlab.com/honda_lab/etrobocon も参考にしてください．\n"
   text_editor.configure(bg='yellow',fg='black')
   text_editor.insert("1.0", wiki_description)
spike_wiki_button.bind('<Enter>', mouse_over_button_wiki)
spike_wiki_button.bind('<Leave>',mouse_leave)
# ==== spikeプログラミング_tab の最後尾 =====

# ===== pos_ident_tab をつくる =====
pident_frame_button = tk.Frame(pos_ident_tab, relief=tk.RAISED, bd=2, bg='NavajoWhite2')
pident_frame_input = tk.Frame(pos_ident_tab, relief=tk.RAISED, bd=2, bg="pink", width=500)
pident_frame_radio = tk.Frame(pos_ident_tab, relief=tk.RAISED, bd=2, bg="white", width=500)
pident_frame_text = tk.Frame(pos_ident_tab, relief=tk.RAISED, bd=2, bg="white", width=500)

# フレームの配置
pident_frame_input.grid(row=0, column=1,sticky="ns")
pident_frame_radio.grid(row=1, column=1,sticky="ns")
pident_frame_text.grid(row=2, column=1,sticky="ns")
pident_frame_button.grid(rowspan=3,  row=0, column=0,sticky="ns")

# text Frame
pident_text = scrolledtext.ScrolledText(pident_frame_text,bg="LightSkyBlue1",fg="black",width=102,height=12)
pident_text.grid(row=0, column=0, sticky="ew")

msg=" -------------------- \n" + \
    " これは，celloの運指候補のタブです． \n" + \
" 音階から運指候補を決める試みです．\n" + \
" 指位置は，弦(A=1,D=2,G=3,G=4)，ポジション(1,L2,H2,L3,H3(3),4,5,6,7,8)，指番号(1,2,3,4)の\n" + \
" 3つの組み合わせで表します．\n"
pident_text.insert("1.0",msg)


# Entry Frame
global sound_file
Label=tk.Label(pident_frame_input,text=" WAV File: ",bg="white",fg="black")
entry_wav=tk.Entry(pident_frame_input, textvariable='', width=55, bd=7, bg="IndianRed",fg="white")
entry_wav.insert(0,'')
Label.grid(row=0, column=0, sticky="ew")
entry_wav.grid(row=0, column=1, sticky=tk.EW)

sound_file=os.path.dirname(__file__)+"/sound/tmp.wav"
Label=tk.Label(pident_frame_input,text="Rec WAV File: ",bg="white",fg="black")
entry_rec=tk.Entry(pident_frame_input, textvariable='', width=55, bd=10, bg="IndianRed",fg="white")
entry_rec.insert(0,sound_file)
Label.grid(row=1, column=0, sticky="ew")
entry_rec.grid(row=1, column=1, sticky=tk.EW)

def scale_click():
   # ラジオボタンの値を取得
   value = scale_value.get()
   pident_text.insert("1.0","運指は "+ str(value) +" です．\n")
   play_file=os.path.dirname(__file__)+"/sound/cello_wav/"+str(value)+".wav"
   pident_text.insert("1.0",play_file+"\n")
   entry_wav.delete(0,tk.END)
   entry_wav.insert(0,play_file)
   play_wav()
   

# 音階を選ぶラジオボタン
scale_value = tk.IntVar(value = 0)     # 初期値を設定する場合
Label=tk.Label(pident_frame_radio,text="A線:",bg="white",fg="black",width=7)
Label.grid(row=2, column=0, sticky=tk.E)
A3_radiob = tk.Radiobutton(pident_frame_radio,text="A3",command = scale_click,variable=scale_value,
                           value=100, width=2, height=2,bg="orange")
A3_radiob.grid(row=2, column=1, sticky=tk.EW)
A3sh_radiob = tk.Radiobutton(pident_frame_radio,text="A3#",command = scale_click,variable=scale_value,
                           value=101, width=2, height=2,bg="LightBlue1",fg="black")
A3sh_radiob.grid(row=2, column=2, sticky=tk.EW)
B3_radiob = tk.Radiobutton(pident_frame_radio,text="B3",command = scale_click,variable=scale_value,
                           value=111, width=2, height=2,bg="DarkOliveGreen1")
B3_radiob.grid(row=2, column=3, sticky=tk.EW)
C4_radiob = tk.Radiobutton(pident_frame_radio,text="C4",command = scale_click,variable=scale_value,
                           value=112, width=2, height=2,bg="DarkOliveGreen1")
C4_radiob.grid(row=2, column=4, sticky=tk.EW)
C4sh_radiob = tk.Radiobutton(pident_frame_radio,text="C4#",command = scale_click,variable=scale_value,
                           value=113, width=2, height=2,bg="DarkOliveGreen1")
C4sh_radiob.grid(row=2, column=5, sticky=tk.EW)
D4_radiob = tk.Radiobutton(pident_frame_radio,text="D4",command = scale_click,variable=scale_value,
                           value=114, width=2, height=2,bg="DarkOliveGreen1")
D4_radiob.grid(row=2, column=6, sticky=tk.EW)
D4sh_radiob = tk.Radiobutton(pident_frame_radio,text="D4#",command = scale_click,variable=scale_value,
                           value=131, width=2, height=2,bg="cornsilk2")
D4sh_radiob.grid(row=2, column=7, sticky=tk.EW)
E4_radiob = tk.Radiobutton(pident_frame_radio,text="E4",command = scale_click,variable=scale_value,
                           value=141, width=2, height=2,bg="lightsalmon")
E4_radiob.grid(row=2, column=8, sticky=tk.EW)
F4_radiob = tk.Radiobutton(pident_frame_radio,text="F4",command = scale_click,variable=scale_value,
                           value=142, width=2, height=2,bg="lightsalmon")
F4_radiob.grid(row=2, column=9, sticky=tk.EW)
F4sh_radiob = tk.Radiobutton(pident_frame_radio,text="F4#",command = scale_click,variable=scale_value,
                           value=143, width=2, height=2,bg="lightsalmon")
F4sh_radiob.grid(row=2, column=10, sticky=tk.EW)
G4_radiob = tk.Radiobutton(pident_frame_radio,text="G4",command = scale_click,variable=scale_value,
                           value=144, width=2, height=2,bg="lightsalmon")
G4_radiob.grid(row=2, column=11, sticky=tk.EW)

G4sh_radiob = tk.Radiobutton(pident_frame_radio,text="G4#",command = scale_click,variable=scale_value,
                           value=161, width=4, height=2,bg="coral")
G4sh_radiob.grid(row=3, column=2, sticky=tk.EW)
A4_radiob = tk.Radiobutton(pident_frame_radio,text="A4",command = scale_click,variable=scale_value,
                           value=162, width=2, height=2,bg="coral")
A4_radiob.grid(row=3, column=3, sticky=tk.EW)
A4sh_radiob = tk.Radiobutton(pident_frame_radio,text="A4#",command = scale_click,variable=scale_value,
                           value=163, width=3, height=2,bg="coral")
A4sh_radiob.grid(row=3, column=4, sticky=tk.EW)
B4_radiob = tk.Radiobutton(pident_frame_radio,text="B4",command = scale_click,variable=scale_value,
                           value=171, width=2, height=2,bg="orangered",fg="black")
B4_radiob.grid(row=3, column=5, sticky=tk.EW)
C5_radiob = tk.Radiobutton(pident_frame_radio,text="C5",command = scale_click,variable=scale_value,
                           value=172, width=2, height=2,bg="orangered",fg="black")
C5_radiob.grid(row=3, column=6, sticky=tk.EW)
C5sh_radiob = tk.Radiobutton(pident_frame_radio,text="C5#",command = scale_click,variable=scale_value,
                           value=173, width=3, height=2,bg="orangered",fg="black")
C5sh_radiob.grid(row=3, column=7, sticky=tk.EW)
D5_radiob = tk.Radiobutton(pident_frame_radio,text="D5",command = scale_click,variable=scale_value,
                           value=181, width=2, height=2,bg="hotpink")
D5_radiob.grid(row=3, column=8, sticky=tk.EW)

Label=tk.Label(pident_frame_radio,text="D線:",bg="white",fg="black",width=7)
Label.grid(row=4, column=0, sticky=tk.E)
D3_radiob = tk.Radiobutton(pident_frame_radio,text="D3",command = scale_click,variable=scale_value,
                           value=200, width=2, height=2,bg="orange")
D3_radiob.grid(row=4, column=1, sticky=tk.EW)
D3sh_radiob = tk.Radiobutton(pident_frame_radio,text="D3#",command = scale_click,variable=scale_value,
                           value=201, width=3, height=2,bg="LightBlue1",fg="black")
D3sh_radiob.grid(row=4, column=2, sticky=tk.EW)
E3_radiob = tk.Radiobutton(pident_frame_radio,text="E3",command = scale_click,variable=scale_value,
                           value=211, width=2, height=2,bg="DarkOliveGreen1")
E3_radiob.grid(row=4, column=3, sticky=tk.EW)
F3_radiob = tk.Radiobutton(pident_frame_radio,text="F3",command = scale_click,variable=scale_value,
                           value=212, width=2, height=2,bg="DarkOliveGreen1")
F3_radiob.grid(row=4, column=4, sticky=tk.EW)
F3sh_radiob = tk.Radiobutton(pident_frame_radio,text="F3#",command = scale_click,variable=scale_value,
                           value=213, width=3, height=2,bg="DarkOliveGreen1")
F3sh_radiob.grid(row=4, column=5, sticky=tk.EW)
G3_radiob = tk.Radiobutton(pident_frame_radio,text="G3",command = scale_click,variable=scale_value,
                           value=214, width=2, height=2,bg="DarkOliveGreen1")
G3_radiob.grid(row=4, column=6, sticky=tk.EW)
G3sh_radiob = tk.Radiobutton(pident_frame_radio,text="G3#",command = scale_click,variable=scale_value,
                           value=231, width=3, height=2,bg="cornsilk2")
G3sh_radiob.grid(row=4, column=7, sticky=tk.EW)
A32_radiob = tk.Radiobutton(pident_frame_radio,text="A3",command = scale_click,variable=scale_value,
                           value=241, width=3, height=2,bg="lightsalmon")
A32_radiob.grid(row=4, column=8, sticky=tk.EW)
A3sh2_radiob = tk.Radiobutton(pident_frame_radio,text="A3#",command = scale_click,variable=scale_value,
                           value=242, width=3, height=2,bg="lightsalmon")
A3sh2_radiob.grid(row=4, column=9, sticky=tk.EW)
B32_radiob = tk.Radiobutton(pident_frame_radio,text="B3",command = scale_click,variable=scale_value,
                           value=243, width=3, height=2,bg="lightsalmon")
B32_radiob.grid(row=4, column=10, sticky=tk.EW)
C42_radiob = tk.Radiobutton(pident_frame_radio,text="C4",command = scale_click,variable=scale_value,
                           value=244, width=3, height=2,bg="lightsalmon")
C42_radiob.grid(row=4, column=11, sticky=tk.EW)



Label=tk.Label(pident_frame_radio,text="G線:",bg="white",fg="black",width=7)
Label.grid(row=5, column=0, sticky=tk.E)
G2_radiob = tk.Radiobutton(pident_frame_radio,text="G2",command = scale_click,variable=scale_value,
                           value=300, width=2, height=2,bg="orange")
G2_radiob.grid(row=5, column=1, sticky=tk.EW)
G2sh_radiob = tk.Radiobutton(pident_frame_radio,text="G2#",command = scale_click,variable=scale_value,
                           value=301, width=4, height=2,bg="LightBlue1",fg="black")
G2sh_radiob.grid(row=5, column=2, sticky=tk.EW)
A2_radiob = tk.Radiobutton(pident_frame_radio,text="A2",command = scale_click,variable=scale_value,
                           value=311, width=2, height=2,bg="DarkOliveGreen1")
A2_radiob.grid(row=5, column=3, sticky=tk.EW)
A2sh_radiob = tk.Radiobutton(pident_frame_radio,text="A2#",command = scale_click,variable=scale_value,
                           value=312, width=3, height=2,bg="DarkOliveGreen1")
A2sh_radiob.grid(row=5, column=4, sticky=tk.EW)
B2_radiob = tk.Radiobutton(pident_frame_radio,text="B2",command = scale_click,variable=scale_value,
                           value=313, width=2, height=2,bg="DarkOliveGreen1")
B2_radiob.grid(row=5, column=5, sticky=tk.EW)
C3_radiob = tk.Radiobutton(pident_frame_radio,text="C3",command = scale_click,variable=scale_value,
                           value=314, width=2, height=2,bg="DarkOliveGreen1")
C3_radiob.grid(row=5, column=6, sticky=tk.EW)
C3sh_radiob = tk.Radiobutton(pident_frame_radio,text="C3#",command = scale_click,variable=scale_value,
                           value=331, width=3, height=2,bg="cornsilk2")
C3sh_radiob.grid(row=5, column=7, sticky=tk.EW)


Label=tk.Label(pident_frame_radio,text="C線:",bg="white",fg="black",width=7)
Label.grid(row=6, column=0, sticky=tk.E)
C2_radiob = tk.Radiobutton(pident_frame_radio,text="C2",command = scale_click,variable=scale_value,
                           value=400, width=2, height=2,bg="orange")
C2_radiob.grid(row=6, column=1, sticky=tk.EW)
C2sh_radiob = tk.Radiobutton(pident_frame_radio,text="C2#",command = scale_click,variable=scale_value,
                           value=401, width=3, height=2,bg="LightBlue1",fg="black")
C2sh_radiob.grid(row=6, column=2, sticky=tk.EW)
D2_radiob = tk.Radiobutton(pident_frame_radio,text="D2",command = scale_click,variable=scale_value,
                           value=411, width=2, height=2,bg="DarkOliveGreen1")
D2_radiob.grid(row=6, column=3, sticky=tk.EW)
D2sh_radiob = tk.Radiobutton(pident_frame_radio,text="D2#",command = scale_click,variable=scale_value,
                           value=412, width=3, height=2,bg="DarkOliveGreen1")
D2sh_radiob.grid(row=6, column=4, sticky=tk.EW)
E2_radiob = tk.Radiobutton(pident_frame_radio,text="E2",command = scale_click,variable=scale_value,
                           value=413, width=2, height=2,bg="DarkOliveGreen1")
E2_radiob.grid(row=6, column=5, sticky=tk.EW)
F2_radiob = tk.Radiobutton(pident_frame_radio,text="F2",command = scale_click,variable=scale_value,
                           value=414, width=2, height=2,bg="DarkOliveGreen1")
F2_radiob.grid(row=6, column=6, sticky=tk.EW)
F2sh_radiob = tk.Radiobutton(pident_frame_radio,text="F2#",command = scale_click,variable=scale_value,
                           value=431, width=3, height=2,bg="cornsilk2")
F2sh_radiob.grid(row=6, column=7, sticky=tk.EW)


# Button Frame
def select_wav():
    global filepath,dirname
    typ = [("Wav Files", "*.wav")]
    filepath = askopenfilename(defaultextension="*.wav",filetypes=typ)
    dirname = os.path.dirname(filepath)    

    entry_wav.delete(0,tk.END)
    entry_wav.insert(0, filepath)
    pident_text.insert("1.0", filepath + "を認識します．\n" + "------ \n")

def play_wav():
    pgm=os.path.dirname(__file__)+"/sound/play_chunk.py "
    cmd="mate-terminal --title='Play Sound' --command \"bash -c 'python3 "+ pgm + entry_wav.get() + "';bash\" --geometry 80x10+200+700 &"
    comment=entry_wav.get()+"を再生します\n" + "------ \n"
    pident_text.insert("1.0", comment)
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    res = pro.stderr + '\n'
    pident_text.insert("1.0", res)

def fft_wav():
    pgm=os.path.dirname(__file__)+"/sound/fft_wav.py "
    cmd="mate-terminal --title='FFT Sound' --command \"bash -c 'python3 " + pgm  + entry_wav.get() + "';bash\" --geometry 20x10+300+200 &"
    comment=entry_wav.get()+"を解析(fft)します\n"
    pident_text.insert("1.0", comment)
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    res = pro.stderr + '\n'
    pident_text.insert("1.0", res)

def pident_pgm_open():
    pgm_file=os.path.dirname(__file__)+"/sound/pos_from_wav.py"
    cmd='gvim -geometry 120x30+500+300 ' + pgm_file + ' & '
    pro = subprocess.run(cmd,shell=True)

def pos_from_wav():
    pgm_file=os.path.dirname(__file__)+"/sound/pos_from_wav.py"
    cmd=pgm_file + ' & '
    pro = subprocess.run(cmd,shell=True)


open_wav_button = tk.Button(pident_frame_button, activebackground='DarkOliveGreen1', width=13, text="選ぶ", padx=5, command=select_wav)
play_wav_button = tk.Button(pident_frame_button, activebackground='DarkOliveGreen1', width=11, text="再生", padx=5, command=play_wav)
fft_wav_button = tk.Button(pident_frame_button, activebackground='DarkOliveGreen1', width=11, text="全波形とFFT", padx=5, command=fft_wav)
pgm_wav_button = tk.Button(pident_frame_button, activebackground='DarkOliveGreen1', width=15, text="プログラム編集", padx=5, command=pident_pgm_open)
learn_wav_button = tk.Button(pident_frame_button, activebackground='DarkOliveGreen1', width=15, text="学習", padx=5, command=pos_from_wav)

open_wav_button.grid(row=0, column=0, sticky="ew", padx=5, pady=1)
play_wav_button.grid(row=1, column=0, sticky="ew", padx=5, pady=1)
fft_wav_button.grid(row=2, column=0, sticky="ew", padx=5, pady=1)
pgm_wav_button.grid(row=3, column=0, sticky="ew", padx=5, pady=1)
learn_wav_button.grid(row=4, column=0, sticky="ew", padx=5, pady=1)

def mouse_over_pgm_wav(event):
   pident_text.configure(bg="yellow")
   msg=" ----------------------------\n" + \
       "   [ pident_torch.py を編集します．]\n"
   pident_text.insert("1.0", msg)
pgm_wav_button.bind('<Enter>', mouse_over_pgm_wav)
pgm_wav_button.bind('<Leave>', mouse_leave)

def mouse_over_play_wav(event):
   pident_text.configure(bg="yellow")
   msg="   [ wav ファイルを再生します．「選ぶ」ボタンでwavファイルが選択されている必要があります． ]\n"+ \
   "   実行ターミナル(Play Sound) 内のキー操作で再生を制御できます． \n"+ \
   "   停止：s \n" + \
   "   再開：c \n" + \
   "   終了：q \n" + \
   "   1-chunk 進む：j \n" + \
   "   1-chunk 戻る：k \n" + \
   "   10-chunk 進む：J \n" + \
   "   10-chunk 戻る：K \n" 
   pident_text.insert("1.0", msg)
play_wav_button.bind('<Enter>', mouse_over_play_wav)
play_wav_button.bind('<Leave>', mouse_leave)
# ===== pos_ident_tab 末尾 =====


# ===== sound_analysis_tab をつくる =====
sound_frame_button = tk.Frame(sound_analysis_tab, relief=tk.RAISED, bd=2, bg='NavajoWhite2')
sound_frame_input = tk.Frame(sound_analysis_tab, relief=tk.RAISED, bd=2, bg="pink", width=500)
sound_frame_text = tk.Frame(sound_analysis_tab, relief=tk.RAISED, bd=2, bg="white", width=500)

# フレームの配置
sound_frame_input.grid(row=0, column=1,sticky="ns")
sound_frame_text.grid(row=1, column=1,sticky="ns")
sound_frame_button.grid(rowspan=2,  row=0, column=0,sticky="ns")

# text Frame
sound_text = scrolledtext.ScrolledText(sound_frame_text,bg="LightSkyBlue1",fg="black",width=102)
sound_text.grid(row=0, column=0, sticky="ew")

msg="  これは，音解析のタブです． \n" + \
    "  既存のwavファイルを開いて，波形のグラフ化とそのスペクトルをリアルタイムに可視化します． \n" + \
    "  「再生」ボタンを選択することで，play_chunk.py というプログラムが実行されます． \n" + \
    "  「プログラム編集」ボタンをクリックすると，そのコードを編集してCHUNKサイズや描画範囲などの詳細を \n" + \
    "  調整します． \n" + \
    "  「mic録音」を選択すると，マイクから音声を入力しながらその波形とスペクトルをリアルタイムに確認できます． \n" + \
    "  (mic_wav_rec.py) \n"
sound_text.insert("1.0",msg)


# Entry Frame
# Wav Entry
#global sound_file
Label=tk.Label(sound_frame_input,text="Play WAV File: ",bg="white",fg="black")
entry_sound=tk.Entry(sound_frame_input, textvariable='', width=55, bd=10, bg="IndianRed",fg="white")
entry_sound.insert(0,'')
Label.grid(row=0, column=0, sticky="ew")
entry_sound.grid(row=0, column=1, sticky=tk.EW)

sound_file=os.path.dirname(__file__)+"/sound/tmp.wav"
Label=tk.Label(sound_frame_input,text="Rec WAV File: ",bg="white",fg="black")
entry_rec=tk.Entry(sound_frame_input, textvariable='', width=55, bd=10, bg="IndianRed",fg="white")
entry_rec.insert(0,sound_file)
Label.grid(row=1, column=0, sticky="ew")
entry_rec.grid(row=1, column=1, sticky=tk.EW)

# Data Button Frame
def play_sound():
    pgm=os.path.dirname(__file__)+"/sound/play_chunk.py "
    cmd="mate-terminal --title='Play Sound' --command \"bash -c 'python3 "+ pgm + entry_sound.get() + "';bash\" --geometry 80x10+200+700 &"
    comment=entry_sound.get()+"を再生します\n"
    sound_text.insert("1.0", comment)
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    res = pro.stderr + '\n'
    sound_text.insert("1.0", res)

def fft_sound():
    pgm=os.path.dirname(__file__)+"/sound/fft_wav.py "
    cmd="mate-terminal --title='FFT Sound' --command \"bash -c 'python3 " + pgm  + entry_sound.get() + "';bash\" --geometry 20x10+300+200 &"
    comment=entry_sound.get()+"を解析(fft)します\n"
    sound_text.insert("1.0", comment)
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    res = pro.stderr + '\n'
    sound_text.insert("1.0", res)

def play_chunk_open():
    global play_file
    play_file=os.path.dirname(__file__)+"/sound/play_chunk.py"
    with open(play_file, "r") as open_file:
        play_code=open_file.read()
        sound_text.insert("1.0", play_code)
    cmd='gvim -geometry 120x30+500+300 ' + play_file + ' & '
    pro = subprocess.run(cmd,shell=True)

def mic_record():
    pgm=os.path.dirname(__file__)+"/sound/mic_wav_rec.py "
    cmd="mate-terminal --title='FFT Sound' --command \"bash -c 'python3 " + pgm  + entry_rec.get() + "';bash\" --geometry 80x10+200+600 &"
    comment=entry_rec.get()+"に録音します．\n"
    sound_text.insert("1.0", comment)
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    res = pro.stderr + '\n'
    sound_text.insert("1.0", res)

sound_button_open = tk.Button(sound_frame_button, activebackground='DarkOliveGreen1', width=15, text="選ぶ", padx=5, command=select_file)
sound_button_play = tk.Button(sound_frame_button, activebackground='DarkOliveGreen1', width=11, text="再生", padx=5, command=play_sound)
sound_button_fft = tk.Button(sound_frame_button, activebackground='DarkOliveGreen1', width=11, text="全波形とFFT", padx=5, command=fft_sound)
sound_button_pgm = tk.Button(sound_frame_button, activebackground='DarkOliveGreen1', width=11, text="プログラム編集", padx=5, command=play_chunk_open)
sound_button_mic = tk.Button(sound_frame_button, activebackground='DarkOliveGreen1', width=11, text="mic録音", padx=5, command=mic_record)

sound_button_open.grid(row=0, column=0, sticky="ew", padx=5, pady=1)
sound_button_play.grid(row=1, column=0, sticky="ew", padx=5, pady=1)
sound_button_fft.grid(row=2, column=0, sticky="ew", padx=5, pady=1)
sound_button_pgm.grid(row=3, column=0, sticky="ew", padx=5, pady=1)
sound_button_mic.grid(row=4, column=0, sticky="ew", padx=5, pady=1)

def mouse_over_sound_pgm(event):
   sound_text.configure(bg="yellow")
   msg="   [ play_chunk.py を編集します．]\n"+ \
   "   play_chunk.py は「再生」ボタンで実行されます． \n" 
   sound_text.insert("1.0", msg)
sound_button_pgm.bind('<Enter>', mouse_over_sound_pgm)
sound_button_pgm.bind('<Leave>', mouse_leave)

def mouse_over_sound_play(event):
   sound_text.configure(bg="yellow")
   msg="   [ wav ファイルを再生します．「選ぶ」ボタンでwavファイルが選択されている必要があります． ]\n"+ \
   "   実行ターミナル(Play Sound) 内のキー操作で再生を制御できます． \n"+ \
   "   停止：s \n" + \
   "   再開：c \n" + \
   "   終了：q \n" + \
   "   1-chunk 進む：j \n" + \
   "   1-chunk 戻る：k \n" + \
   "   10-chunk 進む：J \n" + \
   "   10-chunk 戻る：K \n" 
   sound_text.insert("1.0", msg)
sound_button_play.bind('<Enter>', mouse_over_sound_play)
sound_button_play.bind('<Leave>', mouse_leave)
# ===== sound_analysis_tab 末尾 =====

# ===== data_graph_tab をつくる =====
data_frame_button = tk.Frame(data_graph_tab, relief=tk.RAISED, bd=2, bg='NavajoWhite2')
data_frame_input = tk.Frame(data_graph_tab, relief=tk.RAISED, bd=2, bg="pink", width=500)
data_frame_text = tk.Frame(data_graph_tab, relief=tk.RAISED, bd=2, bg="white", width=500)

# フレームの配置
data_frame_input.grid(row=0, column=1,sticky="ns")
data_frame_text.grid(row=1, column=1,sticky="ns")
data_frame_button.grid(rowspan=2,  row=0, column=0,sticky="ns")

# text Frame
data_text = scrolledtext.ScrolledText(data_frame_text,bg="LightSkyBlue1",fg="black",width=102,height=23)
data_text.grid(row=0, column=0, sticky="ew")


data_plot_tutorial=\
"  これは，時系列データをグラフにするタブです． \n" + \
"  データ例 \n" + \
"  0.0,  1.0,  3.2 \n" + \
"  1.0,  2.0,  4.5 \n" + \
"  2.0,  3.0,  6.5 \n" + \
"  左の[プロット]ボタンをクリックしてから，次に[リモート実行]ボタンをクリックして下さい． \n" + \
"  リモート実行するプログラムは[ロボットをうごかす]タブで指定する必要があります． \n" 
data_text.insert("1.0",data_plot_tutorial)


# Entry Frame
# Program Entry
data_plot_mssg=StringVar()
global data_file
data_file=os.path.dirname(__file__)+'/samples/data.csv'
data_plot_mssg=data_file
Label=tk.Label(data_frame_input,text="Data File: ",bg="white",fg="black")
entry_data=tk.Entry(data_frame_input, textvariable=data_plot_mssg, width=55,bg="IndianRed",fg="white")
entry_data.insert(0,data_plot_mssg)
Label.grid(row=0, column=0, sticky="ew")
entry_data.grid(row=0, column=1, sticky=tk.EW)
#data_button_new = tk.Button(data_frame_input, activebackground='DarkOliveGreen1', text='New', command=new_file, bg="RoyalBlue",fg="white")

# Scale
Label=tk.Label(data_frame_input,text="ymax: ",bg="white",fg="black")
Label.grid(row=1, column=0, sticky="ew")

ymax=tk.StringVar()
def write_ymax_value(event=None):
   fp=open(os.path.dirname(__file__)+"/samples/ymax_value","w")
   fp.write(ymax.get())
   fp.close()

scale_ymax = tk.Scale(data_frame_input, 
                    variable = ymax,
                    command = write_ymax_value,
                    orient=tk.HORIZONTAL,   # 配置の向き、水平(HORIZONTAL)、垂直(VERTICAL)
                    length = 300,           # 全体の長さ
                    width = 30,             # 全体の太さ
                    sliderlength = 50,      # スライダー（つまみ）の幅
                    from_ = 0.0,            # 最小値（開始の値）
                    to = 2500.0,            # 最大値（終了の値）
                    resolution=10.0,        # 変化の分解能(初期値:1)
                    tickinterval=500        # 目盛りの分解能(初期値0で表示なし)
                    )
scale_ymax.grid(row=1, column=1, sticky="ew")


# Data Button Frame
data_button_open = tk.Button(data_frame_button, activebackground='DarkOliveGreen1', width=13, text="開く", padx=5, command=data_open)
data_button_init = tk.Button(data_frame_button, activebackground='DarkOliveGreen1', width=11, text="初期化", padx=5, command=data_initialize)
data_button_plot = tk.Button(data_frame_button, activebackground='DarkOliveGreen1', text="プロット", padx=5, command=data_plot_execute)
data_button_rexec = tk.Button(data_frame_button, activebackground='DarkOliveGreen1', bg="RoyalBlue3", fg="white", text="リモート実行", padx=5, height=4, command=remote_execute_2file)

def edit_data_plt():
    file=os.path.dirname(__file__)+"/samples/plot_data.py"
    entry_data.delete(0,tk.END)
    entry_data.insert(0, data_file)
    cmd='gvim -geometry 120x30+500+300 ' + file + ' & '
    pro = subprocess.run(cmd,shell=True)
data_button_edit = tk.Button(data_frame_button, activebackground='DarkOliveGreen1', text="プログラム編集", padx=5, command=edit_data_plt)

data_button_open.grid(row=0, column=0, sticky="ew", padx=5, pady=1)
data_button_init.grid(row=1, column=0, sticky="ew", padx=5, pady=1)
data_button_plot.grid(row=2, column=0, sticky="ew", padx=5, pady=1)
data_button_rexec.grid(row=5, column=0, sticky="ew", padx=5, pady=1)
data_button_edit.grid(row=6, column=0, sticky="ew", padx=5, pady=1)
# ===== data_graph_tab 最後尾 =====

# ===== sympy_graph_tab をつくる =====
sympy_frame_button = tk.Frame(sympy_graph_tab, relief=tk.RAISED, bd=2, bg="plum3")
sympy_frame_input = tk.Frame(sympy_graph_tab, relief=tk.RAISED, bd=2, bg="pink", width=500)
sympy_frame_text = tk.Frame(sympy_graph_tab, relief=tk.RAISED, bd=2, bg="white", width=500)

# フレームの配置
sympy_frame_input.grid(row=0, column=1,sticky="ns")
sympy_frame_text.grid(row=1, column=1,sticky="ns")
sympy_frame_button.grid(rowspan=2,  row=0, column=0,sticky="ns")

# text Frame
sympy_text = scrolledtext.ScrolledText(sympy_frame_text,bg="LightSkyBlue1",fg="black",width=102,height=18)
sympy_text.grid(row=0, column=0, sticky="ew")

# Entry Frame
# Function Entry
math_func=StringVar()
math_func='左の「開く」からプログラムを編集してください'
Label=tk.Label(sympy_frame_input,text="Function: ",bg="white",fg="black")
entry_func=tk.Entry(sympy_frame_input, textvariable=math_func, width=55,bg="IndianRed",fg="white")
entry_func.insert(0,math_func)
Label.grid(row=0, column=0, sticky="ew")
entry_func.grid(row=0, column=1, sticky=tk.EW)
sympy_button_new = tk.Button(sympy_frame_input, activebackground='DarkOliveGreen1', text='New', command=new_file, bg="RoyalBlue",fg="white")
sympy_button_new.grid(row=0, column=2, sticky="ew")

# Scale
Label=tk.Label(sympy_frame_input,text="beta: ",bg="white",fg="black")
Label.grid(row=1, column=0, sticky="ew")

beta=tk.StringVar()
def write_beta_value(event=None):
   fp=open(os.path.dirname(__file__)+"/samples/beta_value","w")
   fp.write(beta.get())
   fp.close()

scale_beta = tk.Scale(sympy_frame_input, 
                    variable = beta,
                    command = write_beta_value,
                    orient=tk.HORIZONTAL,   # 配置の向き、水平(HORIZONTAL)、垂直(VERTICAL)
                    length = 300,           # 全体の長さ
                    width = 30,             # 全体の太さ
                    sliderlength = 50,      # スライダー（つまみ）の幅
                    from_ = 0.0,            # 最小値（開始の値）
                    to = 10.0,              # 最大値（終了の値）
                    resolution=0.05,        # 変化の分解能(初期値:1)
                    tickinterval=50         # 目盛りの分解能(初期値0で表示なし)
                    )

Label=tk.Label(sympy_frame_input,text="offseta: ",bg="white",fg="black")
Label.grid(row=2, column=0, sticky="ew")
offset=tk.StringVar()
def write_offset_value(event=None):
   fp=open(os.path.dirname(__file__)+"/samples/offset_value","w")
   fp.write(offset.get())
   fp.close()
scale_offset = tk.Scale(sympy_frame_input, 
                    variable = offset,
                    command = write_offset_value,
                    orient=tk.HORIZONTAL,   # 配置の向き、水平(HORIZONTAL)、垂直(VERTICAL)
                    length = 300,           # 全体の長さ
                    width = 30,             # 全体の太さ
                    sliderlength = 50,      # スライダー（つまみ）の幅
                    from_ = -10.0,          # 最小値（開始の値）
                    to = 10.0,              # 最大値（終了の値）
                    resolution=0.05,        # 変化の分解能(初期値:1)
                    tickinterval=50         # 目盛りの分解能(初期値0で表示なし)
                    )
scale_beta.grid(row=1, column=1, sticky="ew")
scale_offset.grid(row=2, column=1, sticky="ew")

# Sympy Button Frame
sympy_button_open = tk.Button(sympy_frame_button, activebackground='DarkOliveGreen1', width=15, text="開く", padx=5, command=sympy_open)
sympy_button_plot = tk.Button(sympy_frame_button, activebackground='DarkOliveGreen1', text="プロット", padx=5, command=plot_execute)
sympy_button_sigmoid = tk.Button(sympy_frame_button, activebackground='DarkOliveGreen1', text="sigmoid", padx=5, command=plot_sigmoid)

sympy_button_open.grid(row=0, column=0, sticky="ew", padx=5, pady=1)
sympy_button_plot.grid(row=1, column=0, sticky="ew", padx=5, pady=1)
sympy_button_sigmoid.grid(row=4, column=0, sticky="ew", padx=5, pady=1)

# ===== sympy_graph_tab 最後尾 =====


# ==== EV3プログラミング tab をつくる =====
# Frameの定義と配置
frame_button = tk.Frame(ev3_execute_tab, relief=tk.RAISED, bd=2, bg="DarkOliveGreen3")
frame_input = tk.Frame(ev3_execute_tab, relief=tk.RAISED, bd=2, bg="pink")
frame_text = tk.Frame(ev3_execute_tab, relief=tk.RAISED, bd=2, bg="white")

frame_input.grid(row=0, column=1,sticky="ns")
frame_text.grid(row=1, column=1,sticky="ns")
frame_button.grid(rowspan=2, row=0, column=0,sticky="ns")

#text_editor = tk.Text(frame_text,bg="LightSkyBlue1",fg="black")
text_editor = scrolledtext.ScrolledText(frame_text,bg="LightSkyBlue1",fg="black",width=102)
text_editor.grid(row=0, column=0, sticky="ew")

copy_right="  **********************************************．\n"+ \
   "   IP Address: にリモートホストのアドレスを入力してください．\n" + \
   "   これはリモートホスト(robot)でプログラムを開発するためのGUIです．\n"+ \
   "   Gitレポジトリ: https://gitlab.com/honda_lab/etrobocon.git  \n"+ \
   "   CC BY-SA Yasushi Honda 2023 7/1．\n" + \
   "   icons: https://icons8.com/pulsar-color  \n"
text_editor.insert(tk.END, copy_right)


# Program Entry 
filepath=StringVar()
filepath='プログラム名（ディレクトリは自動的に補完されます）'
Label=tk.Label(frame_input,text="Program: ",bg="white",fg="black")
entry_prog=tk.Entry(frame_input, textvariable=filepath, width=55, bd=10, bg="IndianRed",fg="white")
#entry_prog.insert(0,filepath)
Label.grid(row=0, column=0, sticky="ew")
entry_prog.grid(row=0, column=1, sticky=tk.EW)
button_new = tk.Button(frame_input, activebackground='DarkOliveGreen1', text='New', command=new_file, bg="RoyalBlue",fg="white")
button_new.grid(row=0, column=2, sticky="ew")

# IP Address Entry
addr=StringVar()
Label=tk.Label(frame_input,text="IP Address: ",bg="white",fg="black")
Addr=tk.Entry(frame_input, textvariable=addr, width=55, bd=10, bg="IndianRed",fg="white")
Addr.insert(0,'172.16.7.xxx')
Label.grid(row=1, column=0, sticky="ew")
Addr.grid(row=1, column=1, sticky="ew")
button_ok = tk.Button(frame_input, activebackground='DarkOliveGreen1', text='ping', command=ping)
button_ok.grid(row=1, column=2, sticky="ew")


button_save = tk.Button(frame_button, text="Save As", padx=5, command=file_save)
button_gedit = tk.Button(frame_button, activebackground='DarkOliveGreen1', fg="white", bg="RoyalBlue", width=13,borderwidth=10, text="開く", padx=5, command=gedit)
button_gvim = tk.Button(frame_button, activebackground='DarkOliveGreen1', text="gvim", padx=5, command=open_gvim)
button_exec = tk.Button(frame_button, activebackground='DarkOliveGreen1', text="ローカル実行", padx=5, command=execute)
button_rexec = tk.Button(frame_button, activebackground='DarkOliveGreen1', bg="RoyalBlue3", fg="white", borderwidth=10, text="リモート実行", padx=5, height=4, command=remote_execute)
button_ssh = tk.Button(frame_button, activebackground='DarkOliveGreen1', text="ssh", padx=5, command=open_ssh)
button_key = tk.Button(frame_button, activebackground='DarkOliveGreen1', text="ssh-key", padx=5, command=ssh_key)
button_open_res = tk.Button(frame_button, activebackground='DarkOliveGreen1', text="open res", padx=5, command=open_res_terminal)
button_wiki = tk.Button(frame_button, activebackground='DarkOliveGreen1', text="wiki", padx=5, command=open_wiki)
button_copy_right = tk.Button(frame_button, activebackground='DarkOliveGreen1', text="copy right", padx=5, command=show_copy_right)
button_reboot = tk.Button(frame_button, activebackground='DarkOliveGreen1', text="再起動", padx=5, command=reboot)

#button_open.grid(row=0, column=0, sticky='ew', padx=5, pady=5)
#button_save.grid(row=1, column=0, sticky="ew", padx=5)
button_gedit.grid(row=0, column=0, sticky="ew", padx=5, pady=1)
button_rexec.grid(row=1, column=0, sticky="ew", padx=5, pady=5)
button_gvim.grid(row=2, column=0, sticky="ew", padx=5, pady=1)
button_exec.grid(row=3, column=0, sticky="ew", padx=5, pady=1)
button_ssh.grid(row=4, column=0, sticky="ew", padx=5, pady=1)
button_key.grid(row=5, column=0, sticky="ew", padx=5, pady=1)
button_open_res.grid(row=6, column=0, sticky="ew", padx=5, pady=1)
button_wiki.grid(row=7, column=0, sticky="ew", padx=5, pady=1)
button_copy_right.grid(row=8, column=0, sticky="ew", padx=5, pady=1)
button_reboot.grid(row=9, column=0, sticky="ew", padx=5, pady=1)

def mouse_over_button_reboot(event):
   msg="   [ このロボットコマンドセンターRCC2自体を再起動 ]\n"
   text_editor.configure(bg="yellow")
   text_editor.insert("1.0", msg)


button_reboot.bind('<Enter>', mouse_over_button_reboot)
button_reboot.bind('<Leave>',mouse_leave)

def mouse_over_button_key(event):
   text_editor.configure(bg="yellow")
   msg="   [ ssh-key をつくります．１度実行したら以後は必要ありません ]\n"+ \
   "   ssh-key-gen -t rsa ~/.ssh/" + addr.get() + "_rsa \n"+ \
   "   ssh-copy-id -i ~/.ssh/" + addr.get() + "_rsa.pub robot@" + addr.get() + "\n"
   text_editor.insert("1.0", msg)
button_key.bind('<Enter>', mouse_over_button_key)
button_key.bind('<Leave>',mouse_leave)

def mouse_over_button_ssh(event):
   msg="   [ IP addressで指定されたロボットにsshでログインします ]\n"
   text_editor.configure(bg="yellow")
   text_editor.insert("1.0", msg)
button_ssh.bind('<Enter>', mouse_over_button_ssh)
button_ssh.bind('<Leave>',mouse_leave)

def mouse_over_button_open_res(event):
   msg="   [ 結果出力terminalを起動し直します ]\n"
   text_editor.configure(bg="yellow")
   text_editor.insert("1.0", msg)
button_open_res.bind('<Enter>', mouse_over_button_open_res)
button_open_res.bind('<Leave>',mouse_leave)

def mouse_over_button_copy_right(event):
   global copy_right
   text_editor.insert("1.0", copy_right)
button_copy_right.bind('<Enter>', mouse_over_button_copy_right)
button_copy_right.bind('<Leave>',mouse_leave)

def mouse_over_button_wiki(event):
   global wiki_description
   wiki_description="   Honda Lab のGitLab wikiを開きます．\n" + \
   "   行動のためのロボット知能などについて知ることができます．\n" + \
   "   またこのRCC2については https://gitlab.com/honda_lab/etrobocon も参考にしてください．\n"
   text_editor.configure(bg='yellow',fg='black')
   text_editor.insert("1.0", wiki_description)
button_wiki.bind('<Enter>', mouse_over_button_wiki)
button_wiki.bind('<Leave>',mouse_leave)
# ==== EV3プログラミング_tab の最後尾 =====

# ==== git_operation_tab をつくる =====
git_frame_button = tk.Frame(git_operation_tab, relief=tk.RAISED, bd=2, bg="orange")
git_frame_input = tk.Frame(git_operation_tab, relief=tk.RAISED, bd=2, bg="pink", width=500)
git_frame_text = tk.Frame(git_operation_tab, relief=tk.RAISED, bd=2, bg="white", width=500)

git_frame_button.grid(rowspan=2, row=0, column=0,sticky="ns")
git_frame_input.grid(row=0, column=1,sticky="ns")
git_frame_text.grid(row=1, column=1,sticky="ns")

# Text Frame
git_text = tk.Text(git_frame_text,bg="LightSkyBlue1",fg="black",width=102,height=27)
git_text.grid(row=0, column=0, sticky="ew")
git_text.insert("1.0", "ここではGitLabへのSign inを求められる可能性があります．\n")

#  Input Frame
git_filepath=StringVar()
git_filepath='ここにファイル名を入力してください'
Label=tk.Label(git_frame_input,text="File: ",bg="white",fg="black")
git_entry=tk.Entry(git_frame_input, textvariable=git_filepath, width=55,bg="IndianRed",fg="white")
git_entry.insert(0,git_filepath)
Label.grid(row=0, column=0, sticky="ew")

git_entry.grid(row=0, column=1, sticky=tk.EW)

# Button Frame
git_button_open = tk.Button(git_frame_button, text="Open", padx=5, command=open_gvim,width=15)
git_button_add = tk.Button(git_frame_button, text="add", padx=5, command=git_add)
git_button_commit = tk.Button(git_frame_button, text="commit", padx=5, command=git_commit)
git_button_push = tk.Button(git_frame_button, text="push", padx=5, command=git_push)
git_button_reboot = tk.Button(git_frame_button, text="再起動", width=10, padx=5, command=reboot)

git_button_open.grid(row=1, column=0, sticky="ew", padx=5, pady=1)
git_button_add.grid(row=2, column=0, sticky="ew", padx=5, pady=1)
git_button_commit.grid(row=3, column=0, sticky="ew", padx=5, pady=1)
git_button_push.grid(row=4, column=0, sticky="ew", padx=5, pady=1)
git_button_reboot.grid(row=8, column=0, sticky="ew", padx=5, pady=1)

# ==== git_operation_tab 最後尾 =====

notebook.pack(expand=True, fill='both', padx=10, pady=10)

#open_res_terminal()

root.mainloop()

