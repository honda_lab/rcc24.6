# CC BY-SA Yasushi Honda 2023 9/11
# rcc2311のselectedに選ばれたcsvファイルをグラフ描画
import tkinter as tk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import os

common_bg='orange'
text_fg="#071407"
text_bg="#d0e0d0"
button_bg="#ffc666"


# ===== data_graph_tab をつくる =====
def const_tab(tab,text,selected,local,robot_id, robot_addr, calc_id, calc_addr):
   frame_button = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)
   frame_scale = tk.Frame(tab, relief=tk.RAISED, bd=0, bg=common_bg)

   # フレームの配置
   frame_button.pack(side="left")
   frame_scale.pack(side="left",expand=True,fill="x",padx=2)

   #select_entry=tk.Entry(frame_button,  bd=2, bg="white",fg="black")
   #select_entry.insert(0,"")
   #select_entry.pack(side="right",expand=True,fill="x",padx=3)

   data_plot_tutorial="これは，データ(Plot_Data/data.csv)をグラフにするタブです．" + \
   "data.csvが更新されると，リアルタイムにそれがグラフに反映されます．" +\
   "下記のようなデータ形式でdata.csvを記述してください． \n\n" + \
   "# 時刻, data1, data2 \n" + \
   "  0.0,  1.0,  3.2 \n" + \
   "  1.0,  2.0,  4.5 \n" + \
   "  2.0,  3.0,  6.5 \n" + \
   "     .....        \n" + \
   "\n\n" +\
   "左の[プロット]ボタンをクリックすると，Data File:の内容が描画されます．" + \
   "メインタブの「スタート」ボタンで実行する start.py などが， data.csv" + \
   "に上記形式でデータを書き込むと，リアルタイムに描画が更新されます． \n" 

   # Scale
   Label=tk.Label(frame_scale,text="elev: ", bg="dodgerblue",fg="white")
   #Label.pack(side="left",fill='y')
   elev=tk.StringVar()
   elev.set(15) 

   def write_value(event=None):
      if os.path.isdir(selected.get()):
         my_dir=selected.get() 
      elif os.path.isfile(selected.get()):
         my_dir=os.path.dirname(selected.get())
      else:
         print("ファイルを選んでください．")
         return

      filePath=my_dir+"/elev_azim"
      fp=open(filePath,"w")
      fp.write("%d %d" % (int(elev.get()),int(azim.get())))
      fp.close()

   scale_elev = tk.Scale(frame_scale, 
                    variable = elev,
                    command = write_value,
                    orient=tk.HORIZONTAL,   # 配置の向き、水平(HORIZONTAL)、垂直(VERTICAL)
                    width = 15,              # 全体の太さ
                    sliderlength = 30,      # スライダー（つまみ）の幅
                    from_ = 0,            # 最小値（開始の値）
                    to = 90,            # 最大値（終了の値）
                    showvalue=True,
                    resolution=1,        # 変化の分解能(初期値:1)
                    #tickinterval=15,        # 目盛りの分解能(初期値0で表示なし)
                    bg="dodgerblue",fg="white"
                    )
   #scale_elev.pack(expand=True,side="left",fill="x")

   Label=tk.Label(frame_scale,text="azim: ", bg="dodgerblue",fg="white")
   #Label.pack(side="left",fill='y')
   azim=tk.StringVar()
   azim.set(45)  
   scale_azim = tk.Scale(frame_scale, 
                    variable = azim,
                    command = write_value,
                    orient=tk.HORIZONTAL,   # 配置の向き、水平(HORIZONTAL)、垂直(VERTICAL)
                    width = 15,              # 全体の太さ
                    sliderlength = 30,      # スライダー（つまみ）の幅
                    from_ = 0,            # 最小値（開始の値）
                    to = 360,            # 最大値（終了の値）
                    resolution=1,        # 変化の分解能(初期値:1)
                    #tickinterval=45,        # 目盛りの分解能(初期値0で表示なし)
                    bg="dodgerblue",fg="white"
                    )
   #scale_azim.pack(expand=True,side="left",fill="x")


   def data_open():
      #global data_file
      #my_dir=os.path.dirname(__file__).replace("/tabs","")
      data_file=my_dir+"/Plot_Data/data.csv"
      select_entry.delete(0,tk.END)
      select_entry.insert(0, data_file)
      cmd='gvim -geometry 120x30+500+300 ' + data_file + ' & '
      pro = subprocess.run(cmd,shell=True)

   def data_initialize():
      my_dir=os.path.expanduser("~")
      cmd="echo \"0.0, 0,0, 0.0\" >  " + my_dir + "/Execute/data.csv"
      pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)

   def data_plot_execute():
      my_dir=os.path.dirname(__file__).replace("/tabs","")
      text.tag_config('finfo_line', background="#a0a7a0", foreground="#0e0a0a")
      if combobox.get()=="3D scatter":
         plot_data_code=my_dir+"/Plot_Data/scatter.py "
      elif combobox.get()=="2D plot":
         plot_data_code=my_dir+"/Plot_Data/plot_data.py "
      elif combobox.get()=="realtime":
         plot_data_code=my_dir+"/Plot_Data/realtime_plot.py "
      elif combobox.get()=="分布図":
         plot_data_code=my_dir+"/Plot_Data/distplot.py "
      elif combobox.get()=="hist2d":
         plot_data_code=my_dir+"/Plot_Data/hist2d.py "
      #text.delete("1.0",tk.END)
      text.insert("1.0", plot_data_code+"を実行します．\n", 'finfo_line')
      text.insert("1.0", "File Treeからcsvファイルを選んでください．\n",'finfo_line')

      cmd="mate-terminal --title='Graph of Data from remote' --command \"bash -c 'python3 " + plot_data_code + selected.get()+ "';bash\" --geometry 60x20+300+0 &"
      pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
      res = pro.stderr + '\n'
      #text.insert("1.0", res)


   def edit_data_plt():
      my_dir=os.path.dirname(__file__).replace("/tabs","")
      text.tag_config('warning_line', background="yellow", foreground="red")
      if combobox.get()=="3D scatter":
         plot_data_code=my_dir+"/Plot_Data/scatter.py "
      elif combobox.get()=="2D plot":
         plot_data_code=my_dir+"/Plot_Data/plot_data.py "
      elif combobox.get()=="realtime":
         plot_data_code=my_dir+"/Plot_Data/realtime_plot.py "
      elif combobox.get()=="分布図":
         plot_data_code=my_dir+"/Plot_Data/distplot.py "
      elif combobox.get()=="hist2d":
         plot_data_code=my_dir+"/Plot_Data/hist2d.py "
      #text.delete("1.0",tk.END)
      text.insert("1.0", plot_data_code+" を修正します．\n",'warning_line')
      cmd='gvim -geometry 120x30+500+300 ' + plot_data_code + ' & '
      pro = subprocess.run(cmd,shell=True)

   # Data Button Frame
   button_init = tk.Button(frame_button, text="初期化", 
           activebackground='DarkOliveGreen1',  activeforeground="red", bg="orange", fg="black",
           width=8, borderwidth=2,padx=0, command=data_initialize)
   button_plot = tk.Button(frame_button, text="描画", 
           activebackground='DarkOliveGreen1', activeforeground="red", bg=button_bg, fg=text_fg,
           width=8, borderwidth=2,padx=0, command=data_plot_execute)
   edit_button = tk.Button(frame_button, text=" 修正 ", 
           activebackground='DarkOliveGreen1', activeforeground="red", bg=button_bg, fg=text_fg,
           width=8, borderwidth=2,padx=0, command=edit_data_plt)

   style = ttk.Style()
   #style.theme_use("native")
   style.configure("office.TCombobox", selectbackground="Dodgerblue", fieldbackground=text_bg)
   style.configure("office.TCombobox", selectforeground="white", fieldforeground='white')
   style.configure("office.TCombobox", arrowsize=20, arrowcolor='white', background='Dodgerblue4')
   style.configure("office.TCombobox", foreground=text_fg)
   style.configure("office.TCombobox", justify=tk.CENTER, padding=5)

   plt_list = ['2D plot', '3D scatter', 'realtime', '分布図', 'hist2d']
   v = tk.StringVar()
   #combobox = ttk.Combobox(frame_button, width=5, textvariable= v, values=module)
   combobox = ttk.Combobox(frame_button, font=(16), width=10, values=plt_list, style="office.TCombobox")
   combobox.current(0)
   #combobox.bind('<<ComboboxSelected>>', data_plot_execute)
   combobox.pack(side="left",padx=6,pady=0)

   #button_init.pack(side="left",padx=2,pady=0)
   button_plot.pack(side="left",padx=2,pady=0,fill='both')
   edit_button.pack(side="left",padx=2,pady=0,fill='both')
