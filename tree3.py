#!/usr/bin/env python3
# File Treeからプログラムを実行/停止．
# CC BY-SA Yasushi Honda 2023 12.20
# 参考：https://qiita.com/dede-20191130/items/327fbeb181f13ee4ba2b
import glob
import os,sys
import subprocess
import tkinter as tk
import tkinter.ttk as ttk
import argparse

text_fg="#071407"
text_bg="#d6e0d6"

# pythonファイルを別窓で開くクラス
class Tree():
    def __init__(self,title,myPath,color,local,robot,calc,pos):
        self.local=local
        self.robot=robot
        self.calc=calc
        self.path=myPath
        self.color=color
        self.pos=pos

        self.default_font_size=10
        self.line_spacing=self.default_font_size*2

        # ルートフレームの作成
        self.root = tk.Tk()
        print("title:%s" % title)
        self.root.title(myPath+"@"+title)
        # rootウィンドウの大きさ設定
        self.root.geometry("750x500"+pos)
        self.root.attributes("-topmost", False)

        # Frame設定
        entry_frame = tk.Frame(self.root, bg="orange")
        entry_frame.pack(side="top",fill="x")
        frame1 = ttk.Frame(self.root, padding=3)
        frame1.pack(expand=True,side="top",fill="both")
        exec_frame = ttk.Frame(self.root, padding=0)
        exec_frame.pack(side="top",fill="x")
        button_frame = tk.Frame(self.root, bg=color)
        button_frame.pack(side="top",fill="x")

        # ツリービューの設定
        self.style = ttk.Style()
        self.style.configure("Treeview", fieldbackground=text_bg, rowheight=20)
        self.tree = ttk.Treeview(frame1, height=4, selectmode="browse" )

        # 列インデックスの作成
        self.tree["columns"] = (1,2)

        self.tree["show"] = ("tree","headings")

        # 各列の設定
        self.tree.column(1, width=600)
        self.tree.column(2, width=600)

        # 各列のヘッダー設定
        self.tree.heading(1, text="コメント")
        self.tree.heading(2, text="絶対パス")

        # tagの色指定
        self.tree.tag_configure("python_row", background=color,foreground='black')
        #self.tree.tag_configure("dir_row", background=color,foreground='black')
        self.tree.tag_configure("dir_row", background='#b0b4b0',foreground='#040804')

        # ツリービューの配置
        #self.tree.grid(row=0, column=0, columnspan=2, padx=3, pady=3, sticky=tk.N + tk.S + tk.E + tk.W)
        self.tree.pack(expand=True,side="top",fill="both")

        # スクロールバーの設定
        hscrollbar = ttk.Scrollbar(frame1, orient=tk.HORIZONTAL, command=self.tree.xview)
        vscrollbar = ttk.Scrollbar(frame1, orient=tk.VERTICAL, command=self.tree.yview)
        self.tree.configure(xscrollcommand=hscrollbar.set)
        self.tree.configure(yscrollcommand=vscrollbar.set)
        #vscrollbar.grid(row=0, column=1, columnspan=1, padx=3, pady=3, sticky=tk.NS)
        #hscrollbar.grid(row=1, column=0, columnspan=1, padx=3, pady=3, sticky=tk.EW)

        # ボタンの作成
        parent_button = tk.Button(button_frame,text="<<", command=self.parent_dir,bg="white",fg="black",
               activebackground='pink')
        close_button = tk.Button(button_frame,text="閉じる", command=self.destroy,bg="white",fg="black",
               activebackground='pink')
        child_button = tk.Button(button_frame,text=">>", command=self.child_dir,bg="white",fg="black",
               activebackground='pink')
        reload_button = tk.Button(button_frame,text="reload", command=self.reload_mydir,bg="white",fg="black",
               activebackground='pink')
        font_plus_button = tk.Button(button_frame,text="font+", command=self.font_plus,bg="white",fg="black",
               activebackground='pink')
        font_minus_button = tk.Button(button_frame,text="font-", command=self.font_minus,bg="white",fg="black",
               activebackground='pink')
        line_spacing_plus_btn = tk.Button(button_frame,text="space+", command=self.line_spacing_plus,bg="white",fg="black",
               activebackground='pink')
        line_spacing_minus_btn = tk.Button(button_frame,text="space-", command=self.line_spacing_minus,bg="white",fg="black",
               activebackground='pink')

        # ボタンの配置
        close_button.pack(side="right")
        font_plus_button.pack(side="right")
        font_minus_button.pack(side="right")
        line_spacing_plus_btn.pack(side="right")
        line_spacing_minus_btn.pack(side="right")
        child_button.pack(side="right")
        reload_button.pack(side="right")
        parent_button.pack(side="right")

        #parent_button.grid(padx=5, pady=5, row=1,column=0)
        #select_button.grid(padx=5, pady=5, row=1,column=1)
        #child_button.grid(padx=5, pady=5, row=1,column=2)
        #reload_button.grid(padx=5, pady=5, row=1,column=3)

        # make IP address entry 
        Label=tk.Label(entry_frame,text="Local:",bg="white",fg="black")
        self.local_entry=tk.Entry(entry_frame, bd=2, bg="white",fg="black")
        self.local_entry.insert(0,self.local)
        Label.pack(expand=True, side="left", fill="both")
        self.local_entry.pack(expand=True, side="left", fill="both")

        Label=tk.Label(entry_frame,text=" Robot:",bg="white",fg="black")
        self.robot_entry=tk.Entry(entry_frame, bd=2, bg="white",fg="black")
        self.robot_entry.insert(0,self.robot)
        Label.pack(expand=True, side="left", fill="both")
        self.robot_entry.pack(expand=True, side="left", fill="both")

        Label=tk.Label(entry_frame,text=" Calc:",bg="white",fg="black")
        self.calc_entry=tk.Entry(entry_frame, bd=2, bg="white",fg="black")
        self.calc_entry.insert(0,self.calc)
        Label.pack(expand=True, side="left", fill="both")
        self.calc_entry.pack(expand=True, side="left", fill="both")

        # 実行フレーム
        Label=tk.Label(exec_frame,text="Program: ")
        #Label.pack(side="left")
        self.program_entry=tk.Entry(exec_frame, bd=2, bg="white",fg="black")
        self.program_entry.insert(0,"")
        self.program_entry.pack(side="right",expand=True,fill="x")

        exec_button = tk.Button(exec_frame,text="実行", command=self.execute,
               activebackground='DarkOliveGreen3',
               fg='black', borderwidth=2)
        py_button = tk.Button(exec_frame,text="Python", command=self.python_exec,
               activebackground='DarkOliveGreen3',
               bg='DodgerBlue', fg='white', borderwidth=2)
        term_exec_button = tk.Button(exec_frame,text="端末実行", command=self.term_exec,
               activebackground='DarkOliveGreen3',
               fg='black', borderwidth=2)
        stop_button = tk.Button(exec_frame,text="停止", command=self.stop,
               activebackground='red',
               fg='black', borderwidth=2)

        exec_button.pack(side="left")
        py_button.pack(side="left")
        term_exec_button.pack(side="left")
        stop_button.pack(side="left")


        self.open_list("",myPath)

    def open_list(self,parent,path):
        # 選択可能なpythonファイルを辞書の値として設定
        # 指定フォルダ直下のすべてのpythonファイル（再帰的に検索）
        file_Dic = {} # ファイルリスト用の辞書
        dir_Dic = {} # ディレクトリリスト用の辞書
        if os.path.isdir(path):
            #for file in glob.glob(myPath+'/**', recursive=True):
            for file in glob.glob(path+'/**'):
                # fileは絶対パス付きのファイル名 
                #print(file)
                nameRoot, ext = os.path.splitext(os.path.basename(file))
                #if os.path.isfile(file) and ext in ('.py', '.pyw') and nameRoot != '__init__':
                if os.path.isfile(file) and ext in ('.py', '.pyw'):
                    fp=open(file,"r",encoding="utf8",errors='ignore')
                    fp.readline()
                    comment=fp.readline() # 2行目をコメントとして読み込む
                    fp.close()
                else:
                    comment="none"

                if os.path.isfile(file):
                    # 辞書として要素を辞書に格納（２次元辞書）
                    file_Dic[file] = {'name':os.path.basename(file),
                                  'comment':comment,
                                  'abspath':file,
                                  'ext':ext}
                    # ファイルの２行目をコメントとして表示
                    # selectで絶対パス付きのファイル名を必要とするので，abspathも辞書に格納
                elif os.path.isdir(file):
                    dir_Dic[file] = {'name':os.path.basename(file),'comment':'' ,'abspath':file}

            # dirレコードの挿入
            for k, v in sorted(dir_Dic.items(), key=lambda x: x[1]['name']):
                child=self.tree.insert(parent, "end", text=str(v['name']), 
                     values=(str(v['comment']),str(v['abspath'])), tag='dir_row' )
                #print(str(v['abspath']))
                self.open_list(child, str(v['abspath']))

            # fileレコードの挿入
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']=='.py':
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=(str(v['comment']),str(v['abspath'])),tag="python_row" )
            for k, v in sorted(file_Dic.items(), key=lambda x: x[1]['name']):
                if v['ext']!='.py':
                    self.tree.insert(parent, "end", text=str(v['name']), 
                        values=("",str(v['abspath'])) )

    def select_record(self,event):
        try:
            selectedItems = self.tree.selection()
            filePath = self.tree.item(selectedItems[0])['values'][1]
            print(filePath)
            self.program_entry.delete(0,tk.END)
            self.program_entry.insert(0,filePath)
        except:
            print("ファイルを選んでください")

    def start(self):
        self.tree.bind("<<TreeviewSelect>>",self.select_record)
        self.root.mainloop()

    def destroy(self):
        # フレーム閉じる
        self.root.quit()
        self.root.destroy()

    def term_exec(self):
        # 表から選んだファイルパスを取得
        try:
            selectedItems = self.tree.selection()
            filePath = self.tree.item(selectedItems[0])['values'][1]
            print(filePath)
            current_dir=os.path.dirname(filePath)
            os.chdir(current_dir)

            # pyファイル呼び出し実行
            cmd="mate-terminal --title=filePath --command \"bash -c 'python3 " + filePath + " " + self.local + " " + self.robot + " " + self.calc + \
            "';bash\" &"
            '''
            cmd="xterm -geometry 100x15" + self.pos + \
            " -bg " + self.color + " -fg black " + \
            " -fn 7x14 " + \
            " -e \"bash -c ' " + filePath + " " + self.local + " " + self.robot + " " + self.calc + \
            "';bash\" &"
            '''
            print(cmd)
            pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
            #proc=subprocess.Popen(cmd.strip().split(' '))
        except:
            print("ファイルを選んでください")

    def python_exec(self):
        # 表から選んだファイルパスを取得
        try:
            selectedItems = self.tree.selection()
            filePath = self.tree.item(selectedItems[0])['values'][1]
            print(filePath)
            current_dir=os.path.dirname(filePath)
            os.chdir(current_dir)

            # pyファイル呼び出し実行
            #cmd="mate-terminal --title='Execute' --command \"bash -c 'python3 " + filePath + \
            cmd="xterm -geometry 100x15" + self.pos + \
            " -bg " + self.color + " -fg black " + \
            " -fn 7x14 " + \
            " -e \"bash -c 'python3 " + filePath + " " + self.local + " " + self.robot + " " + self.calc + \
            "';bash\" &"
            print(cmd)
            pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
            #proc=subprocess.Popen(cmd.strip().split(' '))
        except:
            print("ファイルを選んでください")

    def execute(self):
        # 表から選んだファイルパスを取得
        try:
            selectedItems = self.tree.selection()
            filePath = self.tree.item(selectedItems[0])['values'][1]
            print(filePath)

            # pyファイル呼び出し実行
            #cmd="mate-terminal --title='Execute' --command \"bash -c 'python3 " + filePath + \
            cmd=filePath + " " + self.local + " " + self.robot + " " + self.calc + " &"
            print(cmd)
            #pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
            proc=subprocess.Popen(cmd.strip().split(' '))
        except:
            print("ファイルを選んでください")

    def stop(self):
        try:
            selectedItems = self.tree.selection()
            filePath = self.tree.item(selectedItems[0])['values'][1]
            program=os.path.basename(filePath).replace(".py","")
            print(program)
            cmd='pkill ' + program
            # 同期リモート実行，すなわち実行が終了するまで待つ場合は，subprocess.runを使う．
            prc=subprocess.run(cmd,shell=True,capture_output=True,text=True)

        except:
            print("停止に失敗しました")

        cmd='Execute/stop.py'
        prc=subprocess.run(cmd,shell=True,capture_output=True,text=True)

    def parent_dir(self):
        print(self.path,len(self.path))
        self.path=self.path[:self.path.rfind('/')]
        if len(self.path)==0:
            self.path="/home"
        self.root.title(self.path)
        self.tree.delete(*self.tree.get_children())
        self.open_list("",self.path)

    def child_dir(self):
        try:
            selectedItems = self.tree.selection()
            self.path = self.tree.item(selectedItems[0])['values'][1]
        except:
            print("選択失敗")

        print(self.path)
        if os.path.isdir(self.path):
            self.root.title(self.path)
            self.tree.delete(*self.tree.get_children())
            self.open_list("",self.path)
        else:
            print("ディレクトリを選んでください")

    def reload_mydir(self):
        self.tree.delete(*self.tree.get_children())
        self.open_list("",self.path)

    def font_mod(self,size):
        font = ('Yu Gothic UI', size)
        self.style.configure("Treeview", font=font, rowheight=size*2)
        self.tree.update()

    def font_plus(self):
        self.default_font_size+=2
        self.line_spacing=self.default_font_size*2
        font = ('Yu Gothic UI', self.default_font_size)
        #style.configure('.TButton', font=font)
        #style.configure('TLabel', font=font)
        self.local_entry.configure(font=font)
        self.robot_entry.configure(font=font)
        self.calc_entry.configure(font=font)
        self.program_entry.configure(font=font)
        self.font_mod(self.default_font_size)
    def font_minus(self):
        self.default_font_size-=2
        self.line_spacing=self.default_font_size*2
        font = ('Yu Gothic UI', self.default_font_size)
        #style.configure('TButton', font=font)
        #style.configure('TLabel', font=font)
        self.local_entry.configure(font=font)
        self.robot_entry.configure(font=font)
        self.calc_entry.configure(font=font)
        self.program_entry.configure(font=font)
        self.font_mod(self.default_font_size)

    def line_spacing_plus(self):
        self.line_spacing+=2
        self.style.configure("Treeview", rowheight=self.line_spacing)

    def line_spacing_minus(self):
        self.line_spacing-=2
        self.style.configure("Treeview", rowheight=self.line_spacing)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir")
    parser.add_argument("--local")
    parser.add_argument("--robot")
    parser.add_argument("--calc")
    parser.add_argument("--title")
    parser.add_argument("--pos")
    parser.add_argument("--color")
    args = parser.parse_args()
    my_dir=args.dir
    local=args.local
    robot=args.robot
    calc=args.calc
    title=args.title
    pos=args.pos
    color=args.color

    print(pos)
    my_tree=Tree(title,my_dir,color,local,robot,calc,pos) 
    my_tree.start()

