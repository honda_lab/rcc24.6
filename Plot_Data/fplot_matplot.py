from matplotlib import pyplot as plt
import time
import numpy as np

beta=0.1
offset=2.5
def func(x):
   y=1/(1+np.exp(-beta*(x-offset)))
   return y

def dfunc(x):
   y=beta*np.exp(-beta*(x-offset))/(1+np.exp(-beta*(x-offset)))**2
   return y
 
x=[0.0 for i in range(500)]
y=[0.0 for i in range(500)]
y1=[0.0 for i in range(500)]
lin, =  plt.plot(x,y,color='#ff0000',label='data0')
lin1, =  plt.plot(x,y1,color='#0000ff',label='data1')
 
now=time.time()
start=now
plt.grid()
plt.legend(loc='upper right')
 
tmin=-10;tmax=10
dt=(tmax-tmin)/500
while 1:
   fp=open("samples/beta_value","r")
   try:
      val=fp.read()
      beta=float(val)
   except:
      pass
   fp=open("samples/offset_value","r")
   try:
      val=fp.read()
      offset=float(val)
   except:
      pass

   t=tmin
   while t<tmax:
      x.pop(0)
      x.append(t)
      y.pop(0)
      y.append(func(t))
      y1.pop(0)
      y1.append(dfunc(t))
      t+=dt
   plt.xlim(min(x),max(x))
   plt.ylim(min(y),max(max(y),max(y1)))
   lin.set_xdata(x)
   lin.set_ydata(y)
   lin1.set_xdata(x)
   lin1.set_ydata(y1)
   plt.pause(0.0001)
   time.sleep(0.1)
   now=time.time()
   fp.close()
  
