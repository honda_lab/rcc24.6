#!/usr/bin/python3
# csv/trcファイルから分布図を作成
import matplotlib
from matplotlib import pyplot as plt
import os,sys,re
import time
import numpy as np
import seaborn as sns
matplotlib.use('TkAgg')

'''
plt.rcParams["figure.figsize"] = [7, 6]
plt.rcParams["grid.color"]="#505050"
plt.rcParams["grid.linestyle"]="--"
plt.rcParams["axes.titlecolor"] = "#eeeeee"
plt.rcParams["axes.facecolor"]="#303030"
plt.rcParams["axes.labelcolor"]="#eeeeee"
plt.rcParams["figure.facecolor"]="dodgerblue"
plt.rcParams["legend.facecolor"]="#eeeeee"
plt.rcParams["xtick.color"]="#eeeeee"
plt.rcParams["ytick.color"]="#eeeeee"
'''

plt.rcParams["figure.figsize"] = [7, 6]
plt.rcParams["grid.color"]="#505050"
plt.rcParams["grid.linestyle"]="--"
plt.rcParams["axes.titlecolor"] = "#000000"
plt.rcParams["axes.labelcolor"]="#000000"
plt.rcParams["axes.facecolor"]="#ffffff"
plt.rcParams["figure.facecolor"]="#ffffff"
plt.rcParams["legend.facecolor"]="#a0a0a0"
plt.rcParams["xtick.color"]="#000000"
plt.rcParams["ytick.color"]="#000000"

plt.grid()
PERIOD=0.5

x=[]
y=[]
comments=[]
xlabel='x axis'
ylabel='y axis'
plt.get_current_fig_manager().window.wm_geometry("+220+300")
 
now=time.time()
start=now
init=now
filePath=sys.argv[1] # 第一引数で与えられたファイルをプロットする
print(filePath)
plt.title(filePath)
fp=open(filePath,"r")
# はじめの数行はコメント
for i in range(1):
   line=fp.readline()
   print(line)

clm_dst=1 # 分布を求めるカラム 
xlabel="data["+str(clm_dst)+"]"
ylabel="Distribution"
legend=xlabel
while line!="": # データ行が空になるまで繰り返す
   if line[0]=="#": # コメント行
      legend=line.strip("\n")
      comments=re.split('[, ;:]+',line.strip('\n'))
      print(comments)
      if len(comments)>=2:
         xlabel=comments[0]
         ylabel=comments[1]
   else: # データ行
      # 正規表現を利用して１行を各データに分割する．
      data=re.split('[, ;:]+',line.strip('\n'))
      y.append(float(data[clm_dst]))

   line=fp.readline()

# ヒストグラムとカーネル密度推定
legend=legend+", std="+ str("%5.2f" % np.std(y)) # データの分散をラベル化
sns.distplot(y,label=legend)

plt.legend()
plt.xlabel(xlabel)
plt.ylabel(ylabel)
output=os.path.dirname(filePath)+'/plot.eps'
print(output)
plt.savefig(output)
plt.show()

fp.close()
  
