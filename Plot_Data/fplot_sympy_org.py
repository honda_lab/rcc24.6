from sympy import * 
from sympy.abc import * # 1文字変数の Symbol の定義が省略できる
import seaborn as sns
sns.set()
sns.set_style("darkgrid", {'grid.linestyle': '--'})
sns.set(font_scale=2.5)
import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = [12, 12]
                              
# 係数を与える 
beta=1.5
offset=2.5
xrange=(x,-7,10)
# 関数を決める  (シグモイド関数の例）
f= 1/(1+exp(-beta*(x-offset)))
print(f)
# 導関数
g= beta*exp(-beta*(x-offset))/(1+exp(-beta*(x-offset)))**2
print(f.diff(x))
# 関数を描画                          
g1=plot(f,xrange,show=False)
g2=plot(f.diff(x),xrange,show=False)

g1.extend(g2)
g1.show()


