#!/usr/bin/python3
# csvデータをリアルタイムに更新しながら描画
import matplotlib
from matplotlib import pyplot as plt
import os,sys,re
import time
import numpy as np

plt.rcParams["figure.figsize"] = [7, 6]
plt.rcParams["grid.color"]="#505050"
plt.rcParams["grid.linestyle"]="--"
plt.rcParams["axes.titlecolor"] = "#eeeeee"
plt.rcParams["axes.facecolor"]="#303030"
plt.rcParams["axes.labelcolor"]="#eeeeee"
plt.rcParams["figure.facecolor"]="dodgerblue"
plt.rcParams["legend.facecolor"]="#eeeeee"
plt.rcParams["xtick.color"]="#eeeeee"
plt.rcParams["ytick.color"]="#eeeeee"

#plt.style.use(['dark_background'])
plt.title("Graph of data from remote")
#plt.grid()

plt.xlabel("time(sec)")
plt.ylabel("Distance(mm)")
ymin=0
ymax=200
yrange=ymax-ymin
plt.ylim(ymin-yrange*0.1,ymax+yrange*0.1)

PERIOD=0.5

N_data=100
x=[0.0 for i in range(N_data)]
y=[0.0 for i in range(N_data)]
y1=[0.0 for i in range(N_data)]
y2=[0.0 for i in range(N_data)]
lin, =  plt.plot(x,y,color='orange',label='100.5MHz')
lin1, =  plt.plot(x,y1,color='white',label='90.5MHz')
lin2, =  plt.plot(x,y2,color='yellow',label='101.0MHz')
plt.legend(loc='upper left')
plt.get_current_fig_manager().window.wm_geometry("+220+300")
plt.pause(1.001)
 
 
now=time.time()
start=now
init=now
filePath=sys.argv[1] # 第一引数で与えられたファイルをプロットする
print(filePath)
my_dir=os.path.dirname(filePath)
fp=open(filePath,"r")
while 1:
   line=fp.readline()
   now=time.time()
   try:
      #print(line)
      data=re.split('[, ;:]+',line.strip('\n'))
      #print(data)
      t=now-init
      data1=float(data[4])
      data2=float(data[6])
      data3=float(data[8])

      x.pop(0)
      x.append(t)
      y.pop(0)
      y.append(data1)
      y1.pop(0)
      y1.append(data2)
      y2.pop(0)
      y2.append(data3)
      plt.xlim(min(x),max(x))
      lin.set_xdata(x)
      lin.set_ydata(y)
      lin1.set_xdata(x)
      lin1.set_ydata(y1)
      lin2.set_xdata(x)
      lin2.set_ydata(y2)
      plt.pause(0.001)
   except:
      pass
      #print("readline failed")
      #input("input any to continue")

   time.sleep(0.001)
   if now-start>PERIOD:
      yrange=max(y2)-min(y2)
      plt.ylim(0,30)
      start=now

fp.close()
  
