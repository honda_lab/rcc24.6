#!/usr/bin/python3
# csv/trcファイルから分布図を作成
import matplotlib
from matplotlib import pyplot as plt
import os,sys,re
import time
import numpy as np
import matplotlib.cm as cm  # カラーマップ
matplotlib.use('TkAgg')

plt.rcParams["figure.figsize"] = [7, 6]
plt.rcParams["grid.color"]="#505050"
plt.rcParams["grid.linestyle"]="--"
plt.rcParams["axes.titlecolor"] = "#000000"
plt.rcParams["axes.labelcolor"]="#000000"
plt.rcParams["axes.facecolor"]="#ffffff"
plt.rcParams["figure.facecolor"]="#ffffff"
plt.rcParams["legend.facecolor"]="#a0a0a0"
plt.rcParams["xtick.color"]="#000000"
plt.rcParams["ytick.color"]="#000000"
matplotlib.rc('font', family='Noto Sans CJK JP')

#plt.grid()

x=[]
y=[]
comments=[]
xlabel='x axis'
ylabel='y axis'
 
now=time.time()
start=now
init=now
filePath=sys.argv[1] # 第一引数で与えられたファイルをプロットする
print(filePath)
fp=open(filePath,"r")
line=fp.readline()

## 何行目からプロットするか，最初の数行コメントを飛ばす
for i in range(7):  ##
   line=fp.readline()
   print(line)

clm_x=2 ## 分布を求めるカラム 
clm_y=3 ## 分布を求めるカラム 
xlabel="data["+str(clm_x)+"]"
ylabel="data["+str(clm_y)+"]"
legend=xlabel
while line!="": # データ行が空になるまで繰り返す
   if line[0]=="#": # コメント行
      legend=line.strip("\n")
      comments=re.split('[, ;:\\t]+',line.strip('\n'))
      print(comments)
      if len(comments)>=2:
         xlabel=comments[1]
         ylabel=comments[2]
   else: # データ行
      # 正規表現を利用して１行を各データに分割する．
      data=re.split('[, ;:\\t]+',line.strip('\n'))
      x.append(float(data[clm_x]))
      y.append(float(data[clm_y]))

   line=fp.readline()

fig = plt.figure(figsize=(7,5))
ax = fig.add_subplot(111)
# ヒストグラムとカーネル密度推定
legend=legend+", std="+ str("%5.2f" % np.std(y)) # データの分散をラベル化
width=2000   ## x軸の範囲
height=2000  ## y軸の範囲
H = ax.hist2d(x,y, bins=[60,60], cmap=cm.jet, range=[[-width,width],[-height,height]])
H[3].set_clim(0,50) ## カラーマップで表示する範囲を指定．ここからはみ出た値は表示されないので注意．
ax.set_title(filePath,fontsize=8)
ax.set_xlabel(xlabel)
ax.set_ylabel(ylabel)

fig.colorbar(H[3],ax=ax)

#plt.get_current_fig_manager().window.wm_geometry("+220+300")
#plt.legend()
output=os.path.dirname(filePath)+'/hist2d.jpg'  ##
print(output)
plt.savefig(output,dpi=300)  
plt.show()

fp.close()
  
