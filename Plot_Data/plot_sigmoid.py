from matplotlib import pyplot as plt
import time
import os
import numpy as np
#import seaborn as sns
#sns.set()
#sns.set_style("whitegrid", {'grid.linestyle': '--'})
#sns.set(font_scale=0.8)
plt.rcParams["figure.figsize"] = [8, 8]


plt.title("Sigmoid function")
plt.grid()
plt.legend()

beta=0.1
offset=2.5
def func(x):
   y=1/(1+np.exp(-beta*(x-offset)))
   return y

def dfunc(x):
   y=beta*np.exp(-beta*(x-offset))/(1+np.exp(-beta*(x-offset)))**2
   return y
 
x=[0.0 for i in range(500)]
y=[0.0 for i in range(500)]
y1=[0.0 for i in range(500)]
lin, =  plt.plot(x,y,color='#ff0000',label='sigmoid')
lin1, =  plt.plot(x,y1,color='#0000ff',label='sigmoid.diff')
 
now=time.time()
start=now
plt.grid()
plt.legend(loc='upper left')
 
tmin=-10;tmax=10
dt=(tmax-tmin)/500
while 1:
   fp=open(os.path.dirname(__file__)+"/beta_value","r")
   try:
      val=fp.read()
      beta=float(val)
   except:
      pass
   fp=open(os.path.dirname(__file__)+"/offset_value","r")
   try:
      val=fp.read()
      offset=float(val)
   except:
      pass

   t=tmin
   while t<tmax:
      x.pop(0)
      x.append(t)
      y.pop(0)
      y.append(func(t))
      y1.pop(0)
      y1.append(dfunc(t))
      t+=dt
   plt.xlim(min(x),max(x))
   ymin=min(y)
   ymax=max(max(y),max(y1))
   yrange=ymax-ymin
   plt.ylim(ymin-yrange*0.1,ymax+yrange*0.1)
   lin.set_xdata(x)
   lin.set_ydata(y)
   lin1.set_xdata(x)
   lin1.set_ydata(y1)
   plt.pause(0.0001)
   time.sleep(0.1)
   now=time.time()
   fp.close()
  
