#!/usr/bin/python3
# rcc2311のFile Treeで選んだcsvファイルを2Dグラフ描画(plot)
import matplotlib
from matplotlib import pyplot as plt
import os,sys,re
import time
import numpy as np

plt.rcParams["figure.figsize"] = [7, 6]
plt.rcParams["grid.color"]="#505050"
plt.rcParams["grid.linestyle"]="--"
plt.rcParams["axes.titlecolor"] = "#000000"
plt.rcParams["axes.labelcolor"]="#000000"
plt.rcParams["axes.facecolor"]="#ffffff"
plt.rcParams["figure.facecolor"]="#ffffff"
plt.rcParams["legend.facecolor"]="#a0a0a0"
plt.rcParams["xtick.color"]="#000000"
plt.rcParams["ytick.color"]="#000000"
matplotlib.rc('font', family='Noto Sans CJK JP')

plt.grid()

ymin=0
ymax=200
yrange=ymax-ymin
plt.ylim(ymin-yrange*0.1,ymax+yrange*0.1)

x=[]
y=[]
comments=[]
xlabel='x axis'
ylabel='y axis'
plt.get_current_fig_manager().window.wm_geometry("+220+300")
 
now=time.time()
start=now
init=now
filePath=sys.argv[1] # 第一引数で与えられたファイルをプロットする
print(filePath)
plt.title(filePath,fontsize=8)
fp=open(filePath,"r")
for i in range(0): ## 先頭数行を読み込む(コメント行)
   line=fp.readline()
   print(line)
clm_x=0 ## x軸にするカラム 
clm_y=1 ## y軸にするカラム 
xlabel="data["+str(clm_x)+"]"
ylabel="data["+str(clm_y)+"]"
legend=ylabel
line=fp.readline()  # 1行目読み込み
while line!="": # データ行が空になるまで繰り返す
   if line[0]=="#": # コメント行
      legend=line.strip("\n")
      comments=re.split('[, ;:]+',line.strip('\n'))
      print(comments)
      if len(comments)>=2:
         xlabel=comments[0]
         ylabel=comments[1]
   else: # データ行
      # 正規表現を利用して１行を各データに分割する．
      data=re.split('[, ;:\\t]+',line.strip('\n'))
      x.append(float(data[clm_x]))
      y.append(float(data[clm_y]))

   line=fp.readline()

xrange=max(x)-min(x)
plt.xlim(min(x)-xrange*0.1,max(x)+xrange*0.1)

yrange=max(y)-min(y)
plt.ylim(min(y)-yrange*0.1,max(y)+yrange*0.1)

plt.scatter(x,y,marker='.',color='orange',label=legend)
#plt.plot(x,y,label=legend)  # データを線で結ぶ場合
plt.legend()
plt.xlabel(xlabel)
plt.ylabel(ylabel)
output=os.path.dirname(filePath)+'/plot.jpg'  ##
print(output)
plt.savefig(output)  ##
plt.show()

fp.close()
  
