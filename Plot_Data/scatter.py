#!/usr/bin/python3
# rcc2311のFile Treeで選んだcsvファイルを散布図(scatter)描画
import matplotlib
from matplotlib import pyplot as plt
import os,sys,re
import time
import numpy as np

plt.rcParams["figure.figsize"] = [7, 6]
plt.rcParams["grid.color"]="#505050"
plt.rcParams["grid.linestyle"]="--"
plt.rcParams["axes.titlecolor"] = "#000000"
plt.rcParams["axes.labelcolor"]="#000000"
plt.rcParams["axes.facecolor"]="#ffffff"
plt.rcParams["figure.facecolor"]="#ffffff"
plt.rcParams["legend.facecolor"]="#a0a0a0"
plt.rcParams["xtick.color"]="#000000"
plt.rcParams["ytick.color"]="#000000"

fig=plt.figure()
ax=fig.add_subplot(projection='3d')
ax.set_proj_type('ortho')  #  ortho /  persp
#ax.set_proj_type('persp' , focal_length=0.2)

ymin=0
ymax=200
yrange=ymax-ymin
#plt.ylim(ymin-yrange*0.1,ymax+yrange*0.1)

PERIOD=0.5

x=[]
y=[]
z=[]
comments=[]
legend=''
xlabel='x axis'
ylabel='y axis'
zlabel='z axis'
#plt.get_current_fig_manager().window.wm_geometry("+220+300")
 
 
now=time.time()
start=now
init=now
filePath=sys.argv[1] # 第一引数で与えられたファイルをプロットする
plt.title(filePath)
print(filePath)
fp=open(filePath,"r")
for i in range(7):
   line=fp.readline()
   print(line)
while line!="": # データ行が空になるまで繰り返す
   if line[0]=="#": # コメント行
      legend=line.strip("\n")
      comments=re.split('[, ;:\\t]+',line.strip('\n'))
      print(comments)
      if len(comments)>=3:
         xlabel=comments[1]
         ylabel=comments[2]
         zlabel=comments[3]
   else: # データ行
      # １行を各データに分割する．
      data=re.split('[, ;:\\t]+',line.strip('\n'))
      x.append(float(data[2]))
      y.append(float(data[3]))
      z.append(float(data[4]))

   line=fp.readline()

fp.close()

plt.xlim(min(x),max(x))
file=os.path.dirname(filePath)+"/elev_azim"
if os.path.isfile(file):
   fp=open(os.path.dirname(filePath)+"/elev_azim","r")
   line=fp.readline()
   item=re.split('[, :;]+',line.strip('\n'))
   print(item)
   elev=int(item[0])
   azim=int(item[1])
else:
   elev=25; azim=45
   fp=open(os.path.dirname(filePath)+"/elev_azim","w")
   fp.write("%d %d" % (elev, azim))

ax.view_init(elev=elev, azim=azim)
fp.close()

plt.ylim(min(y),max(y))
ax.scatter(x,y,z,marker='o',color='orange',label=legend)
ax.legend(loc='upper right')
ax.set_zlabel(zlabel)
ax.set_xlabel(xlabel)
ax.set_ylabel(ylabel)

output=os.path.dirname(filePath)+'/scatter.eps'
print(output)
plt.savefig(output)
plt.show()


  
