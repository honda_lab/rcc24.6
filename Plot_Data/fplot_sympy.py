from sympy import * 
from sympy.abc import * # 1文字変数の Symbol の定義が省略できる
import seaborn as sns
sns.set()
sns.set_style("darkgrid", {'grid.linestyle': '--'})
sns.set(font_scale=0.8)
import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = [8, 6]
#plt.legend(loc='upper left')
                              

# 係数を与える 
beta=0.010
beta2=100
offset=0
gap=30
xrange=(x,-105,105)
# 関数を決める  (シグモイド関数の例）
#f= 1/(1+exp(-beta*(x-offset)))
#f= beta*(x-offset)**2
#f= (100-gap)*tanh(beta*(x-offset))+gap*tanh(beta2*(x-offset))
f=62*atanh(x/101)+6*sign(x)
print(f)
print(f.diff(x))
# 関数を描画                          
grph=plot(f,xrange,ylim=(-135,135),legend=True,nb_ob_points=1000,show=False)

grph[0].label=str(f)
#grph[1].label='devivative'

grph.show()
