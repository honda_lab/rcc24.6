#!/usr/bin/env python3
#: Robot Command Center 24.0522
# Pythonファイルを開いて編集した後，実行するためのGUI
# CC BY-SA Yasushi Honda 2023 12/20

import tkinter as tk
import tkinter.font as tkFont
#from ttkthemes import ThemedTk
from tkinter import ttk,StringVar,scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re
import time
import os,sys
from tabs import main_tab
from tabs import robot_tab
from tabs import git_tab
from tabs import git_clone_tab
from tabs import posident_tab
from tabs import sound_tab
from tabs import data_tab
from tabs import sympy_tab
from tabs import wifi_tab
from tabs import latex_tab
from tabs import movie_tab
import tree4
import remote_tree
from threading import Thread
import timeout_decorator
import matplotlib.pyplot as plt
#from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

text_fg="#204030"
text_bg="#d0e0d0"
common_bg="orange"
howto_bg="#fafaa0"
mouse_over_color="DarkOliveGreen1"

my_dir=os.getcwd()
try:
    fp=open(my_dir+"/rcc24.conf")
    line=fp.readline()
    default_font_size=int(line.split(":")[1].replace(" ","").strip("\n"))
    line=fp.readline()
    title_bar=line.split(":")[1].replace(" ","").strip("\n")
    line=fp.readline()
    win_geometry=line.split(":")[1].replace(" ","").strip("\n")
    fp.close()
except:
    default_font_size=10
    title_bar="False"
    win_geometry="+30+30"

line_spacing=default_font_size*2

# rootをつくる
root = tk.Tk()
root.resizable(True, True)

#root=ThemedTk(theme="default")
root.attributes("-topmost", False) # 前面に固定
root.wm_overrideredirect(title_bar)  # remove tile bar
root.title('Robot Command Center (main)開発用')
root.geometry(win_geometry)
#root.option_add( "*font", "lucida "+str(default_font_size) ) # 全体のdefault font
root.option_add( "*font", "YuGothic "+str(default_font_size) ) # 全体のdefault font
#root.option_add( "*font", "Impact "+str(default_font_size) ) # 全体のdefault font
root.configure(bg=text_bg,borderwidth=3)

# Comboboxのpopdown listbox（ドロップダウンリスト又はプルダウン）の文字色を設定
root.option_add("*TCombobox*Listbox*selectBackground", 'DarkOliveGreen1') 
root.option_add("*TCombobox*Listbox*selectForeground", 'red') 
root.option_add("*TCombobox*Listbox*background", 'dodgerblue4') 
root.option_add("*TCombobox*Listbox*foreground", 'white')   # 文字色

style = ttk.Style()
style.configure('local.TButton', background="orange",foreground="black", borderwidth=3)
style.configure('robot.TButton', background="white",foreground="black", borderwidth=3,padding=2)
style.configure('calc.TButton', background="white",foreground="black", borderwidth=3,padding=2)
style.configure('TNotebook.Tab', expand=[(10,10),(10,10)],tabmargins=[10,10])

style.configure('TLabel', foreground=text_fg, background=text_bg)

# フォントサイズを変更
font = ('Yu Gothic UI', default_font_size)
#font = ('Monospace Regular', default_font_size)
style.configure('TNotebook.Tab', font=font)

style.map(
   "location.TNotebook.Tab",
   foreground=[('active','black'),('disabled','darkgray'),('selected',text_fg)],
   background=[('active','lightgreen'),('disabled','white'),('selected',text_bg)]
)
style.configure("location.TNotebook", tabposition="nw", background=text_bg)
style.configure("location.TNotebook.Tab", width=40)

style.map(
   "local.TNotebook.Tab",
   foreground=[('active','black'),('disabled','gray'),('selected','black')],
   background=[('active','lightgreen'),('disabled','black'),('selected','orange')]
)
style.configure("local.TNotebook", tabposition="nw", background=text_bg)

style.map("robot.TButton",
   foreground=[('active','black'),('disabled','darkgray')],
   background=[('active','DarkOliveGreen3'),('disabled','white')]
)
style.map("calc.TButton",
   foreground=[('active','black'),('disabled','darkgray')],
   background=[('active','pink'),('disabled','white')]
)

# Location tab
location_tab = ttk.Notebook(root,style="location.TNotebook")
location_tab.pack(expand=True, fill='both', padx=0, pady=0)


# local, robot calc の3つのフレーム
local_frame = tk.Frame(location_tab, bg='darkgoldenrod',padx=0)
robot_frame = tk.Frame(location_tab, bg='DarkOliveGreen3',padx=0)
calc_frame = tk.Frame(location_tab, bg='Pink',padx=0)
location_tab.add(local_frame, text='ローカル(Local Linux PC...)')
location_tab.add(robot_frame, text='ロボット(RasPi/EV3...)')
location_tab.add(calc_frame,  text='計算サーバ(Calc. pytorch with GPU...)')

#local内のタブ用
notebook = ttk.Notebook(local_frame,style="local.TNotebook")
notebook.pack(fill='x', padx=0, pady=0)

selected_frame = tk.Frame(local_frame, bg=text_bg,padx=0) # File treeから選ばれたpathを表示するentry
#main_frame = tk.Frame(root, bg=text_bg,padx=2) 

# Main_Panel
main_pane = tk.PanedWindow(local_frame, sashwidth = 3, bg='lightgray') ## File tree と Content Indicator を入れるペイン


selected_frame.pack(fill="x")
#main_frame.pack(expand=True, fill="both")
main_pane.pack(expand=True, fill="both")

tree_frame = ttk.Frame(main_pane, relief=tk.RAISED, padding=0)
text_frame = ttk.Frame(main_pane, relief=tk.RAISED, padding=0)

main_pane.add(tree_frame)
main_pane.add(text_frame)

# Content Indicator スクロールテキスト
text_editor = scrolledtext.ScrolledText(text_frame,bg=text_bg,fg=text_fg, padx=0,pady=0)
#print(text_editor.winfo_class())
text_editor.tag_config('warning_line', background=howto_bg, foreground="red")
text_editor.pack(side="top",expand=True,fill="both")
# create a sizegrip and place it at 
# the bottom-right corner of the window
sizegrip = ttk.Sizegrip(text_editor)
sizegrip.place(relx=1, rely=1, anchor=tk.SE)

def get_branch(event):
  cmd="git branch"
  prc=subprocess.run(cmd,shell=True,capture_output=True,text=True)
  #text_editor.insert("1.0", prc.stdout,"warning_line") 
notebook.bind("<<NotebookTabChanged>>", get_branch)

def make_new():
    my_path=selected_entry.get()
    my_dir=my_path[:my_path.rfind('/')]
    print(my_dir)
    text_pack()
    text_editor.yview_moveto(0)
    if os.path.exists(my_path):
        text_editor.configure(bg='red',fg="white")
        text_editor.insert("1.0",my_path+"はすでに存在します．\n")
    elif os.path.exists(my_dir):
        text_editor.configure(bg='yellow',fg="black")
        text_editor.insert("1.0",my_dir+"はすでに存在します．\n")
        text_editor.insert("1.0",my_path+"を作成します．\n")
        cmd='gedit ' + my_path +' &'
        pro = subprocess.run(cmd,shell=True)
        ltree.open_list("",my_dir)
    else:
        text_editor.configure(bg='yellow',fg="black")
        text_editor.insert("1.0",my_dir+"存在しません\n")
        text_editor.insert("1.0",my_dir+"を作成します．\n")
        os.mkdir(my_dir)
        cmd='gedit ' + my_path +' &'
        pro = subprocess.run(cmd,shell=True)
        ltree.open_list("",my_dir)

# 選ばれたエントリーを表示
Label=tk.Label(selected_frame,text=" Path:", bg=text_bg,fg=text_fg, height=1)
Label.pack(side="left",padx=0,pady=0)
selected_entry=tk.Entry(selected_frame, bd=0, bg="DodgerBlue4",fg="white")
try:
    exec_dir=os.getcwd()
    print(exec_dir)
    fp=open(exec_dir+"/work_dir","r")
    work_dir=fp.readline()
    fp.close()
    #print(work_dir)
    selected_entry.insert(0,work_dir)
    #os.chdir(work_dir)
except:
    selected_entry.insert(0,"")
    work_dir=os.path.expanduser("~")

make_button = ttk.Button(selected_frame, text='つくる', command=make_new, style='local.TButton', width=8)
selected_entry.pack(side="left",expand=True,fill="x")
make_button.pack(side="left")

# IP Address を読み込む
addr=StringVar()
my_dir=os.getcwd()
#my_dir=my_dir[:my_dir.rfind('/')]
print(my_dir)
try:
    fp=open(my_dir+"/ipaddress")
    line1=fp.readline()
    line2=fp.readline()
    line3=fp.readline()
    line4=fp.readline()
    line5=fp.readline()
    local=line1.split(":")[1].replace(" ","").strip("\n")
    robot_id=line2.split(":")[1].replace(" ","").strip("\n")
    robot_addr=line3.split(":")[1].replace(" ","").strip("\n")
    calc_id=line4.split(":")[1].replace(" ","").strip("\n")
    calc_addr=line5.split(":")[1].replace(" ","").strip("\n")
    fp.close()
except:
    local=""
    robot_id=""
    robot_addr=""
    calc_id=""
    calc_addr=""

# Calc. Tab
calc_location=tk.Frame(calc_frame,bg='pink')
calc_location.pack(side='top',fill='x')
calc_tree_frame=tk.Frame(calc_frame,bg='pink')
calc_tree_frame.pack(side='top',expand=True,fill='both')

# Calc user
calcid_btn = tk.Button(calc_location, text=' Execute(@Calc.):  ', \
        command=lambda: tree_exec("Calculation_server", calcu_entry.get(),calc_entry.get(),\
        "+0+440","lightpink"), width=15, bg='DodgerBlue', fg='white')
calcu_entry=tk.Entry(calc_location, width=11, bg="IndianRed",fg="white", borderwidth=2)
calcu_entry.insert(0,calc_id)
calcid_btn.pack(side="left",padx=3)
calcu_entry.pack(side="left",padx=3)
# Calc entry
Label=tk.Label(calc_location,text=" @ ", compound="left",bg="pink",fg="black", height=1)
calc_entry=tk.Entry(calc_location, width=17, bg="IndianRed",fg="white", borderwidth=2)
calc_entry.insert(0,calc_addr)
Label.pack(side="left")

calc_entry.pack(side="left",padx=3)

calc_text=tk.Text(calc_location, height=1, width=40, bg="white",fg="black") # ping 結果の表示
ping_calc_btn = ttk.Button(calc_location, text='ping', command=lambda:ping(calc_entry.get(),calc_text),width=5,style='calc.TButton')
ping_calc_btn.pack(side="left",padx=3)

calc_text.pack(side="left",padx=1)

calc_file_button = ttk.Button(calc_location, text='ファイルマネージャ', 
        command=lambda:file_mane(calcu_entry.get(),calc_entry.get()), style='calc.TButton')
calc_file_button.pack(side="right")


# Robot Tab
robot_location=tk.Frame(robot_frame,bg='DarkOliveGreen3')
robot_location.pack(side='top',fill='x')
robot_tree_frame=tk.Frame(robot_frame,bg='DarkOliveGreen3')
robot_tree_frame.pack(side='top',expand=True,fill='both')
# Robot ID entry
robot_exec_btn=tk.Button(robot_location,text="Execute(@RPi): ", width=15, \
        command=lambda: tree_exec("Robot",robot_entry.get(),addr_entry.get(),"+0+450","DarkOliveGreen3"),bg='DodgerBlue',fg='white')
robot_entry=tk.Entry(robot_location, textvariable=robot_id, width=11, bg="IndianRed",fg="white", borderwidth=2)
robot_entry.insert(0,robot_id)
robot_exec_btn.pack(side="left",padx=3)
robot_entry.pack(side="left",padx=3)

Label=tk.Label(robot_location,text=" @ ", compound="left",bg="DarkOliveGreen3",fg="black")
addr_entry=tk.Entry(robot_location, textvariable=addr, width=17, bg="IndianRed",fg="white", borderwidth=2)
addr_entry.insert(0,robot_addr)
Label.pack(side="left")
addr_entry.pack(side="left",padx=3)

robot_text=tk.Text(robot_location, height=1, width=40, bg="white",fg="black") # ping 結果の表示
button_ping = ttk.Button(robot_location, text='ping', command=lambda:ping(addr_entry.get(),robot_text), style='robot.TButton', width=5)
button_ping.pack(side="left",padx=3)
robot_text.pack(side="left",padx=1)

robot_file_button = ttk.Button(robot_location, text='ファイルマネージャ', 
        command=lambda:file_mane(robot_entry.get(),addr_entry.get()), style='robot.TButton')

robot_file_button.pack(side="right")

# Local Tab 内
# Exec.(robot)タブ
robot_tab_frame = tk.Frame(notebook, bg=common_bg)
notebook.add(robot_tab_frame, text=' Exec. ')
local_entry=robot_tab.const_tab(robot_tab_frame,text_frame,text_editor,
        local,robot_entry,addr_entry,calcu_entry,calc_entry,selected_entry)

# Calc. File Tree
calc_tree=remote_tree.Tree(calc_tree_frame,"Execute/",'Calc',local_entry,robot_entry,addr_entry,
                           calcu_entry,calc_entry)
# Robot File Tree
robot_tree=remote_tree.Tree(robot_tree_frame,"Execute/",'Robot',local_entry,robot_entry,addr_entry,
                           calcu_entry,calc_entry)

def tab_refresh(event):
    tab_name=location_tab.tab(location_tab.select(),'text')
    print(tab_name)
    if 'Robot' in tab_name:
        robot_tree.reload_robot()
    elif 'Calc' in tab_name:
        calc_tree.reload_calc()

#location_tab.bind("<<NotebookTabChanged>>", tab_refresh)


def file_mane(user,addr):
    try:
        cmd="gio mount sftp://"+user+"@"+addr +'/'
        pro = subprocess.run(cmd,shell=True)
    except:
        pass

    typ = [("Python", "*.py"), ("C", "*.c"),("Text", "*.txt"),("Shell","*.sh"),("Data","*.dat"),("All","*")]
    remote_dir="/run/user/1000/gvfs/sftp:host=" + addr + ",user="+user+"/home/"+user+"/Execute"
    if os.path.isdir(remote_dir)==False:
        cmd='ssh '+user+'@'+addr+' mkdir Execute'
        pro = subprocess.run(cmd,shell=True)
        time.sleep(1)

    cmd='caja -t ' + remote_dir + ' ' + my_dir +' &'
    pro = subprocess.run(cmd,shell=True)

def text_pack():
    for widget in text_frame.winfo_children():
        widget.pack_forget()
    text_editor.pack(side="top",expand=True,fill="both")

def ping(addr,show_text):
    text_pack()
    cmd="ping -c 2 " + addr + " &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    text_editor.yview_moveto(0)
    show_text.delete("1.0",tk.END)
    if 'time' in pro.stdout.split()[13]:
        show_text.insert("1.0",'Connected: '+pro.stdout.split()[13])
    else:
        show_text.insert("1.0",'Fail')
    text_editor.insert("1.0",pro.stdout)
    if len(pro.stderr)>0:
        show_text.insert("1.0",pro.stderr)
        text_editor.insert("1.0",pro.stderr)

    # IP addressの現在値を "ipaddress" に書き出す
    with open("ipaddress",mode="w") as fp:
        fp.write("local: "+local_entry.get()+"\n")
        fp.write("robot_id: "+robot_entry.get()+"\n")
        fp.write("robot: "+addr_entry.get()+"\n")
        fp.write("calc_id: "+calcu_entry.get()+"\n")
        fp.write("calc: "+calc_entry.get()+"\n")

def mouse_leave(event):
    text_editor.configure(fg=text_fg,bg=text_bg)
    #text_editor.delete("1.0",tk.END)


@timeout_decorator.timeout(15) # ネットワークなしのときなど，時間切れを設定
def tree_exec(title,user,addr,pos,color):
    local=local_entry.get()
    robot_id=robot_entry.get()
    robot_addr=addr_entry.get()
    calc_id=calcu_entry.get()
    calc_addr=calc_entry.get()
    #my_dir=os.path.dirname(__file__).replace("/tabs","")
    my_dir=os.getcwd()
    program="tree3.py"

    # programファイルの存在確認
    cmd='ssh '+user+'@'+addr+' test -e Execute/'+program+'; echo $?'
    prc=subprocess.run(cmd,shell=True,capture_output=True,text=True)
    if int(prc.stdout.strip("\n"))==1: # ファイルが存在しない場合
        # Execute/の存在確認
        cmd='ssh '+user+'@'+addr+' test -d Execute/ ; echo $?'
        prc=subprocess.run(cmd,shell=True,capture_output=True,text=True)
        print(int(prc.stdout.strip("\n")))
        if int(prc.stdout.strip("\n"))==1: # Execute/が存在しない場合
            cmd='ssh '+user+'@'+addr+' mkdir Execute'
            prc=subprocess.run(cmd,shell=True,capture_output=True,text=True)
           
        cmd='scp '+my_dir+'/'+program+' '+user+'@'+addr+':Execute/'
        print(cmd)
        prc=subprocess.run(cmd,shell=True,capture_output=True,text=True)
        print(prc.stdout)
        print(prc.stderr)
    else:
        print("%s は %s に存在します．" % (program,addr))

    try: # program がすでに実行を確認．
        cmd='ssh '+user+'@'+addr+' pgrep -c ' + program
        print(cmd)
        prc=subprocess.run(cmd,shell=True,capture_output=True,text=True)
        print(prc.stdout)
        #print("The number of %s : %3d" % (program,int(prc.stdout)),flush=True)
        if int(prc.stdout)>0:
            text_editor.configure(bg='red')
            text_editor.insert("0.0","\n %sはすでに実行中です． \n \
                                再実行するためには，すでに実行中の%sを閉じてください．\n" % (program,program))
        else:
            text_editor.configure(bg=text_bg)
            # ssh 実行時に source ~/.profile; を追加しておくことで，環境変数を有効化
            # venv の python3/pytorch を実行する場合，環境変数PATHにvenvの場所が先頭に入っている
            # 必要がある．
            print(title,user)
            cmd='ssh -X '+user+'@'+addr+' source ~/.profile; Execute/' + program \
                  + " --local " + local \
                  + " --dir " + "/home/" + user + "/Execute" \
                  + " --robot " + robot_addr \
                  + " --title " + title \
                  + " --pos " + pos \
                  + " --color " + color
            if len(calc_addr)>0:
                cmd=cmd  + " --calc " + calc_addr
            else: # calcサーバのアドレスが空白のときはデフォルトでLocalをつかう
                cmd=cmd  + " --calc " + local
            cmd=cmd+ " &" # 実行後に"&"をつけないと，local(このプログラム)がキーボードを受け付けない．
            print(cmd)
            try:
                # 非同期リモート実行の場合は，subprocess.Popenを使う．
                proc=subprocess.Popen(cmd.strip().split(' '))
            except:
                print("リモート実行GUIの起動に失敗しました．")
    except:
        print("リモート実行中の確認に失敗しました．")
        print("ロボットのアドレスが入力されているか確認してください．")

    # IP addressの現在値を "ipaddress" に書き出す
    with open("ipaddress",mode="w") as fp:
        fp.write("local: "+local+"\n")
        fp.write("robot_id: "+robot_id+"\n")
        fp.write("robot: "+robot_addr+"\n")
        fp.write("calc_id: "+calc_id+"\n")
        fp.write("calc: "+calc_addr+"\n")

# How To を読み込む
#my_dir=os.path.dirname(__file__).replace("/tabs","")
try:
    my_dir=os.getcwd()
    fp=open(my_dir+"/howto.txt","r")
    copy_right=""
    for line in fp.readlines():
        copy_right+=line
    fp.close()
    text_editor.insert(tk.END, copy_right)
except:
    pass

def mouse_over_button_send(event):
    #text_editor.delete(0.0,tk.END)
    description="Program: 選ばれたプログラムをリモートの Execute/" + \
    "に送ります(リモートコピーrcp)．\n" 
    text_editor.configure(bg=mouse_over_color,fg='black')
    text_editor.insert("1.0", description)

def mouse_over_button_ping(event):
    #text_editor.delete(0.0,tk.END)
    msg=" ************************************ \n" + \
    "    IP addressで指定されたロボットとの接続を確認します． \n" + \
    " ************************************ \n" 
    text_editor.configure(bg=mouse_over_color)
    text_editor.configure(fg="black")
    text_editor.insert("1.0", msg)

#button_ping.bind('<Enter>', mouse_over_button_ping)
button_ping.bind('<Leave>',mouse_leave)

# File Tree
canvas = tk.Canvas(master=text_frame, width=640, height=480)
ltree=tree4.Tree(tree_frame, text_frame, work_dir, text_bg,robot_entry,addr_entry,calcu_entry,calc_entry,
        text_editor,canvas,selected_entry)
ltree.open_list("",work_dir) ## ファイルツリーをつくる

# 検索タブ
main_tab_frame = tk.Frame(notebook, bg=common_bg)
#notebook.add(main_tab_frame, text=' 検索 ')
main_tab.const_tab(main_tab_frame,text_frame,text_editor,
        robot_entry,addr_entry,calcu_entry,calc_entry,selected_entry)

data_graph_tab = tk.Frame(notebook, bg='orange')
notebook.add(data_graph_tab, text='データグラフ', compound=tk.LEFT)
data_tab.const_tab(data_graph_tab,text_editor,selected_entry, local,robot_entry,addr_entry,calcu_entry,calc_entry)

sympy_graph_tab = tk.Frame(notebook, bg='orange')
#notebook.add(sympy_graph_tab, text='関数グラフ', image=func_img, compound=tk.LEFT)
sympy_tab.const_tab(sympy_graph_tab,text_editor,selected_entry,ltree)

sound_analysis_tab = tk.Frame(notebook, bg='orange')
#notebook.add(sound_analysis_tab, text='スペクトル解析 ', image=sound_img, compound=tk.LEFT)
sound_tab.const_tab(sound_analysis_tab,text_editor,selected_entry,local,robot_entry,addr_entry,calcu_entry,calc_entry)

git_operation_tab = tk.Frame(notebook, bg='orange')
#notebook.add(git_operation_tab, text=' git作業 ', image=git_img, compound=tk.LEFT)
git_tab.const_tab(git_operation_tab,selected_entry,text_editor,text_frame,local,robot_entry,addr_entry,calcu_entry,calc_entry)

git_clone_frame = tk.Frame(notebook, bg='orange')
#notebook.add(git_clone_tab, text=' git clone ', image=git_img, compound=tk.LEFT)
git_clone_tab.const_tab(git_clone_frame,selected_entry,text_editor,text_frame,local,robot_entry,addr_entry,calcu_entry,calc_entry)

movie_operation_tab = tk.Frame(notebook, bg='orange')
movie_tab.const_tab(movie_operation_tab,text_frame,selected_entry,text_editor,local,robot_entry,addr_entry,calcu_entry,calc_entry)
#notebook.add(movie_operation_tab, text=' Movie ', compound=tk.LEFT)

# インターフェイス名をファイルから読み取る．
my_dir=os.getcwd()
try:
    with open(my_dir+"/wifi_interface",mode="r") as fp:
      wifi=fp.readline().strip("\n")
except:
    wifi=""

wifi_setting_tab = tk.Frame(notebook, bg='orange')
notebook.add(wifi_setting_tab, text=' Wi-Fi ', compound=tk.LEFT)
wifi_tab.const_tab(wifi_setting_tab, text_frame, text_editor, wifi, local_entry)

latex_frame = tk.Frame(notebook, bg=common_bg)
#notebook.add(latex_frame, text=' LaTeX ')
latex_tab.const_tab(latex_frame,selected_entry,local,robot_entry,addr_entry,calcu_entry,calc_entry)


fig, ax = plt.subplots()
# ロボットツリーフレーム
home_dir=os.path.expanduser("~")

after_id=None
pos_x=0
pos_y=0
color_cnt=0
hue_cnt=0;sat_cnt=10;val_cnt=80
sign=+1
import colorsys
import numpy as np
def pos_move():
    global root_start
    global pos_x,pos_y
    global color_cnt
    global hue_cnt,sat_cnt,val_cnt,sign
    global after_id
    dl=180*np.pi/50
    incremental=1
    period=40 # msec
    width=5
    height=5
    canvas.create_rectangle(0, 10-10, 70, 10+50, fill="gray", width=0)
    now=time.time()
    duration=int(now-root_start)
    pos_x=int(sat_cnt*np.cos(hue_cnt*np.pi/180))+300
    pos_y=int(sat_cnt*np.sin(hue_cnt*np.pi/180))+200
    #red=int(color_cnt/256)
    #green=int((color_cnt-red*256)/16)
    #blue=int((color_cnt-red*256-green*16))
    val_cnt=80+int(20*np.cos(duration*np.pi/180))
    rgb=colorsys.hsv_to_rgb(hue_cnt/(360), sat_cnt/180, val_cnt/100)
    red=int(rgb[0]*255)
    green=int(rgb[1]*255)
    blue=int(rgb[2]*255)
    #print('%1x %1x %1x' % (red,green,blue))
    color_num=red*(256*256)+green*(256)+blue
    rect_color='#{:06X}'.format(color_num)
    text_color='#{:06X}'.format(int("ffffff",16)-color_num) # rectangleの色と文字の色を反転
    #print(color_num)
    #color_num="#ff0000"
    try:
        canvas.create_oval(pos_x-width, pos_y-height,pos_x+width, pos_y+height, fill=rect_color, width=0)
    except:
        pass
    h_str= str("h:%5d" % (hue_cnt))
    canvas.create_text(30, 10, text=h_str, fill="yellow")
    s_str= str("s:%5.1f" % (sat_cnt))
    canvas.create_text(32, 30, text=s_str, fill="yellow")
    v_str= str("v:%5d" % (val_cnt))
    canvas.create_text(30, 50, text=v_str, fill="yellow")
    color_cnt+=incremental
    color_cnt%=4096
    hue_cnt+=int(dl/sat_cnt*180/np.pi)
    d_sat=dl/sat_cnt * 2
    hue_cnt=hue_cnt%360
    if sat_cnt>180:
        #sign=-sign
        sat_cnt=10
        #root.after_cancel(after_id)
        #canvas.create_oval(300-200, 200-200, 300+200, 200+200, fill="white", width=0)
        canvas.delete('all')
    sat_cnt=sat_cnt+(sign*d_sat)
    #print("%5d %5.1f %5d" % (hue_cnt,sat_cnt,val_cnt))

    after_id=root.after(period, pos_move)


def reboot():
    cmd="pkill rcc24.py;" + os.getcwd() + "/rcc24.py &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)

# メニューバーの作成
# ファイル
def menu_file_open_click(event=None):
    print("「ファイルを開く」が選択された")
    filename = askopenfilename(
        title = "ファイルを開く",
        initialdir = "./" # 自分自身のディレクトリ
    )
    print(filename)
    spike_prog.delete(0,tk.END)
    spike_prog.insert(0,filename)
    ev3_prog.delete(0,tk.END)
    ev3_prog.insert(0,filename)
    data_prog.delete(0,tk.END)
    data_prog.insert(0,filename)

    cmd='gvim -geometry 120x25+645+0 ' + filename + ' &'
    pro = subprocess.run(cmd,shell=True)
    text_editor.insert("1.0", filename + " を編集します．\n\n")
 
def new_file_edit(event=None):
    #dir=askdirectory()
    #filepath=dir+'/' + file
    filepath=asksaveasfilename(initialdir='~')
    try:
        cmd='gedit ' + filepath +' &'
        pro = subprocess.run(cmd,shell=True)
        selected_entry.delete(0,tk.END)
        selected_entry.insert(0, filepath)
    except:
        pass


def help():
    text_editor.delete(0.0,tk.END)
    text_editor.configure(bg=text_bg,fg=text_fg)
    text_editor.insert("1.0", copy_right)

def open_wiki():
    wiki_addr="https://gitlab.com/honda_lab/etrobocon23/-/wikis/home"
    cmd="mate-terminal --title='Wiki Honda Lab' --hide-menubar --command \"bash -c 'firefox " + wiki_addr + " ';bash\" --geometry 77x10+300+660 &"
    pro = subprocess.run(cmd,shell=True, capture_output=True,text=True)
    if len(pro.stderr)>0:
        text_editor.configure(bg="red",fg="white")   
        res = pro.stderr + "\n \n"
        text_editor.insert("1.0", res)
    else:
        text_editor.configure(fg="black", bg="LightSkyBlue1")

def open_gvim():
    cmd='gvim -geometry 120x38+645 &'
    pro = subprocess.run(cmd,shell=True)

def key(user,address):
    print(user,address)
    rsa_file=os.path.expanduser('~/.ssh/')+address+"_rsa"

    if os.path.isfile(rsa_file):
        msg="すでにssh-keyが存在します"
        text_editor.insert("1.0", "\n" + msg + "\n")

        cmd="mate-terminal --command \"bash -c 'ssh-copy-id -i ~/.ssh/"+address+"_rsa.pub "+user+"@"+address+"';bash\" --geometry +300+200 &"
        pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
        text_editor.insert("1.0", "\n" + cmd + "\n")
    else:
        cmd="ssh-keygen -f ~/.ssh/" + address + "_rsa -t rsa -N '' "
        pro = subprocess.run(cmd,shell=True,capture_output=True,text=True)
        text_editor.insert("1.0", "\n" + pro.stdout + "\n")
        time.sleep(1)

        cmd="mate-terminal --command \"bash -c 'ssh-copy-id -i ~/.ssh/"+address+"_rsa.pub "+user+"@"+address+"';bash\" --geometry +300+200 &"
        pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
        text_editor.insert("1.0", "\n" + cmd + "\n")

def ssh():
    '''
    cmd="xterm -geometry 85x24+0+500 -title 'Robot' " + \
        " -bg DarkOliveGreen3 " + " -fg black " + \
        " -fn 7x14 " + \
        " -e \"bash -c 'ssh "+robot_entry.get()+"@"+addr_entry.get() + \
        "';bash\" &"
    cmd="terminator --command \"bash -c 'ssh "+robot_entry.get()+"@"+addr_entry.get() + "';bash\" --geometry 320x240+0+587 &"
    '''
    cmd="mate-terminal --command \"bash -c 'ssh "+robot_entry.get()+"@"+addr_entry.get() + "';bash\" --geometry 79x15+0+587 &"
    #pro=subprocess.Popen(cmd.strip().split(' '),shell=True, text=True)
    #pro=subprocess.Popen(cmd.strip().split(' '),stdout=subprocess.PIPE,stderr=subprocess.STDOUT,text=True)
    print(cmd)
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    text_editor.insert("1.0", "   [ " + addr_entry.get() + "にsshでログインします ]\n")

def write_conf():
    global default_font_size, title_bar, win_geometry
    my_dir=os.getcwd()
    #my_dir=my_dir[:my_dir.rfind('/')]
    width=root.winfo_width()
    height=root.winfo_height()
    x=root.winfo_x()
    y=root.winfo_y()
    if title_bar=="False":
        x=x-1
        y=y-77
    else:
        y=y-37
    geometry=str(width)+"x"+str(height)+"+"+str(x)+"+"+str(y)
    with open(my_dir+"/rcc24.conf","w") as fp:
        fp.write("default font size: "+str(default_font_size)+"\n")
        fp.write("title bar: "+title_bar+"\n")
        fp.write("geometry: "+geometry)
        fp.close()

def quit():
    write_conf()
    root.quit()
    root.destroy()

def font_family():
    fonts=tk.font.families()
    text_editor.insert("1.0",fonts)

def canvas_pack():
    global after_id
    for widget in text_frame.winfo_children():
        widget.pack_forget()
    canvas.pack(side="top",expand=True,fill="both")
    after_id=root.after(10,pos_move)

def stop_after():
    global after_id
    print(after_id)
    root.after_cancel(after_id) 
    canvas.delete('all')

def add_local_tab():
    location_tab.add(local_frame, text='ローカル (Local Linux PC...)')
    location_tab.select(location_tab.tabs()[location_tab.index('end')-1])

def add_main_tab():
    notebook.add(main_tab_frame, text=' 検索 ')
    notebook.select(notebook.tabs()[notebook.index('end')-1])

def add_robot_tab():
    notebook.add(robot_tab_frame, text=' Exec. ')
    notebook.select(notebook.tabs()[notebook.index('end')-1])

def add_data_tab():
    notebook.add(data_graph_tab, text='データグラフ', compound=tk.LEFT)
    notebook.select(notebook.tabs()[notebook.index('end')-1])

def add_func_tab():
    notebook.add(sympy_graph_tab, text='関数グラフ', compound=tk.LEFT)
    notebook.select(notebook.tabs()[notebook.index('end')-1])

def add_spect_tab():
    notebook.add(sound_analysis_tab, text='スペクトル解析 ', compound=tk.LEFT)
    notebook.select(notebook.tabs()[notebook.index('end')-1])

def add_git_tab():
    notebook.add(git_operation_tab, text=' git作業 ', compound=tk.LEFT)
    notebook.select(notebook.tabs()[notebook.index('end')-1])

def add_git_clone_tab():
    notebook.add(git_clone_frame, text=' git clone ', compound=tk.LEFT)
    notebook.select(notebook.tabs()[notebook.index('end')-1])

def add_latex_tab():
    notebook.add(latex_frame, text=' LaTeX ')
    notebook.select(notebook.tabs()[notebook.index('end')-1])

def add_wifi_tab():
    notebook.add(wifi_setting_tab, text=' Wi-Fi ', compound=tk.LEFT)
    notebook.select(notebook.tabs()[notebook.index('end')-1])

def add_movie_tab():
    notebook.add(movie_operation_tab, text=' Movie ', compound=tk.LEFT)
    notebook.select(notebook.tabs()[notebook.index('end')-1])

def del_tab():
    idx=notebook.tabs().index(notebook.select())
    #text_editor.insert("1.0","del tabx"+str(idx)+"\n","warning_line")
    notebook.forget(idx)

def font_plus():
    global default_font_size
    global line_spacing
    default_font_size+=2
    line_spacing=default_font_size*2
    font = ('Yu Gothic UI', default_font_size)
    style.configure('TNotebook.Tab', font=font)
    style.configure('local.TButton', font=font)
    style.configure('robot.TButton', font=font)
    style.configure('calc.TButton', font=font)
    style.configure('TLabel', font=font)
    menubar.configure(font=font)
    local_entry.configure(font=font)
    robot_entry.configure(font=font)
    addr_entry.configure(font=font)
    calcu_entry.configure(font=font)
    calc_entry.configure(font=font)
    selected_entry.configure(font=font)
    text_editor.configure(font=font)
    ltree.font_mod(default_font_size)
def font_minus():
    global default_font_size
    global line_spacing
    default_font_size-=2
    line_spacing=default_font_size*2
    font = ('Yu Gothic UI', default_font_size)
    style.configure('TNotebook.Tab', font=font)
    style.configure('local.TButton', font=font)
    style.configure('robot.TButton', font=font)
    style.configure('calc.TButton', font=font)
    style.configure('TLabel', font=font)
    menubar.configure(font=font)
    local_entry.configure(font=font)
    robot_entry.configure(font=font)
    addr_entry.configure(font=font)
    calcu_entry.configure(font=font)
    selected_entry.configure(font=font)
    calc_entry.configure(font=font)
    text_editor.configure(font=font)
    ltree.font_mod(default_font_size)

def line_spacing_plus():
    global line_spacing
    global default_font_size
    line_spacing+=2
    ltree.line_spacing(line_spacing)
    style.configure('TScrolledText', rowheight=line_spacing)
    #text_editor.configure(spacing1=line_spacing,spacing2=line_spacing,spacing3=line_spacing)
    text_editor.configure(spacing1=line_spacing-default_font_size*2)

def line_spacing_minus():
    global line_spacing
    global default_font_size
    line_spacing-=2
    ltree.line_spacing(line_spacing)
    style.configure('TScrolledText', rowheight=line_spacing)
    #text_editor.configure(spacing1=line_spacing,spacing2=line_spacing,spacing3=line_spacing)
    text_editor.configure(spacing1=line_spacing-default_font_size*2)

def topmost():
    print(root.attributes("-topmost"))
    if root.attributes("-topmost")==1:
        root.attributes("-topmost", False)
    else:
        root.attributes("-topmost", True) # 前面に固定

    root.update()

def change_title_bar():
    global title_bar
    if title_bar=="True":
        title_bar="False"
    else:
        title_bar="True"
    print(title_bar)

    quit()
    cmd="./rcc24.py"
    prc=subprocess.run(cmd,shell=True,capture_output=True,text=True)


menubar = tk.Menu(root)
menubar.configure(bg=text_bg,fg="black")
menu_file = tk.Menu(menubar, tearoff = False)
menu_file.configure(bg=text_bg,fg=text_fg)
#menu_file.add_command(label = "ファイルを選ぶ",  command = menu_file_open_click,  accelerator="Ctrl+O")
menu_file.add_command(label = "新規",  command = new_file_edit,  accelerator="Ctrl+N")
menu_file.add_command(label = "RCC24の再起動",  command=reboot)
menu_file.add_command(label = "終了",  command = quit)

menu_tool = tk.Menu(menubar, tearoff = False)
menu_tool.configure(bg=text_bg,fg=text_fg)
#menu_tool.add_command(label = "ファイルマネージャ",  command=file_mane,  accelerator="Ctrl+e")
menu_tool.add_command(label = "ロボット端末",  command=ssh)
menu_tool.add_command(label = "ロボットssh-key",  command=lambda: key(robot_entry.get(),addr_entry.get()))
menu_tool.add_command(label = "計算サーバssh-key",  command=lambda: key(calcu_entry.get(),calc_entry.get()))
menu_tool.add_command(label = "gvim",  command=open_gvim)

menu_help = tk.Menu(menubar, tearoff = False)
menu_help.configure(bg=text_bg,fg=text_fg)
menu_help.add_command(label = "How to",  command=help)
menu_help.add_command(label = "Wiki",  command=open_wiki)

menu_profile = tk.Menu(menubar, tearoff = False)
menu_profile.add_command(label = "font+", command=font_plus)
menu_profile.add_command(label = "font-", command=font_minus)
menu_profile.add_command(label = "line spacing+", command=line_spacing_plus)
menu_profile.add_command(label = "line spacing-", command=line_spacing_minus)
#menu_profile.add_command(label = "title bar",  command=change_title_bar)
menu_profile.add_command(label = "topmost",  command=topmost)
menu_profile.add_command(label = "text",  command=text_pack)
menu_profile.add_command(label = "canvas",  command=canvas_pack)
menu_profile.add_command(label = "stop canvas",  command=stop_after)
'''
menu_fonts = tk.Menu(menu_tool, tearoff = False)
menu_fonts.add_command(label = "YuGothic")
menu_fonts.add_command(label = "Lucida")
menu_profile.add_cascade(label = "font family",  menu=menu_fonts)
'''

menu_tab = tk.Menu(menubar, tearoff = False)
menu_tab.configure(bg=text_bg,fg=text_fg)
menu_tab.add_command(label = "git作業",  command = add_git_tab)
menu_tab.add_command(label = "Wi-Fi",  command = add_wifi_tab)
menu_tab.add_command(label = "データグラフ",  command = add_data_tab)
menu_tab.add_command(label = "関数グラフ",  command = add_func_tab)
menu_tab.add_command(label = "検索",  command = add_main_tab)
menu_tab.add_command(label = "LaTeX",  command = add_latex_tab)
menu_tab.add_command(label = "Exec.",  command = add_robot_tab)
menu_tab.add_command(label = "音スペクトル解析",  command = add_spect_tab)
menu_tab.add_command(label = "動画",  command = add_movie_tab)
menu_tab.add_command(label = "git clone",  command = add_git_clone_tab)
#menu_tab.add_command(label = "Local",  command = add_local_tab)
menu_tab.add_command(label = "削除",  command = del_tab)

# メニューバーに各メニューを追加
menubar.add_cascade(label="ファイル", menu = menu_file)
menubar.add_cascade(label="ツール", menu = menu_tool)
menubar.add_cascade(label="タブ追加/削除", menu = menu_tab)
menubar.add_cascade(label="Help", menu = menu_help)
menubar.add_cascade(label="設定", menu = menu_profile)
#menubar.add_cascade(label="選択",     menu = menu_select)

root_start=time.time()

# 親ウィンドウのメニューに、作成したメニューバーを設定
root.config(menu = menubar)

root.mainloop()

